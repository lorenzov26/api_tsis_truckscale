const identifyDb = ({ env }) => {
  return async function selects() {
    try {
      if (env === "dev") {
        return "truckscale";
      }

      if (env === "test") {
        return "truck_scale_test";
      }

      if (env === "uat") {
        return "truck_scale_uat";
      }

      if (env === "prod") {
        return "truck_scale_prod";
      }

      if (env === "sqa") {
        return "truck_scale_sqa";
      }
    } catch (e) {
      console.log(e);
    }
  };
};

module.exports = identifyDb;
