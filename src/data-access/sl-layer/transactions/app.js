const https = require("https");
const axios = require("axios");
const dotenv = require("dotenv");
dotenv.config();

const { hdbClient } = require("../../hdb/app");

require("tls").DEFAULT_MIN_VERSION = "TLSv1";

// ############

// base url for sap
const url = process.env.SAP_URL;

// base url for xsjs
const xsjs = process.env.XSJS_URL;

const transactions = {
  selectAllWarehouses: async ({ info }) => {
    try {
      const cookie = info.cookie;

      let data = [];

      const request = async link => {
        let res;

        res = await axios({
          method: "GET",
          url: `${url}/${link}`,
          headers: {
            "Content-Type": "application/json",
            Cookie: `${cookie};`
          },
          httpsAgent: new https.Agent({ rejectUnauthorized: false })
        });

        const arr = res.data.value;

        for await (let i of arr) {
          data.push(i);
        }
        while (res.data["odata.nextLink"]) {
          if (res.data["odata.nextLink"].search("/b1s/v1") === 0) {
            const nextPage = res.data["odata.nextLink"].replace("/b1s/v1/", "");

            await request(nextPage);
            break;
          } else {
            const nextPage = res.data["odata.nextLink"];
            await request(nextPage);
            break;
          }
        }
      };

      await request(`Warehouses?$select=WarehouseCode,WarehouseName`);

      return data;
    } catch (e) {
      console.log(e);
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  selectAllPurchaseOrdersTaggedInTransactionTable: async ({ info }) => {
    try {
      const cookie = info.cookie;

      let data = [];

      const request = async link => {
        let res;

        res = await axios({
          method: "GET",
          url: `${url}/${link}`,
          headers: {
            "Content-Type": "application/json",
            Cookie: `${cookie};`
          },
          httpsAgent: new https.Agent({ rejectUnauthorized: false })
        });

        const arr = res.data.value;

        for await (let i of arr) {
          data.push(i);
        }
        while (res.data["odata.nextLink"]) {
          if (res.data["odata.nextLink"].search("/b1s/v1") === 0) {
            const nextPage = res.data["odata.nextLink"].replace("/b1s/v1/", "");

            await request(nextPage);
            break;
          } else {
            const nextPage = res.data["odata.nextLink"];
            await request(nextPage);
            break;
          }
        }
      };

      await request(
        `BFITSUDO001?$select=U_APP_PO_ID&$filter=U_APP_PO_ID ne null`
      );

      return data;
    } catch (e) {
      console.log(e);
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  selectAllPurchaseOrders: async ({ info }) => {
    try {
      const cookie = info.cookie;

      let { from, to } = info;

      let data = [];

      const request = async link => {
        let res;

        res = await axios({
          method: "GET",
          url: `${url}/${link}`,
          headers: {
            "Content-Type": "application/json",
            Cookie: `${cookie};`
          },
          httpsAgent: new https.Agent({ rejectUnauthorized: false })
        });

        const arr = res.data.value;

        for await (let i of arr) {
          data.push(i);
        }
        while (res.data["odata.nextLink"]) {
          if (res.data["odata.nextLink"].search("/b1s/v1") === 0) {
            const nextPage = res.data["odata.nextLink"].replace("/b1s/v1/", "");

            await request(nextPage);
            break;
          } else {
            const nextPage = res.data["odata.nextLink"];
            await request(nextPage);
            break;
          }
        }
      };

      await request(
        `PurchaseOrders?$select=DocEntry,DocDate&$filter=DocDate ge '${from}' and DocDate le '${to}'`
      );

      return data;
    } catch (e) {
      console.log(e);
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  transactionsSelectAllTransmittalTransactions: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie

      const { from, to } = info;

      delete info.cookie; // remove cookie
      delete info.from;
      delete info.to;

      let data = [];

      const request = async link => {
        let res;

        res = await axios({
          method: "GET",
          url: `${url}/${link}`,
          headers: {
            "Content-Type": "application/json",
            Cookie: `${cookie};`
          },
          httpsAgent: new https.Agent({ rejectUnauthorized: false })
        });

        const arr = res.data.value;
        for await (let i of arr) {
          data.push(i);
        }

        // loop always if there is next link
        while (res.data["odata.nextLink"]) {
          const nextPage = res.data["odata.nextLink"];
          await request(nextPage);
          break;
        }
      };

      await request(
        `$crossjoin(BFITSUDO001,PurchaseOrders)?$expand=BFITSUDO001($select=DocEntry,U_TS_TRNS_TYPE,U_TS_STATUS,CreateDate,CreateTime),PurchaseOrders($select=DocEntry,CardCode,CardName,Address)&$filter=BFITSUDO001/U_TS_STATUS eq 'for transmittal' and BFITSUDO001/U_APP_PO_ID eq PurchaseOrders/DocEntry and BFITSUDO001/CreateDate ge '${from}' and BFITSUDO001/CreateDate le '${to}'`
      );

      info.cookie = cookie;
      info.from = from;
      info.to = to;
      return data;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  transactionsSelectAllWithPoTransactions: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie

      const { from, to } = info;

      delete info.cookie; // remove cookie
      delete info.from;
      delete info.to;

      let data = [];

      const request = async link => {
        let res;

        res = await axios({
          method: "GET",
          url: `${url}/${link}`,
          headers: {
            "Content-Type": "application/json",
            Cookie: `${cookie};`
          },
          httpsAgent: new https.Agent({ rejectUnauthorized: false })
        });

        const arr = res.data.value;
        for await (let i of arr) {
          data.push(i);
        }

        // loop always if there is next link
        while (res.data["odata.nextLink"]) {
          const nextPage = res.data["odata.nextLink"];
          await request(nextPage);
          break;
        }
      };

      await request(
        `$crossjoin(BFITSUDO001,U_BFI_TS_TRNSTYPE,PurchaseOrders)?$expand=BFITSUDO001($select=DocEntry,U_TS_TRNS_TYPE,U_TS_STATUS,CreateDate,CreateTime),U_BFI_TS_TRNSTYPE($select=U_TS_TRNSTYPE),PurchaseOrders($select=DocEntry,CardCode,CardName,Address)&$filter=BFITSUDO001/U_TS_TRNS_TYPE eq U_BFI_TS_TRNSTYPE/Code and U_BFI_TS_TRNSTYPE/U_TS_TRNSTYPE ne 'others' and BFITSUDO001/U_APP_PO_ID eq PurchaseOrders/DocEntry and BFITSUDO001/CreateDate ge '${from}' and BFITSUDO001/CreateDate le '${to}'`
      );

      info.cookie = cookie;
      info.from = from;
      info.to = to;
      return data;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  /*
  transactionsSelectAllCompletedTransactions: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie

      const { from, to } = info;

      delete info.cookie; // remove cookie
      delete info.from;
      delete info.to;

      let data = [];

      const request = async link => {
        let res;

        res = await axios({
          method: "GET",
          url: `${url}/${link}`,
          headers: {
            "Content-Type": "application/json",
            Cookie: `${cookie};`
          },
          httpsAgent: new https.Agent({ rejectUnauthorized: false })
        });

        const arr = res.data.value;
        for await (let i of arr) {
          data.push(i);
        }

        // loop always if there is next link
        while (res.data["odata.nextLink"]) {
          const nextPage = res.data["odata.nextLink"];
          await request(nextPage);
          break;
        }
      };

      await request(
        `sml.svc/V_TRANS2?$filter=U_TS_STATUS eq 'completed' and CreateDate ge '${from}' and CreateDate le '${to}'`
      );

      info.cookie = cookie;
      info.from = from;
      info.to = to;

      return data;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },*/
  transactionsSelectAllCompletedTransactions: async ({ info }) => {
    try {
      const { from, to } = info;

      const client = await hdbClient();

      const sql = `
      select
      "a"."DocEntry",
      TO_DATE("a"."CreateDate") as CreateDate,
      "a"."CreateTime",
      "a"."U_TS_STATUS",
      "a"."U_TS_TRCK_CODE",
      "b"."U_TS_IB_WEIGHT" as ib_weight,
      "b"."U_TS_UPDATEDATE" as ib_update_date,
      "b"."U_TS_UPDATETIME" as ib_update_time,
      (select
      "bb"."lastName" || ', ' || "bb"."firstName" 
      from "${process.env.DB}"."@BFI_TS_USERS" "aa" 
      left join "${process.env.sapDatabase}"."OHEM" "bb" on "aa"."U_APP_EMP_ID" = "bb"."empID" 
      where "aa"."Code" = "b"."U_TS_CREATEDBY" ) as ib_weigher,
      "c"."U_TS_OB_WEIGHT" as ob_weight,
      "c"."U_TS_REMARKS" as ob_remarks,
      "c"."U_TS_UPDATEDATE" as ob_update_date,
      "c"."U_TS_UPDATETIME" as ob_update_time,
      "c"."U_TS_OB_SIGN" as ob_sign,
      (select
      "bb"."lastName" || ', ' || "bb"."firstName" 
      from "${process.env.DB}"."@BFI_TS_USERS" "aa" 
      left join "${process.env.sapDatabase}"."OHEM" "bb" on "aa"."U_APP_EMP_ID" = "bb"."empID" 
      where "aa"."Code" = "c"."U_TS_CREATEDBY" ) as ob_weigher,
      "d"."U_TS_TRNSTYPE",
      ("e"."U_TS_LN" || ', ' || "e"."U_TS_FN" ) as driver_name,
      "f"."U_TS_PLATE_NUM",
      "g"."U_TS_TRTYPE",
      "g"."U_TS_TRMODEL",
      "z"."CardName",
      "z"."Address",
      (select
      string_agg("Dscription",
      ', ') 
      from "${process.env.sapDatabase}"."POR1" "dd" 
      where "dd"."DocEntry" = "z"."DocEntry" 
      group by "dd"."DocEntry" ) as item_list,
      "a"."U_TS_NUM_BAGS",
      "a"."U_TS_IS_NAPIER" 
    from "${process.env.DB}"."@BFI_TS_TRNS" "a" 
    left join "${process.env.DB}"."@BFI_TS_IBTRN" "b" on "a"."DocEntry" = "b"."DocEntry" 
    left join "${process.env.DB}"."@BFI_TS_OBTRN" "c" on "a"."DocEntry" = "c"."DocEntry" 
    and "b"."LineId" = "c"."LineId" 
    left join "${process.env.DB}"."@BFI_TS_TRNSTYPE" "d" on "a"."U_TS_TRNS_TYPE" = "d"."Code" 
    left join "${process.env.DB}"."@BFI_TS_DRIVERS" "e" on "b"."U_TS_DRIVERID" = "e"."Code" 
    left join "${process.env.DB}"."@BFI_TS_TRINFO" "f" on "b"."U_TS_TRKINFO" = "f"."Code" 
    left join "${process.env.DB}"."@BFI_TS_TRTYPE" "g" on "f"."U_TS_TRTYPE_ID" = "g"."Code" 
    left join "${process.env.sapDatabase}"."OPOR" "z" on "a"."U_APP_PO_ID" = "z"."DocEntry" 
    WHERE TO_DATE("a"."CreateDate") BETWEEN '${from}' AND '${to}' /*AND LOWER("a"."U_TS_STATUS") = 'completed'*/
    order by "a"."DocEntry" asc;
    `;

      const result = await new Promise(resolve => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  transactionsSelectAllNotCompletedTransactions: async ({ info }) => {
    try {
      const { from, to } = info;

      const client = await hdbClient();

      const sql = `
      select
      "a"."DocEntry",
      TO_DATE("a"."CreateDate") as CreateDate,
      "a"."CreateTime",
      "a"."U_TS_STATUS",
      "a"."U_TS_TRCK_CODE",
      "b"."U_TS_IB_WEIGHT" as ib_weight,
      "b"."U_TS_UPDATEDATE" as ib_update_date,
      "b"."U_TS_UPDATETIME" as ib_update_time,
      (select
      "bb"."lastName" || ', ' || "bb"."firstName" 
      from "${process.env.DB}"."@BFI_TS_USERS" "aa" 
      left join "${process.env.sapDatabase}"."OHEM" "bb" on "aa"."U_APP_EMP_ID" = "bb"."empID" 
      where "aa"."Code" = "b"."U_TS_CREATEDBY" ) as ib_weigher,
      "c"."U_TS_OB_WEIGHT" as ob_weight,
      "c"."U_TS_REMARKS" as ob_remarks,
      "c"."U_TS_UPDATEDATE" as ob_update_date,
      "c"."U_TS_UPDATETIME" as ob_update_time,
      "c"."U_TS_OB_SIGN" as ob_sign,
      (select
      "bb"."lastName" || ', ' || "bb"."firstName" 
      from "${process.env.DB}"."@BFI_TS_USERS" "aa" 
      left join "${process.env.sapDatabase}"."OHEM" "bb" on "aa"."U_APP_EMP_ID" = "bb"."empID" 
      where "aa"."Code" = "c"."U_TS_CREATEDBY" ) as ob_weigher,
      "d"."U_TS_TRNSTYPE",
      ("e"."U_TS_LN" || ', ' || "e"."U_TS_FN" ) as driver_name,
      "f"."U_TS_PLATE_NUM",
      "g"."U_TS_TRTYPE",
      "g"."U_TS_TRMODEL",
      "z"."CardName",
      "z"."Address",
      (select
      string_agg("Dscription",
      ', ') 
      from "${process.env.sapDatabase}"."POR1" "dd" 
      where "dd"."DocEntry" = "z"."DocEntry" 
      group by "dd"."DocEntry" ) as item_list,
      "a"."U_TS_NUM_BAGS" 
    from "${process.env.DB}"."@BFI_TS_TRNS" "a" 
    left join "${process.env.DB}"."@BFI_TS_IBTRN" "b" on "a"."DocEntry" = "b"."DocEntry" 
    left join "${process.env.DB}"."@BFI_TS_OBTRN" "c" on "a"."DocEntry" = "c"."DocEntry" 
    and "b"."LineId" = "c"."LineId" 
    left join "${process.env.DB}"."@BFI_TS_TRNSTYPE" "d" on "a"."U_TS_TRNS_TYPE" = "d"."Code" 
    left join "${process.env.DB}"."@BFI_TS_DRIVERS" "e" on "b"."U_TS_DRIVERID" = "e"."Code" 
    left join "${process.env.DB}"."@BFI_TS_TRINFO" "f" on "b"."U_TS_TRKINFO" = "f"."Code" 
    left join "${process.env.DB}"."@BFI_TS_TRTYPE" "g" on "f"."U_TS_TRTYPE_ID" = "g"."Code" 
    left join "${process.env.sapDatabase}"."OPOR" "z" on "a"."U_APP_PO_ID" = "z"."DocEntry" 
    WHERE TO_DATE("a"."CreateDate") BETWEEN '${from}' AND '${to}' AND LOWER("a"."U_TS_STATUS") <> 'completed'
    order by "a"."DocEntry" asc;
    `;

      const result = await new Promise(resolve => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  /*
  transactionsSelectAllNotCompletedTransactions: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie

      const { from, to } = info;

      delete info.cookie; // remove cookie
      delete info.from;
      delete info.to;

      let data = [];

      const request = async link => {
        let res;

        res = await axios({
          method: "GET",
          url: `${url}/${link}`,
          headers: {
            "Content-Type": "application/json",
            Cookie: `${cookie};`
          },
          httpsAgent: new https.Agent({ rejectUnauthorized: false })
        });

        const arr = res.data.value;
        for await (let i of arr) {
          data.push(i);
        }

        // loop always if there is next link
        while (res.data["odata.nextLink"]) {
          const nextPage = res.data["odata.nextLink"];
          await request(nextPage);
          break;
        }
      };

      await request(
        `sml.svc/V_TRANS2?$filter=U_TS_STATUS ne 'completed' and CreateDate ge '${from}' and CreateDate le '${to}'`
      );

      info.cookie = cookie;
      info.from = from;
      info.to = to;

      return data;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },*/

  transactionsSelectAllWithoutPoTransactions: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie

      const { from, to } = info;

      delete info.cookie; // remove cookie
      delete info.from;
      delete info.to;

      let data = [];

      const request = async link => {
        let res;

        res = await axios({
          method: "GET",
          url: `${url}/${link}`,
          headers: {
            "Content-Type": "application/json",
            Cookie: `${cookie};`
          },
          httpsAgent: new https.Agent({ rejectUnauthorized: false })
        });

        const arr = res.data.value;
        for await (let i of arr) {
          data.push(i);
        }

        // loop always if there is next link
        while (res.data["odata.nextLink"]) {
          const nextPage = res.data["odata.nextLink"];
          await request(nextPage);
          break;
        }
      };

      await request(
        `$crossjoin(BFITSUDO001,U_BFI_TS_TRNSTYPE)?$expand=BFITSUDO001($select=DocEntry,U_TS_TRNS_TYPE,U_TS_STATUS,CreateDate,CreateTime),U_BFI_TS_TRNSTYPE($select=U_TS_TRNSTYPE)&$filter=BFITSUDO001/U_TS_TRNS_TYPE eq U_BFI_TS_TRNSTYPE/Code and BFITSUDO001/CreateDate ge '${from}' and BFITSUDO001/CreateDate le '${to}'`
      );

      info.cookie = cookie;
      info.from = from;
      info.to = to;

      return data;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  transactionsUpdate: async info => {
    try {
      const { cookie, id } = info; // store cookie
      delete info.cookie; // remove cookie
      delete info.id; // remove id
      const res = await axios({
        method: "PATCH",
        url: `${url}/BFITSUDO001(${id})`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const req = {
        status: res.status,
        msg: res.statusText
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },
  selectOnePurchaseOrder: async ({ info }) => {
    try {
      const client = await hdbClient();

      const sql = `select "a"."DocEntry", "a"."CardName", "a"."Address", 
      (
      select
         string_agg("Dscription",
         ', ') 
        from "${process.env.sapDatabase}"."POR1" "b" 
        where "b"."DocEntry" = "a"."DocEntry" 
        group by "b"."DocEntry" 
      )as item_list from "${process.env.sapDatabase}"."OPOR" "a"
      where "a"."DocEntry" = ${info.id}`;

      const result = await new Promise(resolve => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  // selectOnePurchaseOrder: async ({ info }) => {
  //   try {
  //     const cookie = info.cookie; // store cookie
  //     delete info.cookie; // remove cookie

  //     const res = await axios({
  //       method: "GET",
  //       url: `${url}/PurchaseOrders?$select=CardCode,CardName,Address,DocumentLines&$filter=DocEntry eq ${info.id}`,
  //       headers: {
  //         "Content-Type": "application/json",
  //         Cookie: `${cookie};`
  //       },
  //       httpsAgent: new https.Agent({ rejectUnauthorized: false })
  //     });

  //     const req = {
  //       status: res.status,
  //       msg: res.statusText,
  //       data: res.data
  //     };

  //     info.cookie = cookie;
  //     return req;
  //   } catch (e) {
  //     const err = {
  //       status: e.response.status,
  //       msg: e.response.statusText,
  //       data: e.response.data
  //     };
  //     return err;
  //   }
  // },

  transactionsSelectOneWithPo: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "GET",
        url: `${url}/$crossjoin(BFITSUDO001,U_BFI_TS_TRNSTYPE,PurchaseOrders)?$expand=BFITSUDO001($select=DocEntry,U_TS_TRNS_TYPE,U_TS_STATUS,CreateDate,CreateTime),U_BFI_TS_TRNSTYPE($select=U_TS_TRNSTYPE),PurchaseOrders($select=DocEntry,CardCode,CardName,Address)&$filter=BFITSUDO001/U_TS_TRNS_TYPE eq U_BFI_TS_TRNSTYPE/Code and U_BFI_TS_TRNSTYPE/U_TS_TRNSTYPE ne 'others' and BFITSUDO001/U_APP_PO_ID eq PurchaseOrders/DocEntry and and BFITSUDO001/DocEntry eq ${info.id}`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data
      };

      info.cookie = cookie;
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  transactionsSelectOneForGrpo: async ({ info }) => {
    try {
      delete info.cookie; // remove cookie
      delete info.user;
      delete info.pw;
      delete info.source;

      const client = await hdbClient();

      const sql = `select
            "a"."DocEntry",
            "a"."CreateDate",
            "a"."CreateTime",
            "a"."U_TS_STATUS",
            "a"."U_TS_TRCK_CODE",
            "a"."U_TS_NUM_BAGS",
            "b"."U_TS_IB_WEIGHT" as ib_weight,
            "b"."U_TS_UPDATEDATE" as ib_update_date,
            "b"."U_TS_UPDATETIME" as ib_update_time,
            (select
            "bb"."lastName" || ', ' || "bb"."firstName" 
            from "${process.env.DB}"."@BFI_TS_USERS" "aa" 
            left join "${process.env.sapDatabase}"."OHEM" "bb" on "aa"."U_APP_EMP_ID" = "bb"."empID" 
            where "aa"."Code" = "b"."U_TS_CREATEDBY" ) as ib_weigher,
            "c"."U_TS_OB_WEIGHT" as ob_weight,
            "c"."U_TS_REMARKS" as ob_remarks,
            "c"."U_TS_UPDATEDATE" as ob_update_date,
            "c"."U_TS_UPDATETIME" as ob_update_time,
            "c"."U_TS_OB_SIGN" as ob_sign,
            (select
            "bb"."lastName" || ', ' || "bb"."firstName" 
            from "${process.env.DB}"."@BFI_TS_USERS" "aa" 
            left join "${process.env.sapDatabase}"."OHEM" "bb" on "aa"."U_APP_EMP_ID" = "bb"."empID" 
            where "aa"."Code" = "c"."U_TS_CREATEDBY" ) as ob_weigher,
            "d"."U_TS_TRNSTYPE",
            ("e"."U_TS_LN" || ', ' || "e"."U_TS_FN" ) as driver_name,
            "f"."U_TS_PLATE_NUM",
            "g"."U_TS_TRTYPE",
            "g"."U_TS_TRMODEL",
            "z"."CardName",
            "z"."Address",
            (select
            string_agg("Dscription",
            ', ') 
            from "${process.env.sapDatabase}"."POR1" 
            where "DocEntry" = "z"."DocEntry" 
            group by "DocEntry" ) as item_list,
            "a"."U_TS_NUM_BAGS" 
          from "${process.env.DB}"."@BFI_TS_TRNS" "a" 
          left join "${process.env.DB}"."@BFI_TS_IBTRN" "b" on "a"."DocEntry" = "b"."DocEntry" 
          left join "${process.env.DB}"."@BFI_TS_OBTRN" "c" on "a"."DocEntry" = "c"."DocEntry" 
          and "b"."LineId" = "c"."LineId" 
          left join "${process.env.DB}"."@BFI_TS_TRNSTYPE" "d" on "a"."U_TS_TRNS_TYPE" = "d"."Code" 
          left join "${process.env.DB}"."@BFI_TS_DRIVERS" "e" on "b"."U_TS_DRIVERID" = "e"."Code" 
          left join "${process.env.DB}"."@BFI_TS_TRINFO" "f" on "b"."U_TS_TRKINFO" = "f"."Code" 
          left join "${process.env.DB}"."@BFI_TS_TRTYPE" "g" on "f"."U_TS_TRTYPE_ID" = "g"."Code" 
          left join "${process.env.sapDatabase}"."OPOR" "z" on "a"."U_APP_PO_ID" = "z"."DocEntry" WHERE "a"."U_TS_TRCK_CODE" = '${info.code}'
          order by "a"."DocEntry" asc;`;

      const result = await new Promise(resolve => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  transactionsSelectOneOthers: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "GET",
        url: `${url}/$crossjoin(BFITSUDO001,U_BFI_TS_TRNSTYPE,BFITSUDO001/BFI_TS_IBTRNCollection,BFITSUDO001/BFI_TS_OBTRNCollection)?$expand=BFITSUDO001($select=DocEntry,U_TS_TRNS_TYPE,U_TS_TRCK_CODE,U_TS_STATUS,CreateDate,CreateTime,U_TS_TRCK_CODE,U_TS_NUM_BAGS),U_BFI_TS_TRNSTYPE($select=U_TS_TRNSTYPE),BFITSUDO001/BFI_TS_IBTRNCollection($select=LineId,DocEntry,U_TS_IB_WEIGHT,U_TS_UPDATEDATE,U_TS_UPDATETIME),BFITSUDO001/BFI_TS_OBTRNCollection($select=LineId,DocEntry,U_TS_OB_WEIGHT,U_TS_UPDATEDATE,U_TS_UPDATETIME,U_TS_OB_SIGN)&$filter=BFITSUDO001/U_TS_TRNS_TYPE eq U_BFI_TS_TRNSTYPE/Code and BFITSUDO001/DocEntry eq  BFITSUDO001/BFI_TS_IBTRNCollection/DocEntry and BFITSUDO001/DocEntry eq  BFITSUDO001/BFI_TS_OBTRNCollection/DocEntry and BFITSUDO001/DocEntry eq ${info.id}`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data
      };

      info.cookie = cookie;
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  transactionsSelectOneByTrackingCode: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const res = await axios({
        method: "GET",
        url: `${url}/BFITSUDO001?$filter=U_TS_TRCK_CODE eq '${info.trackingCode}'`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data
      };

      info.cookie = cookie;
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  transactionsSelectOne: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie


      const res = await axios({
        method: "GET",
        url: `${url}/BFITSUDO001(${info.id})`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data
      };

      info.cookie = cookie;
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  transactionsAdd: async info => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/BFITSUDO001`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data
      };

      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  // transactionDataGRPO: async ({ id }) => {
  //   try {

  //   } catch (e) {
  //     const err = {
  //       status: e.response.status,
  //       msg: e.response.statusText,
  //       data: e.response.data
  //     };
  //     return err;
  //   }
  // },

  selectTransactionInFsqrByTrackingCode: async ({ trackingCode }) => {
    try {
      const res = await axios({
        method: "POST",
        url: `${process.env.fsqrURL}/transactions/transactions/find_transaction`,
        data: {
          employee_id: "truck_scale",
          supplier_code: trackingCode
        },
        headers: {
          authorization: "Bearer "
        }
      });

      return res.data.transactions[0];
    } catch (e) {
      return e;
    }
  }
};

module.exports = { transactions };
