const https = require("https");
const axios = require("axios");
const dotenv = require("dotenv");
dotenv.config();
require("tls").DEFAULT_MIN_VERSION = "TLSv1";
const { hdbClient } = require("../../hdb/app");
// ############

// base url for sap
const url = process.env.SAP_URL;

// base url for xsjs
const xsjs = process.env.XSJS_URL;

const truckscale = {
  truckscaleUpdate: async ({ info }) => {
    try {
      const { cookie, id } = info; // store cookie
      delete info.cookie; // remove cookie
      delete info.id; // remove id
      delete info.modules;
      const res = await axios({
        method: "PATCH",
        url: `${url}/U_BFI_TS_TRSCALE('${id}')`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const req = {
        status: res.status,
        msg: res.statusText
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  truckscaleSelectOne: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "GET",
        url: `${url}/U_BFI_TS_TRSCALE?$filter=Code eq '${info.id}'`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data.value
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  truckscaleSelectAll: async ({ info }) => {
    try {
      const cookie = info.cookie;

      let data = [];

      const request = async link => {
        let res;

        res = await axios({
          method: "GET",
          url: `${url}/${link}`,
          headers: {
            "Content-Type": "application/json",
            Cookie: `${cookie};`
          },
          httpsAgent: new https.Agent({ rejectUnauthorized: false })
        });

        const arr = res.data.value;

        for await (let i of arr) {
          data.push(i);
        }
        while (res.data["odata.nextLink"]) {
          if (res.data["odata.nextLink"].search("/b1s/v1") === 0) {
            const nextPage = res.data["odata.nextLink"].replace("/b1s/v1/", "");
            await request(nextPage);
            break;
          } else {
            const nextPage = res.data["odata.nextLink"];
            await request(nextPage);
            break;
          }
        }
      };

      await request(`U_BFI_TS_TRSCALE`);

      return data;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  truckscaleAdd: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_BFI_TS_TRSCALE`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  truckscaleGetMaxCode: async ({ }) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@BFI_TS_TRSCALE";`;

      const result = await new Promise(resolve => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  truckscaleAddSelectByIPAndDeviceName: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie

      const res = await axios({
        method: "GET",
        url: `${url}/U_BFI_TS_TRSCALE?$filter=U_TS_TR_IP eq '${info.U_TS_TR_IP}' and U_TS_DEVICE eq '${info.U_TS_DEVICE}' and Code ne '${info.id}'`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data.value
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  }
};

module.exports = { truckscale };
