const https = require("https");
const axios = require("axios");
const dotenv = require("dotenv");
dotenv.config();
require("tls").DEFAULT_MIN_VERSION = "TLSv1";

const { hdbClient } = require("../../hdb/app");

// ############

// base url for sap
const url = process.env.SAP_URL;

// base url for xsjs
const xsjs = process.env.XSJS_URL;

const drivers = {
  driversUpdate: async ({ info }) => {
    try {
      const { cookie, id } = info; // store cookie
      delete info.cookie; // remove cookie
      delete info.id; // remove id
      const res = await axios({
        method: "PATCH",
        url: `${url}/U_BFI_TS_DRIVERS('${id}')`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const req = {
        status: res.status,
        msg: res.statusText
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  driversSelectOne: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "GET",
        url: `${url}/U_BFI_TS_DRIVERS?$filter=Code eq '${info.id}'`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data.value
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  driversSelectAll: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      let data = [];

      const request = async link => {
        let res;

        res = await axios({
          method: "GET",
          url: `${url}/${link}`,
          headers: {
            "Content-Type": "application/json",
            Cookie: `${cookie};`
          },
          httpsAgent: new https.Agent({ rejectUnauthorized: false })
        });

        const arr = res.data.value;
        for await (let i of arr) {
          data.push(i);
        }

        // loop always if there is next link
        while (res.data["odata.nextLink"]) {
          const nextPage = res.data["odata.nextLink"];
          await request(nextPage);
          break;
        }
      };

      await request(`U_BFI_TS_DRIVERS`);

      return data;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  driversAdd: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_BFI_TS_DRIVERS`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText
      };

      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  driversGetMaxCode: async ({ }) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@BFI_TS_DRIVERS";`;

      const result = await new Promise(resolve => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  driversAddSelectByName: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie

      const res = await axios({
        method: "GET",
        url: `${url}/U_BFI_TS_DRIVERS?$filter=U_TS_FN eq '${info.U_TS_FN}' and U_TS_LN eq '${info.U_TS_LN}' and Code ne '${info.id}'`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data.value
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  }
};

module.exports = { drivers };
