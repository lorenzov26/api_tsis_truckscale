const https = require("https");
const axios = require("axios");
const dotenv = require("dotenv");

const { hdbClient } = require("../../hdb/app");

dotenv.config();
require("tls").DEFAULT_MIN_VERSION = "TLSv1";

// ############

// base url for sap
const url = process.env.SAP_URL;

// base url for xsjs
const xsjs = process.env.XSJS_URL;

const actions = {
  actionsUpdate: async ({ info }) => {
    try {
      const { cookie, id } = info; // store cookie
      delete info.cookie; // remove cookie
      delete info.id; // remove id
      const res = await axios({
        method: "PATCH",
        url: `${url}/U_BFI_TS_ACT('${id}')`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const req = {
        status: res.status,
        msg: res.statusText
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  actionsSelectOne: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "GET",
        url: `${url}/$crossjoin(U_BFI_TS_ACT,U_BFI_TS_MOD,U_BFI_TS_USERS,EmployeesInfo)?$expand=U_BFI_TS_ACT($select=Code,U_TS_DESC,U_TS_STATUS),U_BFI_TS_MOD($select=Code,U_TS_DESC,U_TS_STATUS),U_BFI_TS_USERS($select=Code),EmployeesInfo($select=LastName,FirstName)&$filter=U_BFI_TS_ACT/U_TS_MOD_ID eq U_BFI_TS_MOD/Code and U_BFI_TS_ACT/U_TS_CREATEDBY eq U_BFI_TS_USERS/Code and U_BFI_TS_ACT/Code eq '${info.id}' and U_BFI_TS_USERS/U_APP_EMP_ID eq EmployeesInfo/EmployeeID`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data.value
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  actionsAdd: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_BFI_TS_ACT`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  actionsAddSelectByName: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie

      const res = await axios({
        method: "GET",
        url: `${url}/U_BFI_TS_ACT?$filter=U_TS_DESC eq '${info.U_TS_DESC}' and Code ne '${info.id}'`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      return res.data.value;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  actionsGetMaxCode: async ({ }) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@BFI_TS_ACT";`;

      const result = await new Promise(resolve => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  actionsAdd: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_BFI_TS_ACT`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  // actionsSelectAll: async ({ info }) => {
  //   try {
  //     const cookie = info.cookie; // store cookie
  //     delete info.cookie; // remove cookie

  //     let data = [];

  //     const request = async link => {
  //       let res;

  //       res = await axios({
  //         method: "GET",
  //         url: `${url}/${link}`,
  //         headers: {
  //           "Content-Type": "application/json",
  //           Cookie: `${cookie};`
  //         },
  //         httpsAgent: new https.Agent({ rejectUnauthorized: false })
  //       });

  //       const arr = res.data.value;

  //       for await (let i of arr) {
  //         data.push(i);
  //       }

  //       // loop always if there is next link
  //       while (res.data["odata.nextLink"]) {
  //         const nextPage = res.data["odata.nextLink"];
  //         await request(nextPage);
  //         break;
  //       }
  //     };

  //     await request(
  //       `$crossjoin(U_BFI_TS_ACT,U_BFI_TS_MOD,U_BFI_TS_USERS,EmployeesInfo)?$expand=U_BFI_TS_ACT($select=Code,U_TS_DESC,U_TS_STATUS,U_TS_CREATEDATE,U_TS_CREATETIME),U_BFI_TS_MOD($select=Code,U_TS_DESC,U_TS_STATUS),U_BFI_TS_USERS($select=Code),EmployeesInfo($select=LastName,FirstName)&$filter=U_BFI_TS_ACT/U_TS_MOD_ID eq U_BFI_TS_MOD/Code and U_BFI_TS_ACT/U_TS_CREATEDBY eq U_BFI_TS_USERS/Code and U_BFI_TS_USERS/U_APP_EMP_ID eq EmployeesInfo/EmployeeID`
  //     );

  //     return data;

  //   } catch (e) {
  //     const err = {
  //       status: e.response.status,
  //       msg: e.response.statusText,
  //       data: e.response.data
  //     };
  //     return err;
  //   }
  // }
  actionsSelectAll: async ({ info }) => {
    try {
      const client = await hdbClient();

      const sql = `select "a"."Code" as actionCode, "a"."U_TS_DESC" as actionDescription, "a"."U_TS_STATUS" as actionStatus, "a"."U_TS_CREATEDATE" as actionCreateDate, 
      "a"."U_TS_CREATETIME" as actionCreateTime,
      "b"."Code" as moduleCode, "b"."U_TS_DESC" as moduleDescription, "b"."U_TS_STATUS" as moduleStatus, 
      "d"."lastName", "d"."firstName" from "${process.env.DB}"."@BFI_TS_ACT" "a"
      left join "${process.env.DB}"."@BFI_TS_MOD" "b" on "a"."U_TS_MOD_ID" = "b"."Code" 
      left join "${process.env.DB}"."@BFI_TS_USERS" "c" on "a"."U_TS_CREATEDBY" = "c"."Code"
      left join "${process.env.reviveDB}"."OHEM" "d" on "c"."U_APP_EMP_ID" = "d"."empID"`;

      const result = await new Promise(resolve => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  }
};

module.exports = { actions };
