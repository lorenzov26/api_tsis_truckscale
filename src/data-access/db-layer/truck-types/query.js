const db = ({ dbs }) => {
  return Object.freeze({
    // insertTruckType,
    // selectAllTruckTypes,
    // selectOneTruckType,
    // updateTruckType,
    // selectTruckTypeAndModel, // for checking during add
    // selectTruckTypeAndModelUpdate // for checking during update
    selectOneTruckTypeByTypeAndModelForUpdate,
    insertTruckType,
    selectAllTruckTypes,
    updateTruckType,
    selectOneTruckTypeByTypeAndModelForAdd
  });

 async function selectOneTruckTypeByTypeAndModelForUpdate({info}) {
    const db = await dbs();
    const sql = `
    SELECT * FROM truck_types WHERE LOWER(type)=LOWER($1) AND LOWER(model)=LOWER($2) and id != $3
    `;
    const params = [info.truck_type, info.truck_model, info.id];
    return db.query(sql, params);
  }

  async function selectOneTruckTypeByTypeAndModelForAdd({info}) {
    const db = await dbs();
    const sql = `
    SELECT * FROM truck_types WHERE LOWER(type)=LOWER($1) AND LOWER(model)=LOWER($2)
    `;
    const params = [info.truck_type, info.truck_model];
    return db.query(sql, params);
  }

  async function selectAllTruckTypes({info }) {
    const db = await dbs();
    const sql = `
    SELECT * FROM truck_types
    `;
    const params = [];
    return db.query(sql);
  }

  async function insertTruckType({info}) {
    const db = await dbs();
    const sql = `
    INSERT INTO public.truck_types(
       type, model, size, created_at, updated_at, created_by, updated_by)
      VALUES ( $1, $2, $3, $4, $5, $6, $7);
    `;
    const params = [info.truck_type, info.truck_model, info.truck_size, info.dateToday, info.dateToday,
    info.created_by, info.created_by
    ];
    return db.query(sql, params);
  }

  async function updateTruckType({info}) {
    const db = await dbs();
    const sql = `
    UPDATE public.truck_types
    SET  type=$1, model=$2, size=$3, updated_at=$4, updated_by=$5
    WHERE id = $6;
    `;
    const params = [
      info.truck_type, info.truck_model, info.truck_size, info.dateToday, info.updated_by, info.id
    ];
    return db.query(sql, params);
  }

  
  // async function selectTruckTypeAndModelUpdate({ id, ...info }) {
  //   const db = await dbs();
  //   const sql = `SELECT * FROM ts_truck_types WHERE LOWER(truck_type)=LOWER($1) AND LOWER(truck_model)=LOWER($2) AND id <> $3;`;
  //   const params = [info.truck_type, info.truck_model, id];
  //   return db.query(sql, params);
  // }

  // async function selectTruckTypeAndModel({ ...info }) {
  //   const db = await dbs();
  //   const sql = `SELECT * FROM ts_truck_types WHERE truck_type=$1 AND truck_model=$2;`;
  //   const params = [info.truck_type, info.truck_model];
  //   return db.query(sql, params);
  // }

  // async function insertTruckType({ ...info }) {
  //   const db = await dbs();
  //   const sql = `INSERT INTO ts_truck_types (truck_type,truck_model,truck_size,created_at)
  //     VALUES ($1,$2,$3,$4)`;
  //   const params = [
  //     info.truck_type,
  //     info.truck_model,
  //     info.truck_size,
  //     info.created_at
  //   ];
  //   return db.query(sql, params);
  // }

  // async function selectAllTruckTypes() {
  //   const db = await dbs();
  //   const sql = `
  //     SELECT * FROM ts_truck_types
  //     `;
  //   return db.query(sql);
  // }

  // async function selectOneTruckType({ id }) {
  //   const db = await dbs();
  //   const sql = `
  //     SELECT * FROM ts_truck_types WHERE id = $1
  //     `;
  //   const params = [id];
  //   return db.query(sql, params);
  // }

  // async function updateTruckType({ id, ...info }) {
  //   const db = await dbs();
  //   const sql = `
  //     UPDATE ts_truck_types SET truck_type = $1, truck_model = $2, truck_size = $3, updated_at = $4
  //     WHERE id = $5
  //     `;
  //   const params = [
  //     info.truck_type,
  //     info.truck_model,
  //     info.truck_size,
  //     info.updated_at,
  //     id
  //   ];
  //   return db.query(sql, params);
  // }
};

module.exports = db;
