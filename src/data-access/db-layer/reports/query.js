const db = ({ dbs }) => {
  return Object.freeze({
    // selectAllCompletedTransaction
    selectAllReports,
    updateReport
  });

async function selectAllReports({info}) {
    const db = await dbs();
    const sql = `
    select a.id as transaction_id, b.name as transction_type, 
    c.first_name || ' ' || c.last_name as driver_name, d.plate_number, 
    e.model as truck_model, a.status as transaction_status,
    f.id as inbound_transaction_id, f.weight_type as inbound_transaction_weight_type,
    f.weight as inbound_weight, f.date_weight as inbound_date_weight,
    g.id as outbound_transaction_id, g.weight_type as outbound_transaction_weight_type,
    g.weight as outbound_weight, g.date_weight as outbound_date_weight, g.signature as 
    outbound_signature,
    (select q.first_name || ' ' || q.last_name from employees q where q.id = g.weigher) as weigher_name,
    h.name as supplier_name, a.supplier_address as supplier_address, a.number_of_bags, a.created_at as date_created,
    a.remarks, i.name as item, j.description as transaction_sub_type, c.first_name as driver_first_name, c.last_name as driver_last_name, k.name as unit_of_measure,
    k.id as uom_id, a.print_count
    from transactions a
    left join transaction_types b on a.transaction_type_id = b.id
    left join drivers c on a.driver_id = c.id
    left join truck_infos d on a.truck_info_id = d.id
    left join truck_types e on d.truck_type_id = e.id
    left join inbound_transactions f on f.transaction_id = a.id
    left join outbound_transactions g on g.transaction_id = a.id
    left join suppliers h on a.supplier = h.id
    left join items i on a.item = i.id
    left join transaction_sub_types j on a.transaction_sub_type = j.id
    left join unit_of_measures k on a.uom_id = k.id
    where TO_CHAR(a.created_at, 'yyyy-mm-dd') between $1 and $2
    order by a.created_at desc
    `;

    const params = [info.from, info.to];
    return db.query(sql, params);
  }

  async function updateReport({info}) {
    const db = await dbs();

    const sql = `
    UPDATE transactions set
	  driver_id=$1, truck_info_id=$2, number_of_bags=$3, supplier=$4, item=$5, updated_at=$6, updated_by=$7, supplier_address=$8, uom_id=$9, remarks=$10
	  WHERE id = $11;
    `;

    const params = [info.driver_id, info.truck_info_id, info.number_of_bags, info.supplier, info.item, info.dateToday, info.updated_by, info.supplier_address, info.uom_id, 
    info.remarks,
    info.id];
    return db.query(sql, params);
  }
  
  
  // async function selectAllCompletedTransaction(data) {
  //   const db = await dbs();
  //   const sql = `
  //     SELECT DISTINCT a.*,b.*,c.*
  //     FROM
  //     (
  //     SELECT DISTINCT a.supplier_name, a.supplier_address, c.firstname as wfirstname, c.lastname as wlastname, tst.created_by, b.description, tst.id,tstt.transaction_type_name,tst.purchase_order_id,tst.tracking_code,tst.transmittal_number,
  //     tst.no_of_bags, tsd.firstname,tsd.lastname,tsti.plate_number,tst.created_at,tst.status
  //     FROM ts_transactions tst 
      
  //     LEFT JOIN ts_users c ON tst.created_by = c.id
  //     LEFT JOIN ts_po a ON tst.purchase_order_id = a.id
  //     LEFT JOIN ts_raw_materials b ON a.raw_material_id = b.id

  //     LEFT JOIN ts_transaction_types tstt ON tstt.id = tst.transaction_type_id
  //     LEFT JOIN ts_inbound_transactions tsi ON tsi.transaction_id = tst.id LEFT JOIN ts_drivers tsd ON tsd.id = tsi.drivers_id
  //     LEFT JOIN ts_truck_infos tsti ON tsti.id = tsi.truck_info_id
  //     )a LEFT JOIN
  //     (
  //       SELECT tsi.id inbound_id,tsi.transaction_id transaction_id_ib, tsi.ib_weight,tsi.ib_timestamp
  //       FROM ts_inbound_transactions tsi LEFT JOIN ts_transactions tst ON tst.id = tsi.transaction_id
  //     )b ON b.transaction_id_ib = a.id LEFT JOIN
  //     (
  //       SELECT tso.id outbound_id,tso.transaction_id transaction_id_ob, tso.ob_weight,tso.ob_timestamp,tso.inbound_id ob_inbound_id
  //       FROM ts_outbound_transactions tso LEFT JOIN ts_transactions tst ON tst.id = tso.transaction_id
  //     )c ON c.transaction_id_ob = a.id 
  //     WHERE LOWER(a.status)='completed' AND TO_CHAR(a.created_at, 'yyyy-mm-dd') BETWEEN $1 AND $2
  //     ORDER BY a.id DESC;
  //   `;
  //   const params = [data.from, data.to];
  //   return db.query(sql, params);
  // }
};

module.exports = db;
