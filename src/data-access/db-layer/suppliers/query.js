const db = ({ dbs }) => {
  return Object.freeze({
    // insertNewSupplier,
    // selectByName,
   
    // selectOneSupplier,
    // selectByNameUpdate,
    // updateSupplier
    selectAllSuppliers,
    selectOneSupplierByName,
    insertSupplier,
    insertSupplierWithAddress
    
  });

   async function selectAllSuppliers({}) {
    const db = await dbs();
    const sql = `SELECT * FROM suppliers where is_active = true;`;
    return db.query(sql);
  }

  async function selectOneSupplierByName({info}) {
    const db = await dbs();
    const sql = `SELECT * FROM suppliers where LOWER(name) = LOWER($1)`;
    const params = [info.supplier]
    return db.query(sql, params);
  }

  async function insertSupplier({info}) {
    const db = await dbs();
    const sql = `
    INSERT INTO public.suppliers(
       name,  created_at, updated_at, created_by, updated_by)
      VALUES ($1, $2, $3, $4, $5) returning id
    `;
    const params = [info.supplier, info.dateToday,  info.dateToday, info.createdby, info.createdby]
    return db.query(sql, params);
  }

  async function insertSupplierWithAddress({info}) {
    const db = await dbs();
    const sql = `
    INSERT INTO public.suppliers(
       name, address,  created_at, updated_at, created_by, updated_by)
      VALUES ($1, $2, $3, $4, $5, $6) returning id
    `;
    const params = [info.supplier, info.address, info.dateToday,  info.dateToday, info.createdby, info.createdby]
    return db.query(sql, params);
  }



  // add new supplier
  // async function insertNewSupplier({ ...info }) {
  //   const db = await dbs();
  //   const sql = `INSERT INTO ts_suppliers (name,address,contact_number)
  //   VALUES ($1,$2,$3);`;
  //   const params = [info.name, info.address, info.contact_number];
  //   return db.query(sql, params);
  // }
  // // check if supplier name exist
  // async function selectByName({ ...info }) {
  //   const db = await dbs();
  //   const sql = `SELECT * FROM ts_suppliers WHERE name = $1;`;
  //   const params = [info.name];
  //   return db.query(sql, params);
  // }
  // // select all suppliers
  // async function selectAllSupplier() {
  //   const db = await dbs();
  //   const sql = `SELECT * FROM ts_suppliers;`;
  //   return db.query(sql);
  // }
  // // select one supplier
  // async function selectOneSupplier({ id }) {
  //   const db = await dbs();
  //   const sql = `SELECT * FROM ts_suppliers WHERE id = $1;`;
  //   const params = [id];
  //   return db.query(sql, params);
  // }
  // // select name during update
  // async function selectByNameUpdate({ id, ...info }) {
  //   const db = await dbs();
  //   const sql = `SELECT * FROM ts_suppliers WHERE name = $1 AND id <> $2;`;
  //   const params = [info.name, id];
  //   return db.query(sql, params);
  // }
  // // update supplier
  // async function updateSupplier({ id, ...info }) {
  //   const db = await dbs();
  //   const sql = `UPDATE ts_suppliers SET name=$1, address=$2, contact_number=$3 WHERE id=$4;`;
  //   const params = [info.name, info.address, info.contact_number, id];
  //   return db.query(sql, params);
  // }
};

module.exports = db;
