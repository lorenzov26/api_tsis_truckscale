const { makeDb } = require("../app");
const db = require("./query");

const transactionDb = makeDb({ db });

module.exports = transactionDb;
