const { makeDb } = require("../app");
const db = require("./query");

const transactionTypeDb = makeDb({ db });

module.exports = transactionTypeDb;
