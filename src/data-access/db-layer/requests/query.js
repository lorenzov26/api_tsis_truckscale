const db = ({ dbs }) => {
    return Object.freeze({
        addRequest,
        selectAllRequests,
        updateRequestStatus,
        selectOneRequestById,
        addRequestNotif,
        getRequestNotifsByReceiverId,
        updateNotifIsRead,
        selectOneRequestByTransactionIdThatIsPending
    });

    async function addRequest({ info }) {
        const db = await dbs();
        const sql = `
        INSERT INTO public.requests(
             request_reason, transaction_id, print_count, created_by, created_at, updated_by, updated_at, status, is_edit, is_reprint, draft_data)
            VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) returning id, updated_by;
      
        `;
        const params = [info.reason, info.transactionId, info.printCount, info.created_by, info.dateToday, info.created_by, info.dateToday, info.status, info.isEdit, info.isReprint,
        info.draft
        ];
        return db.query(sql, params);
    }

    async function addRequestNotif({ info }) {
        const db = await dbs();
        const sql = `
        INSERT INTO public.requests_notifs(
            request_id, receiver_id, request_status, date, sender_id)
            VALUES ( $1, $2, $3, $4, $5);
      
        `;
        const params = [info.request_id, info.receiver_id, info.request_status, info.date, info.sender_id];
        return db.query(sql, params);
    }


    async function getRequestNotifsByReceiverId({ info }) {
        const db = await dbs();
        const sql = `
        select a.*, b.first_name, b.last_name, c.transaction_id, c.created_by from requests_notifs a
        left join employees b on a.sender_id = b.id 
        left join requests c on a.request_id = c.id
        where a.receiver_id = $1 and date > $2
        order by date desc
        `;
        const params = [info.id, info.date];
        return db.query(sql, params);
    }

    async function updateNotifIsRead({ info }) {
        const db = await dbs();
        const sql = `
        UPDATE public.requests_notifs set
	    is_read=$1
	    WHERE id=$2;
        `;
        const params = [info.is_read, info.id];
        return db.query(sql, params);
    }

    async function selectAllRequests({ info }) {
        const db = await dbs();
        const sql = `
        select a.*, b.first_name || ' ' || b.last_name as requester_name from requests a 
        left join employees b on a.created_by = b.id
        where TO_CHAR(a.created_at, 'yyyy-mm-dd') between $1 and $2  
        order by a.created_at desc
        `;
        const params = [info.from, info.to];
        return db.query(sql, params);
    }

    async function updateRequestStatus({ info }) {
        const db = await dbs();
        const sql = `
        UPDATE public.requests
	    set updated_by=$1, updated_at=$2, status=$3
	    WHERE id = $4 returning id, created_by;
        `;
        const params = [info.updated_by, info.dateToday, info.status, info.id];
        return db.query(sql, params);
    }

    async function selectOneRequestById({ info }) {
        const db = await dbs();
        const sql = `
        select * from requests where id = $1
        `;
        const params = [info.id];
        return db.query(sql, params);
    }

    async function selectOneRequestByTransactionIdThatIsPending({ info }) {
        const db = await dbs();
        const sql = `
        select * from requests 
        where transaction_id = $1 and status in ('pending', 'validated')
        `;
        const params = [info.id];
        return db.query(sql, params);
    }



};

module.exports = db;
