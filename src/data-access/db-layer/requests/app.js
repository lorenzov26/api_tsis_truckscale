const { makeDb } = require("../app");
const db = require("./query");

const requestDb = makeDb({ db });

module.exports = requestDb;
