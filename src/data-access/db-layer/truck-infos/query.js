const db = ({ dbs }) => {
  return Object.freeze({
    // selectOneTruckInfoByPlateNumber, // check during add
    // selectOneTruckInfoByPlateNumberUpdate, // check during update
    // insertTruckInfo,
    // selectAllTruckInfos,
    // selectOneTruckInfo,
    // updateTruckInfo
    selectOneTruckInfosByPlateNumberForAdd,
    insertTruckInfos,
    selectAllTruckInfos,
    updateTruckInfo,
    selectOneTruckInfosByPlateNumberForUpdate,
    selectTruckInfoByPlateNumber,
    insertTruckInfosForAutoAdd
  });

 
   async function selectOneTruckInfosByPlateNumberForAdd({ info }) {
    const db = await dbs();
    const sql = `
      SELECT * FROM truck_infos WHERE LOWER(plate_number) = LOWER($1)
      `;
    const params = [info.plate_number];
    return db.query(sql, params);
  }

  async function selectOneTruckInfosByPlateNumberForUpdate({ info }) {
    const db = await dbs();
    const sql = `
      SELECT * FROM truck_infos WHERE LOWER(plate_number) = LOWER($1) and id != $2
      `;
    const params = [info.plate_number, info.id];
    return db.query(sql, params);
  }

  async function updateTruckInfo({ info }) {
    const db = await dbs();
    const sql = `
    UPDATE public.truck_infos
    SET  truck_type_id=$1, plate_number=UPPER($2), updated_at=$3,  updated_by=$4
    WHERE id = $5
      `;
    const params = [info.truck_type_id, info.plate_number, info.dateToday, info.updated_by, info.id];
    return db.query(sql, params);
  }


  async function insertTruckInfos({ info }) {
    const db = await dbs();
    const sql = `
    INSERT INTO public.truck_infos(
    truck_type_id, plate_number, created_at, updated_at, created_by, updated_by)
      VALUES ( $1, UPPER($2), $3, $4, $5, $6);
      `;
    const params = [info.truck_type_id, info.plate_number, info.dateToday, info.dateToday, info.created_by, info.created_by];
    return db.query(sql, params);
  }

  
  async function insertTruckInfosForAutoAdd({ info }) {
    const db = await dbs();
    const sql = `
    INSERT INTO public.truck_infos(
    truck_type_id, plate_number, created_at, updated_at, created_by, updated_by)
      VALUES ( $1, UPPER($2), $3, $4, $5, $6) returning id;
      `;
    const params = [0, info.truck_info_id, info.dateToday, info.dateToday, info.createdby, info.createdby];
    return db.query(sql, params);
  }

  async function selectAllTruckInfos({}) {
    const db = await dbs();
    const sql = `
    select a.id as truck_info_id, a.plate_number, b.type, b.model, b.id as truck_type_id  from truck_infos a 
    left join truck_types b on a.truck_type_id = b.id
      `;
    const params = [];
    return db.query(sql);
  }

  async function selectTruckInfoByPlateNumber({info}) {
    const db = await dbs();
    const sql = `
    select * from truck_infos where LOWER(plate_number) = LOWER($1)
      `;
    const params = [info.truck_info_id];
    return db.query(sql, params);
  }


  // async function selectOneTruckInfoByPlateNumberUpdate({ id, plate_number }) {
  //   const db = await dbs();
  //   const sql = `
  //     SELECT * FROM ts_truck_infos WHERE plate_number = $1 AND id <> $2;
  //     `;
  //   const params = [plate_number, id];
  //   return db.query(sql, params);
  // }
  // async function selectOneTruckInfoByPlateNumber({ plate_number }) {
  //   const db = await dbs();
  //   const sql = `
  //     SELECT * FROM ts_truck_infos WHERE plate_number = $1
  //     `;
  //   const params = [plate_number];
  //   return db.query(sql, params);
  // }

  // async function updateTruckInfo({ id, ...info }) {
  //   const db = await dbs();
  //   const sql = `
  //     UPDATE ts_truck_infos SET truck_type_id = $1, plate_number = $2, updated_at = $3
  //     WHERE id = $4
  //     `;
  //   const params = [info.truck_type_id, info.plate_number, info.updated_at, id];
  //   return db.query(sql, params);
  // }

  // async function selectOneTruckInfo({ id }) {
  //   const db = await dbs();
  //   const sql = `
  //   SELECT tti.id truckinfo_id,tti.plate_number,ttt.id trucktype_id, ttt.truck_type,ttt.truck_model,ttt.truck_size 
  //   FROM ts_truck_infos tti LEFT JOIN ts_truck_types ttt
  //   ON ttt.id = tti.truck_type_id WHERE tti.id =$1;
  //     `;
  //   const params = [id];
  //   return db.query(sql, params);
  // }

  // async function selectAllTruckInfos() {
  //   const db = await dbs();
  //   const sql = `
  //   SELECT tti.id truckinfo_id,tti.plate_number,ttt.id trucktype_id, ttt.truck_type,ttt.truck_model,ttt.truck_size 
  //   FROM ts_truck_infos tti LEFT JOIN ts_truck_types ttt
  //   ON ttt.id = tti.truck_type_id;
  //     `;
  //   return db.query(sql);
  // }

  // async function insertTruckInfo({ ...info }) {
  //   const db = await dbs();
  //   const sql = `INSERT INTO ts_truck_infos (truck_type_id,plate_number,created_at)
  //     VALUES ($1,$2,$3)`;
  //   const params = [info.truck_type_id, info.plate_number, info.created_at];
  //   return db.query(sql, params);
  // }
};

module.exports = db;
