const { makeDb } = require("../app");
const db = require("./query");

const truckInfosDb = makeDb({ db });

module.exports = truckInfosDb;
