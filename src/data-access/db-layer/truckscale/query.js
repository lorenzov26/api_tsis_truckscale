const db = ({ dbs }) => {
  return Object.freeze({
    // insertNewTruckScale,
    // selectTruckScaleName,
    // deleteExistingData,
    // selectAllTruckScale,
    // selectOneTruckScale,
    // updateTruckScale,
    // selectTruckScaleNameUpdate,
    selectOneTruckScaleName,
    addTruckscale,
    selectAllTruckscales,
    updateTruckscale
  });

   async function selectOneTruckScaleName({info}) {
    const db = await dbs();
    const sql =
      "SELECT * FROM truckscales WHERE LOWER(truckscale_name) = LOWER($1);";
    const params = [info.truckscale_name];
    return db.query(sql, params);
  }

  async function addTruckscale({info}) {
    const db = await dbs();
    const sql =
      `
      INSERT INTO public.truckscales(
        truckscale_name, truckscale_location, truckscale_length, truckscale_ip, 
        truckscale_device, truckscale_baudrate, truckscale_parity, truckscale_databits, truckscale_stopbits, truckscale_id)
        VALUES ( $1, $2, $3, $4, $5, $6, $7, $8, $9, $10);
      `;
    const params = [info.truckscale_name, info.truckscale_location, info.truckscale_length, info.truckscale_ip,
    info.truckscale_device, info.truckscale_baudrate, info.truckscale_parity, info.truckscale_databits, info.truckscale_stopbits, info.truckscale_id
    ];
    return db.query(sql, params);
  }

  async function selectAllTruckscales({info}) {
    const db = await dbs();
    const sql =
      "select * from truckscales";
    const params = [];
    return db.query(sql);
  }

  async function updateTruckscale({ info }) {
    const db = await dbs();
    const sql =
      `
      UPDATE public.truckscales
      set truckscale_name=$1, truckscale_location=$2, truckscale_length=$3, truckscale_ip=$4, truckscale_device=$5, truckscale_baudrate=$6, 
      truckscale_parity=$7, truckscale_databits=$8, truckscale_stopbits=$9, truckscale_id = $10
	    WHERE id = $11;
      `;
    const params = [info.truckscale_name, info.truckscale_location, info.truckscale_length, info.truckscale_ip,
    info.truckscale_device, info.truckscale_baudrate, info.truckscale_parity, info.truckscale_databits, info.truckscale_stopbits, info.truckscale_id, info.id];
    return db.query(sql, params);
  }



  // add new driver
  // async function insertNewTruckScale({ ...info }) {
  //   const db = await dbs();
  //   const sql =
  //     "INSERT INTO ts_truckscales (truckscale_name,truckscale_location,truckscale_length,ip_address,device_name,created_at, " +
  //     "baudrate,parity,databits,stopbits) VALUES ($1,$2,$3,$4,$5,$10,$6,$7,$8,$9);";
  //   const params = [
  //     info.truckscale_name,
  //     info.truckscale_location,
  //     info.truckscale_length,
  //     info.ip_address,
  //     info.device_name,
  //     info.baudrate,
  //     info.parity,
  //     info.databits,
  //     info.stopbits,
  //     info.created_at
  //   ];
  //   return db.query(sql, params);
  // }
  // // select by truckscale name to check if exist
  // async function selectTruckScaleName({ ...info }) {
  //   const db = await dbs();
  //   const sql =
  //     "SELECT * FROM ts_truckscales WHERE LOWER(truckscale_name) = LOWER($1);";
  //   const params = [info.truckscale_name];
  //   return db.query(sql, params);
  // }
  // // delete data of truckscale
  // async function deleteExistingData({ ...info }) {
  //   const db = await dbs();
  //   const sql =
  //     "DELETE FROM ts_truckscales WHERE LOWER(device_name) = LOWER($1) AND ip_address=$2;";
  //   const params = [info.device_name, info.ip_address];
  //   return db.query(sql, params);
  // }
  // // select all truckscale
  // async function selectAllTruckScale({}) {
  //   const db = await dbs();
  //   const sql = "SELECT * FROM ts_truckscales;";
  //   return db.query(sql);
  // }
  // // select one truckscale
  // async function selectOneTruckScale({ id }) {
  //   const db = await dbs();
  //   const sql = "SELECT * FROM ts_truckscales WHERE id = $1;";
  //   const params = [id];
  //   return db.query(sql, params);
  // }
  // // update truckscale
  // async function updateTruckScale({ id, ...info }) {
  //   const db = await dbs();
  //   const sql = `UPDATE ts_truckscales SET truckscale_name=$1, truckscale_location=$2, truckscale_length=$3, ip_address=$4,
  //   device_name=$5, baudrate=$6, parity=$7, databits=$8, stopbits=$9, updated_at=$10 WHERE id=$11;`;
  //   const params = [
  //     info.truckscale_name,
  //     info.truckscale_location,
  //     info.truckscale_length,
  //     info.ip_address,
  //     info.device_name,
  //     info.baudrate,
  //     info.parity,
  //     info.databits,
  //     info.stopbits,
  //     info.updated_at,
  //     id
  //   ];
  //   return db.query(sql, params);
  // }
  // // check if name exist during update
  // async function selectTruckScaleNameUpdate({ id, ...info }) {
  //   const db = await dbs();
  //   const sql =
  //     "SELECT * FROM ts_truckscales WHERE LOWER(truckscale_name) = LOWER($1) AND id <> $2;";
  //   const params = [info.truckscale_name, id];
  //   return db.query(sql, params);
  // }
};

module.exports = db;
