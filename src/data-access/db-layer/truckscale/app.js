const { makeDb } = require("../app");
const db = require("./query");

const truckScaleDb = makeDb({ db });

module.exports = truckScaleDb;
