const { makeDb } = require("../app");
const db = require("./query");

const actionsDb = makeDb({ db });

module.exports = actionsDb;
