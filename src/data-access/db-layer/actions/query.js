const db = ({ dbs }) => {
  return Object.freeze({
    insertNewAction,
    selectActionName,
    selectActionNameUpdate,
    updateAction,
    selectAllActions,
    selectOneAction,
    returnCreatedBy,
    returnModule //get module where the action belongs
  });
  async function returnModule({ id }) {
    const db = await dbs();
    const sql = `SELECT id,descriptions,status FROM ts_modules WHERE id=$1;`;
    const params = [id];
    return db.query(sql, params);
  }
  // get the name of the user who created the role
  async function returnCreatedBy({ id }) {
    const db = await dbs();
    const sql = `SELECT id,employee_id,firstname,lastname
    FROM ts_users WHERE id = $1;`;
    const params = [id];
    return db.query(sql, params);
  }
  // add new action
  async function insertNewAction({ ...info }) {
    const db = await dbs();
    const sql = `INSERT INTO ts_actions (modules_id,description,status,created_at,created_by)
    VALUES ($1,$2,$3,$5,$4)`;
    const params = [
      info.module_id,
      info.description,
      info.status,
      info.created_by,
      info.created_at
    ];
    return db.query(sql, params);
  }
  //   select name to check if exist
  async function selectActionName({ ...info }) {
    const db = await dbs();
    const sql = `SELECT * FROM ts_actions WHERE modules_id = $1 AND LOWER(description) = LOWER($2);`;
    const params = [info.module_id, info.description];
    return db.query(sql, params);
  }
  // select action name during update to check if exist
  async function selectActionNameUpdate({ id, ...info }) {
    const db = await dbs();
    const sql = `SELECT * FROM ts_actions WHERE modules_id = $1 AND LOWER(description) = LOWER($2) AND id <> $3;`;
    const params = [info.module_id, info.description, id];
    return db.query(sql, params);
  }
  // update action
  async function updateAction({ id, ...info }) {
    const db = await dbs();
    const sql = `UPDATE ts_actions SET modules_id = $1, description = $2, status = $3, updated_at = $6,
    modified_by = $4 WHERE id = $5;`;
    const params = [
      info.module_id,
      info.description,
      info.status,
      info.modified_by,
      id,
      info.updated_at
    ];
    return db.query(sql, params);
  }
  // select all actions
  async function selectAllActions() {
    const db = await dbs();
    const sql = `SELECT a.actions_id,a.description,a.status,c.modules_id,c.descriptions,c.status modules_status,a.created_at,
        a.firstname create_fn,a.middlename create_mn,a.lastname create_ln,
        b.firstname modify_fn, b.middlename modify_mn, b.lastname modify_ln, b.updated_at
        FROM 
        (
          SELECT tsa.id AS actions_id,description,status,tsa.created_at,tsu.id AS user_id,email,firstname,middlename,lastname 
          FROM ts_actions tsa LEFT JOIN ts_users tsu ON tsu.id = tsa.created_by
        )a LEFT JOIN
        (
          SELECT tsa.id AS actions_id,description,status,tsa.updated_at,tsu.id AS user_id,email,firstname,middlename,lastname 
          FROM ts_actions tsa LEFT JOIN ts_users tsu ON tsu.id = tsa.modified_by
        )b ON b.actions_id = a.actions_id LEFT JOIN
        (
          SELECT tsa.id actions_id,tsm.id modules_id,tsm.descriptions,tsm.status FROM ts_actions tsa 
          LEFT JOIN ts_modules tsm ON tsm.id = tsa.modules_id
        )c ON c.actions_id = a.actions_id;`;
    return db.query(sql);
  }
  // select single action
  async function selectOneAction({ id }) {
    const db = await dbs();
    const sql = `SELECT a.actions_id,a.description,a.status,c.modules_id,c.descriptions,c.status modules_status,a.created_at,
    a.firstname create_fn,a.middlename create_mn,a.lastname create_ln,
    b.firstname modify_fn, b.middlename modify_mn, b.lastname modify_ln, b.updated_at
    FROM 
    (
      SELECT tsa.id AS actions_id,description,status,tsa.created_at,tsu.id AS user_id,email,firstname,middlename,lastname 
      FROM ts_actions tsa LEFT JOIN ts_users tsu ON tsu.id = tsa.created_by
    )a LEFT JOIN
    (
      SELECT tsa.id AS actions_id,description,status,tsa.updated_at,tsu.id AS user_id,email,firstname,middlename,lastname 
      FROM ts_actions tsa LEFT JOIN ts_users tsu ON tsu.id = tsa.modified_by
    )b ON b.actions_id = a.actions_id LEFT JOIN
    (
      SELECT tsa.id actions_id,tsm.id modules_id,tsm.descriptions,tsm.status FROM ts_actions tsa 
      LEFT JOIN ts_modules tsm ON tsm.id = tsa.modules_id
    )c ON c.actions_id = a.actions_id WHERE a.actions_id = $1;`;
    const params = [id];
    return db.query(sql, params);
  }
};

module.exports = db;
