const { makeDb } = require("../app");
const db = require("./query");

const usersDb = makeDb({ db });

module.exports = usersDb;
