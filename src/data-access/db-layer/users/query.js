const db = ({ dbs }) => {
  return Object.freeze({
    selectAllActivityLogs,
    addUser,
    selectEmployeeByIdAndPassword,
    updateEmployeeTokenAndLoginCount,
    selectTruckScaleByIpandDeviceName,
    getTokenOfUser,
    resetPasswords,
    selectUserByPin,
    selectAllUsers,
    resetLoginCountAndPassword,
    selectUserByFirstnameAndLastname,
    updateUser,
    selectAllPrintLogs,
    updateSetting,
    selectEmployeeByRole,
    selectEmployeeById
  });
  // select all activity logs

  async function updateUser({ info }) {
    const db = await dbs();
    const sql = `
    UPDATE public.employees SET
	  first_name=$1, last_name=$2, role_id=$3, middle_name=$4, updated_by = $5, updated_at = $6, email = $7
	WHERE employee_id = $8;
    `;
    const params = [info.first_name, info.last_name, info.role, info.middle_name, info.updated_by, info.dateToday, info.email, info.employee_id,];
    return db.query(sql, params);
  }

  async function updateSetting({ info }) {
    const db = await dbs();
    const sql = `
    UPDATE public.transaction_types
	SET print_count=$1, updated_by=$2, updated_at=$3
	WHERE id = $4;
    
    `;
    const params = [info.print_count, info.updated_by, info.dateToday, info.id];
    return db.query(sql, params);
  }


  async function addUser({ info }) {
    const db = await dbs();
    const sql = `
    INSERT INTO public.employees(
      employee_id, first_name, last_name, password, login_count, role_id, middle_name, created_by, created_at, updated_by, updated_at, email)
      VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12);
    `;
    const params = [info.employee_id, info.first_name, info.last_name, info.password, info.login_count,
    info.role, info.middle_name, info.created_by, info.dateToday, info.created_by, info.dateToday, info.email
    ];
    return db.query(sql, params);
  }

  async function selectUserByFirstnameAndLastname({ info }) {
    const db = await dbs();
    const sql = `
    select * from employees where employee_id = $1
    `;
    const params = [info.employee_id];
    return db.query(sql, params);
  }

  async function selectEmployeeByIdAndPassword({ info }) {
    const db = await dbs();
    const sql = `
    SELECT a.*, b.name as user_role FROM public.employees a
    left join roles b on a.role_id = b.id
    WHERE employee_id = $1 and password = $2;
    `;
    const params = [info.employee_id, info.password];
    return db.query(sql, params);
  }


  async function selectEmployeeByRole({ info }) {
    const db = await dbs();
    const sql = `
    select a.id, b.name from employees  a
    left join roles b on a.role_id = b.id
    where b.name = $1
    `;
    const params = [info.role];
    return db.query(sql, params);
  }

  async function selectEmployeeById({ info }) {
    const db = await dbs();
    const sql = `
    select * from employees where id = $1
    `;
    const params = [info.id];
    return db.query(sql, params);
  }

  async function selectUserByPin({ info }) {
    const db = await dbs();
    const sql = `
    select * from employees where id = $1 and pin = $2
    `;
    const params = [info.id, info.pin];
    return db.query(sql, params);
  }


  async function updateEmployeeTokenAndLoginCount({ info }) {
    const db = await dbs();
    const sql = `UPDATE public.employees SET token=$1, updated_by = $4, login_count = 
    CASE
    WHEN login_count > 0 THEN  login_count + 1
    ELSE 0 
    END
    WHERE employee_id=$2 AND password=$3;`;
    const params = [info.token, info.employee_id, info.password, info.employeeDbId];
    return db.query(sql, params);
  }

  async function selectTruckScaleByIpandDeviceName({ ...info }) {
    const db = await dbs();
    const sql = `SELECT * FROM truckscales WHERE truckscale_ip = $1 AND truckscale_device = $2;`;
    const params = [info.ip, info.device_name];
    return db.query(sql, params);
  }

  async function getTokenOfUser({ ...info }) {
    const db = await dbs();
    const sql = `select * from employees where employee_id = $1
    and password = $2`;
    const params = [info.employee_id, info.password];
    return db.query(sql, params);
  }

  async function resetPasswords({ id, ...info }) {
    const db = await dbs();
    const sql = `UPDATE employees SET password = $1,token = NULL, login_count = 1, pin = $3 WHERE id = $2;`;
    const params = [info.password, id, info.pin];
    return db.query(sql, params);
  }

  async function resetLoginCountAndPassword({ info }) {
    const db = await dbs();
    const sql = `UPDATE employees SET password = $1, login_count = 0 WHERE id = $2;`;
    const params = [info.employeeId, info.id];
    return db.query(sql, params);
  }

  async function selectAllUsers({ }) {
    const db = await dbs();
    const sql = `
    select a.*, b.name as role_name from employees a
    left join roles b on a.role_id = b.id
    `;
    const params = [];
    return db.query(sql);
  }


  async function selectAllActivityLogs(dateRange) {
    const db = await dbs();
    const sql = `
    select a.*, b.first_name, b.last_name from activity_logs a 
    left join employees b on a.employee_id = b.id 
    WHERE TO_CHAR(a.log_timestamp , 'yyyy-mm-dd') BETWEEN $1 AND $2
    order by a.log_timestamp desc
    `;
    const params = [dateRange.date_from, dateRange.date_to];
    return db.query(sql, params);
  }

  async function selectAllPrintLogs(dateRange) {
    const db = await dbs();
    const sql = `
    select a.id as print_log_id, a.transaction_id, a.date as date_printed, b.first_name, b.last_name,
    b.employee_id, d.name as transaction_type
    from print_logs a 
    left join employees b on a.employee_id = b.id
    left join transactions c on a.transaction_id = c.id
    left join transaction_types d on c.transaction_type_id = d.id
    WHERE TO_CHAR(a.date , 'yyyy-mm-dd') BETWEEN $1 AND $2
    order by a.date desc
    `;
    const params = [dateRange.date_from, dateRange.date_to];
    return db.query(sql, params);
  }
};

module.exports = db;
