const db = ({ dbs }) => {
  return Object.freeze({
    insertModule,
    selectModule,
    selectModuleUpdate,
    updateModule,
    selectAllModules,
    selectSingleModule,
    selectAllModulesWithActions,
    returnCreatedBy
  });
  // get the name of the user who created the role
  async function returnCreatedBy({ id }) {
    const db = await dbs();
    const sql = `SELECT id,employee_id,firstname,lastname
    FROM ts_users WHERE id = $1;`;
    const params = [id];
    return db.query(sql, params);
  }
  // add new module
  async function insertModule({ ...info }) {
    const db = await dbs();
    const sql =
      "INSERT INTO ts_modules (descriptions,status,created_at,created_by) " +
      "VALUES ($1,$2,$3,$4)";
    const params = [
      info.description,
      info.status,
      info.created_at,
      info.created_by
    ];
    return db.query(sql, params);
  }
  // select description to check if exist
  async function selectModule({ ...info }) {
    const db = await dbs();
    const sql =
      "SELECT * FROM ts_modules WHERE LOWER(descriptions) = LOWER($1);";
    const params = [info.description];
    return db.query(sql, params);
  }
  // select description to check if exist on update
  async function selectModuleUpdate({ id, ...info }) {
    const db = await dbs();
    const sql =
      "SELECT * FROM ts_modules WHERE LOWER(descriptions) = LOWER($1) AND id <> $2;";
    const params = [info.description, id];
    return db.query(sql, params);
  }
  // update existing module
  async function updateModule({ id, ...info }) {
    const db = await dbs();
    const sql =
      "UPDATE ts_modules SET descriptions=$1, status=$2, updated_at=$5, modified_by=$3 WHERE id=$4;";
    const params = [
      info.description,
      info.status,
      info.modified_by,
      id,
      info.updated_at
    ];
    return db.query(sql, params);
  }
  // select all modules
  async function selectAllModules() {
    const db = await dbs();
    const sql = `SELECT a.module_id,a.descriptions,a.status,a.created_at,a.firstname create_fn,a.middlename create_mn,a.lastname create_ln,
    b.firstname modify_fn, b.middlename modify_mn, b.lastname modify_ln, b.updated_at
    FROM 
    (
      SELECT tsm.id AS module_id,descriptions,status,tsm.created_at,tsu.id AS user_id,email,firstname,middlename,lastname 
      FROM ts_modules tsm LEFT JOIN ts_users tsu ON tsu.id = tsm.created_by
    )a LEFT JOIN
    (
      SELECT tsm.id AS module_id,descriptions,status,tsm.updated_at,tsu.id AS user_id,email,firstname,middlename,lastname 
      FROM ts_modules tsm LEFT JOIN ts_users tsu ON tsu.id = tsm.modified_by
    )b ON b.module_id = a.module_id;`;
    return db.query(sql);
  }
  // select single module
  async function selectSingleModule({ id }) {
    const db = await dbs();
    const sql = `SELECT a.module_id,a.descriptions,a.status,a.created_at,a.firstname create_fn,a.middlename create_mn,a.lastname create_ln,
    b.firstname modify_fn, b.middlename modify_mn, b.lastname modify_ln, b.updated_at
    FROM 
    (
      SELECT tsm.id AS module_id,descriptions,status,tsm.created_at,tsu.id AS user_id,email,firstname,middlename,lastname 
      FROM ts_modules tsm LEFT JOIN ts_users tsu ON tsu.id = tsm.created_by
    )a LEFT JOIN
    (
      SELECT tsm.id AS module_id,descriptions,status,tsm.updated_at,tsu.id AS user_id,email,firstname,middlename,lastname 
      FROM ts_modules tsm LEFT JOIN ts_users tsu ON tsu.id = tsm.modified_by
    )b ON b.module_id = a.module_id  WHERE a.module_id = $1;`;
    const params = [id];
    return db.query(sql, params);
  }
  async function selectAllModulesWithActions() {
    const db = await dbs();
    const sql = `
    select b.id as module_id, b.name as module_name, b.is_active as module_status, a.id as action_id, a.name as action_name,
    a.is_active as action_status
    from actions a
    left join modules b on a.module_id = b.id
    `;
    return db.query(sql);
  }
};

module.exports = db;
