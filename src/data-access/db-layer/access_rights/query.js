const db = ({ dbs }) => {
  return Object.freeze({
    insertAccessRights,
    deleteAccessRightsOnRole,
    selectAccessRightsOnRole
  });
  // add new action
  async function insertAccessRights({ ...info }) {
    const db = await dbs();
    const sql = `INSERT INTO ts_access_rights (actions_id,roles_id,created_at)
    VALUES ($1,$2,$3);`;
    const params = [info.actions_id, info.roles_id, info.created_at];
    return db.query(sql, params);
  }
  // to delete all access rights base on role; then insert
  async function deleteAccessRightsOnRole({ ...info }) {
    const db = await dbs();
    const sql = `DELETE FROM ts_access_rights WHERE roles_id = $1;`;
    const params = [info.roles_id];
    return db.query(sql, params);
  }
  // select all modules and actions base on role id
  async function selectAccessRightsOnRole({ id }) {
    const db = await dbs();
    const sql = `
    select c.id as module_id, c.name as module_name, c.is_active as module_status, b.id as action_id, b.name as action_name, b.is_active as action_status  
    from access_rights a 
    left join actions b on a.action_id = b.id
    left join modules c on b.module_id = c.id
    where role_id = $1
    `;
    const params = [id];
    return db.query(sql, params);
  }
};

module.exports = db;
