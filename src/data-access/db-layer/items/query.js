const db = ({ dbs }) => {
    return Object.freeze({
      selectAllItems,
      selectOneItemByName,
      insertItem,
      selectAllUoms,
      insertUom,
      selectOneUomByName,
      selectOneUomByNameForUpdate,
      updateUom
    });


    async function selectAllItems({info}) {
      const db = await dbs();
      const sql = `SELECT * FROM items where is_active = true;`;
      return db.query(sql);
    }

    async function selectAllUoms({info}) {
      const db = await dbs();
      const sql = `
      select a.*, b.first_name, b.last_name from unit_of_measures a
      left join employees b on a.created_by = b.id   
      `;
      return db.query(sql);
    }

    async function selectOneItemByName({info}) {
      const db = await dbs();
      const sql = `SELECT * FROM items where LOWER(name) = LOWER($1)`;
      const params = [info.item]
      return db.query(sql, params);
    }

    async function insertItem({info}) {
      const db = await dbs();
      const sql = `
      INSERT INTO public.items(
         name, created_at, updated_at, created_by, updated_by)
        VALUES ( $1, $2, $3, $4, $5) returning id
      `;
      const params = [info.item, info.dateToday,  info.dateToday, info.createdby, info.createdby]
      return db.query(sql, params);
    }

    async function insertUom({info}) {
      const db = await dbs();
      const sql = `

      INSERT INTO public.unit_of_measures(
         name, created_by, created_at, updated_by, updated_at)
        VALUES ( $1, $2, $3, $4, $5);
    
      `;
      const params = [info.uomName, info.created_by, info.dateToday,info.created_by, info.dateToday ]
      return db.query(sql, params);
    }

    async function selectOneUomByName({info}) {
      const db = await dbs();
      const sql = `
      select * from unit_of_measures where lower(name) = lower($1)  
      `;
      const params = [info.uomName]
      return db.query(sql, params);
    }

    async function selectOneUomByNameForUpdate({info}) {
      const db = await dbs();
      const sql = `
      select * from unit_of_measures where lower(name) = lower($1) and id != $2
      `;
      const params = [info.name, info.id]
      return db.query(sql, params);
    }

    async function updateUom({info}) {
      const db = await dbs();
      const sql = `
      UPDATE public.unit_of_measures
	    SET  name=$1, updated_by=$2, updated_at=$3, is_active=$4
	    WHERE id = $5;
      `;
      const params = [info.name, info.updated_by, info.dateToday, info.status, info.id]
      return db.query(sql, params);
    }
  

  };
  
  module.exports = db;
  