const { makeDb } = require("../app");
const db = require("./query");

const itemsDb = makeDb({ db });

module.exports = itemsDb;
