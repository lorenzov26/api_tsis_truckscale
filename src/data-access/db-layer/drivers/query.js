const db = ({ dbs }) => {
  return Object.freeze({
    // insertNewDriver,
    // findByName,
    // findByNameUpdate,
    // updateDriver,
    // selectAllDrivers,
    // selectOneDriver,
    // insertDriverInTransaction,
    selectOneDriverByNameForAdd,
    selectOneDriverByNameForUpdate,
    insertDriver,
    selectAllDrivers,
    updateDriver,
    insertDriverForAutoAdd
  });

  async function updateDriver({info}) {
    const db = await dbs();
    const sql =
      `
      UPDATE public.drivers
	    SET first_name=$1, last_name=$2, middle_name=$3, updated_at=$4, updated_by=$5
    	WHERE id = $6;
      `;

    const params = [info.firstname, info.lastname, info.middlename, info.dateToday, info.updated_by, info.id];
 
    return db.query(sql, params);
  }


  async function selectOneDriverByNameForAdd({info}) {
    const db = await dbs();
    const sql =
      `
      SELECT * FROM drivers WHERE LOWER(first_name) = LOWER($1) 
      AND LOWER(last_name) = LOWER($2)
      
      `;

    const params = [info.firstname, info.lastname];
 
    return db.query(sql, params);
  }

  async function selectOneDriverByNameForUpdate({info}) {
    const db = await dbs();
    const sql =
      `
      SELECT * FROM drivers WHERE LOWER(first_name) = LOWER($1) 
      AND LOWER(last_name) = LOWER($2) and id != $3
      
      `;

    const params = [info.firstname, info.lastname, info.id];
 
    return db.query(sql, params);
  }

  async function selectAllDrivers({info}) {
    const db = await dbs();
    const sql =
      `
      SELECT * FROM drivers  
      `;

    const params = [];
 
    return db.query(sql);
  }
  
  async function insertDriver({info}) {
    const db = await dbs();
    const sql =`
    INSERT INTO public.drivers(
      first_name, last_name, middle_name, created_at, updated_at, created_by, updated_by)
      VALUES ($1, $2, $3, $4, $5, $6, $7);
    `
    const params = [info.firstname, info.lastname, info.middlename, info.dateToday, info.dateToday, info.created_by, info.created_by];
    return db.query(sql, params);
  }

  async function insertDriverForAutoAdd({info}) {
    const db = await dbs();
    const sql =`
    INSERT INTO public.drivers(
      first_name, last_name, created_at, updated_at, created_by, updated_by)
      VALUES ($1, $2, $3, $4, $5, $6) returning id;
    `
    const params = [info.firstname, info.lastname, info.dateToday, info.dateToday, info.createdby, info.createdby];
    return db.query(sql, params);
  }

  // add new driver in transaction add
  // async function insertDriverInTransaction({ ...info }) {
  //   const db = await dbs();
  //   const sql = `
  //       INSERT INTO ts_drivers (firstname,lastname,created_at)
  //       VALUES ($1,$2,$3);
  //   `;
  //   const params = [info.firstname, info.lastname, info.created_at];
  //   await db.query(sql, params);
  //   const sql1 = `SELECT id FROM ts_drivers ORDER BY id DESC LIMIT 1;`;
  //   return db.query(sql1);
  // }
  // // add new driver
  // async function insertNewDriver({ ...info }) {
  //   const db = await dbs();
  //   const sql =
  //     "INSERT INTO ts_drivers (firstname,middlename,lastname,name_extension,created_at) " +
  //     "VALUES($1,$2,$3,$4,$5);";
  //   const params = [
  //     info.firstname,
  //     info.middlename,
  //     info.lastname,
  //     info.name_extension,
  //     info.created_at
  //   ];
  //   return db.query(sql, params);
  // }
  // // find by full name during insert
  // async function findByName({ ...info }) {
  //   const db = await dbs();
  //   const sql =
  //     "SELECT * FROM ts_drivers WHERE LOWER(firstname) = LOWER($1) " +
  //     "AND LOWER(lastname) = LOWER($2);";
  //   const params = [info.firstname, info.lastname];
  //   return db.query(sql, params);
  // }
  // // find by full name during update
  // async function findByNameUpdate({ id, ...info }) {
  //   const db = await dbs();
  //   const sql =
  //     "SELECT * FROM ts_drivers WHERE LOWER(firstname) = LOWER($1) " +
  //     "AND LOWER(lastname) = LOWER($2) AND id <> $3;";
  //   const params = [info.firstname, info.lastname, id];
  //   return db.query(sql, params);
  // }
  // // update existing driver
  // async function updateDriver({ id, ...info }) {
  //   const db = await dbs();
  //   const sql =
  //     "UPDATE ts_drivers SET firstname = $1, middlename = $2, lastname = $3, name_extension = $4, " +
  //     "updated_at = $6 WHERE id = $5";
  //   const params = [
  //     info.firstname,
  //     info.middlename,
  //     info.lastname,
  //     info.name_extension,
  //     id,
  //     info.updated_at
  //   ];
  //   return db.query(sql, params);
  // }
  // // select all drivers
  // async function selectAllDrivers() {
  //   const db = await dbs();
  //   const sql =
  //     "SELECT id,firstname,middlename,lastname,name_extension FROM ts_drivers;";
  //   return db.query(sql);
  // }
  // // select single driver
  // async function selectOneDriver({ id }) {
  //   const db = await dbs();
  //   const sql =
  //     "SELECT id,firstname,middlename,lastname,name_extension FROM ts_drivers WHERE id = $1;";
  //   const params = [id];
  //   return db.query(sql, params);
  // }
};

module.exports = db;
