const db = ({ dbs }) => {
  return Object.freeze({
    insertNewWeight,
    selectByName,
    selectAllWeightType,
    selectOneWeightType,
    selectByNameUpdate,
    updateWeightType
  });
  // add
  async function insertNewWeight({ ...info }) {
    const db = await dbs();
    const sql = `INSERT INTO ts_weight_types (weight_name,created_at)
        VALUES ($1,$2);`;
    const params = [info.weight_name, info.created_at];
    return db.query(sql, params);
  }
  // check if name exist
  async function selectByName({ ...info }) {
    const db = await dbs();
    const sql = `SELECT * FROM ts_weight_types WHERE LOWER(weight_name) = LOWER($1);`;
    const params = [info.weight_name];
    return db.query(sql, params);
  }
  // select all
  async function selectAllWeightType() {
    const db = await dbs();
    const sql = `SELECT * FROM ts_weight_types;`;
    return db.query(sql);
  }
  // select one
  async function selectOneWeightType({ id }) {
    const db = await dbs();
    const sql = `SELECT * FROM ts_weight_types WHERE id = $1;`;
    const params = [id];
    return db.query(sql, params);
  }
  // select name during update
  async function selectByNameUpdate({ id, ...info }) {
    const db = await dbs();
    const sql = `SELECT * FROM ts_weight_types WHERE weight_name = $1 AND id <> $2;`;
    const params = [info.weight_name, id];
    return db.query(sql, params);
  }
  // update
  async function updateWeightType({ id, ...info }) {
    const db = await dbs();
    const sql = `UPDATE ts_weight_types SET weight_name=$1, updated_at=$2 WHERE id = $3;`;
    const params = [info.weight_name, info.updated_at, id];
    return db.query(sql, params);
  }
};

module.exports = db;
