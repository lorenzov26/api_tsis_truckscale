const { makeDb } = require("../app");
const db = require("./query");

const weightDb = makeDb({ db });

module.exports = weightDb;
