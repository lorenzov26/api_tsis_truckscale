const db = ({ dbs }) => {
  return Object.freeze({
    insertUser,
    insertRole,
    insertModule,
    insertAction,
    insertAccessRight,
    resetSequence,
    cascadeTables,
    updateRole,
    insertWeightType,
    insertTransactionType
  });
  async function insertTransactionType({ ...info }) {
    const db = await dbs();
    const sql = `INSERT INTO ts_transaction_types (transaction_type_name,created_at) VALUES ($1,$2);`;
    const params = [info.name, info.created_at];
    return db.query(sql, params);
  }
  async function insertWeightType({ ...info }) {
    const db = await dbs();
    const sql = `INSERT INTO ts_weight_types (weight_name,created_at) VALUES ($1,$2);`;
    const params = [info.name, info.created_at];
    return db.query(sql, params);
  }
  async function insertUser({ ...info }) {
    const db = await dbs();
    const sql = `INSERT INTO ts_users (id,employee_id,email,firstname,lastname,password, role_id, created_at,login_count) 
      VALUES ($1,$2,$3,$4,$5,$6,$7,$8,0);`;
    const params = [
      info.id,
      info.employee_id,
      info.email,
      info.firstname,
      info.lastname,
      info.password,
      info.role_id,
      info.created_at
    ];
    return db.query(sql, params);
  }

  async function insertRole({ ...info }) {
    const db = await dbs();
    const sql = `INSERT INTO ts_roles (id, name, status, created_at)
      VALUES ($1,$2,$3,$4);`;
    const params = [info.id, info.name, info.status, info.created_at];
    return db.query(sql, params);
  }

  async function insertModule({ ...info }) {
    const db = await dbs();
    const sql = `INSERT INTO ts_modules (id,descriptions,status,created_at,created_by) 
         VALUES ($1,$2,$3,$4,$5)`;
    const params = [
      info.id,
      info.description,
      info.status,
      info.created_at,
      info.created_by
    ];
    return db.query(sql, params);
  }

  async function insertAction({ ...info }) {
    const db = await dbs();
    const sql = `INSERT INTO ts_actions (id,modules_id,description,status,created_at,created_by)
      VALUES ($1,$2,$3,$4,$5,$6)`;
    const params = [
      info.id,
      info.module_id,
      info.description,
      info.status,
      info.created_at,
      info.created_by
    ];
    return db.query(sql, params);
  }

  async function insertAccessRight({ ...info }) {
    const db = await dbs();
    const sql = `INSERT INTO ts_access_rights (id,actions_id,roles_id,created_at)
      VALUES ($1,$2,$3,$4);`;
    const params = [info.id, info.actions_id, info.roles_id, info.created_at];
    return db.query(sql, params);
  }

  async function resetSequence() {
    const db = await dbs();
    const sql = [
      `ALTER SEQUENCE ts_users_id_seq RESTART WITH 1000;`,
      `ALTER SEQUENCE ts_roles_id_seq RESTART WITH 1000;`,
      `ALTER SEQUENCE ts_modules_id_seq RESTART WITH 1000;`,
      `ALTER SEQUENCE ts_actions_id_seq RESTART WITH 1000;`,
      `ALTER SEQUENCE ts_access_rights_id_seq RESTART WITH 1000;`,
      `ALTER SEQUENCE ts_weight_types_id_seq RESTART WITH 1000;`,
      `ALTER SEQUENCE ts_transaction_types_id_seq RESTART WITH 1000;`
    ];
    for await (let d of sql) {
      await db.query(d);
    }
    const data = { msg: "reset" };
    return data;
  }

  async function cascadeTables() {
    const db = await dbs();
    const sql = `
      TRUNCATE TABLE ts_users RESTART IDENTITY CASCADE;
      TRUNCATE TABLE ts_roles RESTART IDENTITY CASCADE;
      TRUNCATE TABLE ts_modules RESTART IDENTITY CASCADE;
      TRUNCATE TABLE ts_actions RESTART IDENTITY CASCADE;
      TRUNCATE TABLE ts_access_rights RESTART IDENTITY CASCADE;
      TRUNCATE TABLE ts_activity_logs RESTART IDENTITY CASCADE;
      TRUNCATE TABLE ts_transactions RESTART IDENTITY CASCADE;
      TRUNCATE TABLE ts_transaction_types RESTART IDENTITY CASCADE;
      TRUNCATE TABLE ts_inbound_transactions RESTART IDENTITY CASCADE;
      TRUNCATE TABLE ts_outbound_transactions RESTART IDENTITY CASCADE;
      TRUNCATE TABLE ts_weight_types RESTART IDENTITY CASCADE;

      TRUNCATE TABLE ts_drivers RESTART IDENTITY CASCADE;
      TRUNCATE TABLE ts_truck_infos RESTART IDENTITY CASCADE;
      TRUNCATE TABLE ts_truck_types RESTART IDENTITY CASCADE;
      TRUNCATE TABLE ts_truckscales RESTART IDENTITY CASCADE;
      TRUNCATE TABLE ts_raw_materials RESTART IDENTITY CASCADE;
      `;
    return db.query(sql);
  }

  async function updateRole(created_by, id) {
    const db = await dbs();
    const sql = `
      UPDATE ts_roles SET created_by = $1 WHERE id = $2;
      `;
    const params = [created_by, id];
    return db.query(sql, params);
  }
};

module.exports = db;
