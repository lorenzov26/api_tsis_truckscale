const { makeDb } = require("../app");
const db = require("./query");

const seedersDb = makeDb({ db });

module.exports = seedersDb;
