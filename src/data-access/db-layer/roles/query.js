const db = ({ dbs }) => {
  return Object.freeze({
    insertRole,
    selectAllRoles,
    selectByRoleNameUpdate,
    updateRole,
    deleteAccessRightsByRoleId,
    insertAccessRight,
    selectRoleByName
  });


  // select all roles
  async function selectAllRoles({ }) {
    const db = await dbs();
    const sql = `
    select a.*, b.first_name, b.last_name from roles a
    left join employees b on a.created_by = b.id
    `;
    return db.query(sql);
  }

  async function deleteAccessRightsByRoleId({id}) {
    const db = await dbs();
    const sql = `
    delete from access_rights where role_id = $1
    `;
    const params = [id];
    return db.query(sql, params);
  }

  async function insertAccessRight({info}) {
    const db = await dbs();
    const sql = `
    INSERT INTO public.access_rights(
       action_id, role_id)
      VALUES ($1, $2);
    `;
    const params = [info.action_id, info.role_id];
    return db.query(sql, params);
  }

  async function insertRole({info}) {
    const db = await dbs();
    const sql = `
    INSERT INTO public.roles(
     name, created_at, updated_at, created_by, updated_by)
      VALUES ($1, $2, $3, $4, $5);
    `;
    const params = [info.name, info.date_today, info.date_today, info.created_by, info.created_by];
    return db.query(sql, params);
  }

  async function selectRoleByName({info}) {
    const db = await dbs();
    const sql = `select * from roles where name = $1`;
    const params = [info.name];
    return db.query(sql, params);
  }


  async function selectByRoleNameUpdate({info}) {
    const db = await dbs();
    const sql = `select * from roles where name = $1 and id != $2`;
    const params = [info.name, info.id];
    return db.query(sql, params);
  }

  async function updateRole({info}) {
    const db = await dbs();
    const sql = `
    UPDATE public.roles
    SET name=$1, updated_at=$2, is_active=$3, updated_by=$4
    WHERE id = $5;
    `;
    const params = [info.name, info.date_today, info.status, info.updated_by, info.id];
    return db.query(sql, params);
  }

  
};

module.exports = db;
