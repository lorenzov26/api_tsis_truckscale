const { makeDb } = require("../app");
const db = require("./query");

const rolesDb = makeDb({ db });

module.exports = rolesDb;
