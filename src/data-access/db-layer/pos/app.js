const { makeDb } = require("../app");
const db = require("./query");

const posDb = makeDb({ db });

module.exports = posDb;
