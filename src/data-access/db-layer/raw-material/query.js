const db = ({ dbs }) => {
  return Object.freeze({
    insertNewRawMaterial,
    selectByName,
    selectAllRawMaterials,
    selectOneRawMaterials,
    selectByNameUpdate,
    updateRawMaterial,
    returnCreatedBy
  });
  // get the name of the user who created the role
  async function returnCreatedBy({ id }) {
    const db = await dbs();
    const sql = `SELECT id,employee_id,firstname,lastname
    FROM ts_users WHERE id = $1;`;
    const params = [id];
    return db.query(sql, params);
  }
  // add new supplier
  async function insertNewRawMaterial({ ...info }) {
    const db = await dbs();
    const sql = `INSERT INTO ts_raw_materials (description,created_by,created_at)
      VALUES ($1,$2,$3);`;
    const params = [info.description, info.created_by, info.created_at];
    return db.query(sql, params);
  }
  // check if supplier name exist
  async function selectByName({ ...info }) {
    const db = await dbs();
    const sql = `SELECT * FROM ts_raw_materials WHERE description = $1;`;
    const params = [info.description];
    return db.query(sql, params);
  }
  // select all suppliers
  async function selectAllRawMaterials() {
    const db = await dbs();
    const sql = `SELECT a.id,a.description,a.firstname createdfn,a.lastname createdln,a.created_at,
    b.firstname updatedfn,b.lastname updatedln,b.updated_at
    FROM
    (
    SELECT trm.id,description,tsu.firstname,tsu.lastname,trm.created_at
    FROM ts_raw_materials trm LEFT JOIN ts_users tsu ON tsu.id = trm.created_by
    ) a LEFT JOIN
    (
    SELECT trm.id,description,tsu.firstname,tsu.lastname,trm.updated_at
    FROM ts_raw_materials trm LEFT JOIN ts_users tsu ON tsu.id = trm.updated_by
    ) b ON b.id = a.id;`;
    return db.query(sql);
  }
  // select one supplier
  async function selectOneRawMaterials({ id }) {
    const db = await dbs();
    const sql = `SELECT a.id,a.description,a.firstname createdfn,a.lastname createdln,a.created_at,
    b.firstname updatedfn,b.lastname updatedln,b.updated_at
    FROM
    (
    SELECT trm.id,description,tsu.firstname,tsu.lastname,trm.created_at
    FROM ts_raw_materials trm LEFT JOIN ts_users tsu ON tsu.id = trm.created_by
    ) a LEFT JOIN
    (
    SELECT trm.id,description,tsu.firstname,tsu.lastname,trm.updated_at
    FROM ts_raw_materials trm LEFT JOIN ts_users tsu ON tsu.id = trm.updated_by
    ) b ON b.id = a.id WHERE a.id = $1;`;
    const params = [id];
    return db.query(sql, params);
  }
  // select name during update
  async function selectByNameUpdate({ id, ...info }) {
    const db = await dbs();
    const sql = `SELECT * FROM ts_raw_materials WHERE description = $1 AND id <> $2;`;
    const params = [info.description, id];
    return db.query(sql, params);
  }
  // update supplier
  async function updateRawMaterial({ id, ...info }) {
    const db = await dbs();
    const sql = `UPDATE ts_raw_materials SET description=$1, updated_by=$2, updated_at=$3 WHERE id = $4;`;
    const params = [info.description, info.updated_by, info.updated_at, id];
    return db.query(sql, params);
  }
};

module.exports = db;
