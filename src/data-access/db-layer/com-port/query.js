const db = ({ dbs }) => {
  return Object.freeze({
    selectConfigOfDevice
  });
  // select the config of the device; registered by the admin
  async function selectConfigOfDevice({info}) {

    const db = await dbs();
    const sql = `
    select * from truckscales where truckscale_ip = $1 and truckscale_device = $2
    `;
    const params = [info.ip, info.device];
    return db.query(sql, params);
  }
};

module.exports = db;
