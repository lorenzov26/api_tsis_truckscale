const { makeDb } = require("../app");
const db = require("./query");

const comPortDb = makeDb({ db });

module.exports = comPortDb;
