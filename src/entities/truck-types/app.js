const addTruckType = require("./truck-types-entity")
const updateTruckType = require("./truck-types-entity-update")


const e_addTruckType = addTruckType({})
const e_updateTruckType = updateTruckType({})



module.exports = { e_addTruckType, e_updateTruckType }
