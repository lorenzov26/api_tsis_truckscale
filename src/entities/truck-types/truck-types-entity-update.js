const updateTruckType = ({}) => {
    return function make({ 
      truck_type, 
      truck_model, 
      truck_size, 
      updated_at = new Date().toISOString() } = {}) {
      
      if(!truck_type){
        throw new Error(`Please enter truck type.`)
      }

      if(!truck_model){
        throw new Error(`Please enter truck model.`)
      }

      if(!truck_size){
        throw new Error(`Please enter truck size.`)
      }

      return Object.freeze({
        getTruckType: () => truck_type,
        getTruckModel: () => truck_model,
        getTruckSize: () => truck_size,
        getUpdatedAt: () => updated_at
      });
    };
  };
  
  module.exports = updateTruckType;
  