const addTruckType = ({}) => {
    return function make({ 
      truck_type, 
      truck_model, 
      truck_size, 
      created_at = new Date().toISOString() } = {}) {
      
      if(!truck_type){
        throw new Error(`Please enter truck type.`)
      }

      if(!truck_model){
        throw new Error(`Please enter truck model.`)
      }

      if(!truck_size){
        throw new Error(`Please enter truck size.`)
      }

      return Object.freeze({
        getTruckType: () => truck_type,
        getTruckModel: () => truck_model,
        getTruckSize: () => truck_size,
        getCreatedAt: () => created_at
      });
    };
  };
  
  module.exports = addTruckType;
  