const userLoginEntity = ({ encrypt }) => {
  return function make({ employee_id, password } = {}) {
    if (!employee_id) {
      throw new Error("Please enter employee ID.");
    }
    if (!password) {
      throw new Error("Please enter password.");
    }
    return Object.freeze({
      getId: () => encrypt(employee_id),
      getPassword: () => encrypt(password)
    });
  };
};

module.exports = userLoginEntity;
