const makeUserEntity = ({ encrypt }) => {
  return function make({
    employee_id,
    email,
    firstname,
    middlename,
    lastname,
    name_extension,
    password,
    role_id,
    employee_image,
    employee_signature,
    contact_number,
    created_at = new Date().toISOString(),
    updated_at = new Date().toISOString()
  } = {}) {
    if (!employee_id) {
      throw new Error("Please enter employee ID.");
    }
    if (!firstname) {
      throw new Error("Please enter first name.");
    }
    if (!lastname) {
      throw new Error("Please enter last name.");
    }
    if (!password) {
      throw new Error("Please enter password.");
    }
    if (!role_id) {
      throw new Error("Please enter role of the user.");
    }
    let nameExtension = "";
    let midName = "";
    let emails = "";
    let contacts = "";

    // if there is name extension
    if (name_extension) {
      nameExtension = encrypt(name_extension);
    }
    // if there is middlename
    if (middlename) {
      midName = encrypt(middlename);
    }
    if (email) {
      emails = encrypt(email);
    }
    if (contact_number) {
      contacts = encrypt(contact_number);
    }
    return Object.freeze({
      getId: () => encrypt(employee_id),
      getEmail: () => emails,
      getFirstName: () => encrypt(firstname),
      getMiddleName: () => midName,
      getLastName: () => encrypt(lastname),
      getNameExtension: () => nameExtension,
      getPassword: () => encrypt(password),
      getRole: () => role_id,
      getImage: () => employee_image,
      getSignature: () => employee_signature,
      getContactNumber: () => contacts,
      getCreatedAt: () => created_at,
      getUpdatedAt: () => updated_at
    });
  };
};

module.exports = makeUserEntity;
