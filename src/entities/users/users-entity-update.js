const updateUser = ({ encrypt }) => {
    return function make({
      employee_id,
      email,
      firstname,
      middlename,
      lastname,
      name_extension,
      role_id,
      employee_image,
      employee_signature,
      contact_number,
      updated_at = new Date().toISOString()
    } = {}) {
     
      if(!employee_id){
        throw new Error("Please enter employee ID.");
      }

      if(!firstname){
        throw new Error("Please enter first name.");
      }

      if(!lastname){
        throw new Error("Please enter last name.");
      }

      if(!role_id){
        throw new Error("Please enter role ID.");      
      }

      if(isNaN(role_id)){
        throw new Error("ID must be a number.");     
      }

      if (name_extension) {
        name_extension = encrypt(name_extension);
      }

      if (middlename) {
        middlename = encrypt(middlename);
      } 

      if (email) {
        email = encrypt(email);
      }

      if (contact_number) {
        contact_number = encrypt(contact_number);
      }
    
      return Object.freeze({
        getEmployeeId: () => encrypt(employee_id),
        getEmail: () => email,
        getFirstName: () => encrypt(firstname),
        getMiddleName: () => middlename,
        getLastName: () => encrypt(lastname),
        getNameExtension: () => name_extension,
        getRole: () => role_id,
        getImage: () => employee_image,
        getSignature: () => employee_signature,
        getContactNumber: () => contact_number,
        getUpdatedAt: () => updated_at
      });
    };
  };
  
  module.exports = updateUser;
  