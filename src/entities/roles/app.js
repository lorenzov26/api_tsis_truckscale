const makeRolesEntity = require("./roles-entity");
const makeRolesEntityUpdate = require("./roles-entity-update");

const makeRoles = makeRolesEntity({});
const makeRolesUpdate = makeRolesEntityUpdate({});

module.exports = { makeRoles, makeRolesUpdate };
