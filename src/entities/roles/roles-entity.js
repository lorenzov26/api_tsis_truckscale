const makeRolesEntity = ({}) => {
  return function make({
    name,
    status = "active",
    created_by,
    created_at = new Date().toISOString()
  } = {}) {
    if (!name) {
      throw new Error("Please enter role name.");
    }
    // if (!status) {
    //   throw new Error("Please enter role status.");
    // }
    if (!created_by) {
      throw new Error("Please enter who created the role.");
    }
    return Object.freeze({
      getRoleName: () => name,
      getCreatedAt: () => created_at,
      getRoleStatus: () => status,
      getCreatedBy: () => created_by
    });
  };
};

module.exports = makeRolesEntity;
