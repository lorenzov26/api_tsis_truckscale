const makeModulesEntity = require("./modules-entity");
const makeModulesEntityUpdate = require("./modules-entity-update");

const makeModule = makeModulesEntity({});
const makeModuleUpdate = makeModulesEntityUpdate({});

module.exports = { makeModule, makeModuleUpdate };
