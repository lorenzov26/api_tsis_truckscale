const addInbound = ({}) => {
  return function make({
    transaction_id,
    weight_type_id,
    ib_weight,
    ib_timestamp = new Date().toISOString(),
    users_id,
    truckscale_id,
    drivers_id,
    truck_info_id
  } = {}) {
    if (!transaction_id) {
      throw new Error("Please enter transaction.");
    }
    if (!ib_weight) {
      throw new Error("Please enter inbound weight.");
    }
    if (!users_id) {
      throw new Error("Please enter user.");
    }
    if (!truckscale_id) {
      throw new Error("Please enter truckscale.");
    }
    if (!drivers_id) {
      throw new Error("Please enter driver.");
    }
    if (!truck_info_id) {
      throw new Error("Please enter truck info.");
    }
    return Object.freeze({
      getTransactionId: () => transaction_id,
      getWeightTypeId: () => weight_type_id,
      getIbWeight: () => ib_weight,
      getIbTimestamp: () => ib_timestamp,
      getUsersId: () => users_id,
      getTruckscaleId: () => truckscale_id,
      getDriversId: () => drivers_id,
      getTruckInfoId: () => truck_info_id
    });
  };
};

module.exports = addInbound;
