const makeTransactionEntity = require("./transaction-entity");
const addInbound = require("./transaction-inbound-add");
const addOutbound = require("./transaction-outbound-add");
// ########################
// ########################
const makeTransaction = makeTransactionEntity({});
const e_addInbound = addInbound({});
const e_addOutbound = addOutbound({});

// ########################

module.exports = {
  makeTransaction,
  e_addInbound,
  e_addOutbound
};
