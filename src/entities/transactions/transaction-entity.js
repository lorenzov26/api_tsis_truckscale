const makeTransactionEntity = ({}) => {
  return function make({
    // supplier_name,
    // raw_material_name,
    transaction_type_name,
    created_at = new Date().toISOString(),
    updated_at = new Date().toISOString(),
    tracking_code,
    location_delivery,
    warehouse_location,
    no_of_bags,
    purchase_order_id,
    transmittal_number,
    created_by
  } = {}) {
    // if (!supplier_name) {
    //   throw new Error("Please enter supplier information.");
    // }
    // if (!raw_material_name) {
    //   throw new Error("Please enter raw material information.");
    // }

   

    if (!transaction_type_name) {
      throw new Error("Please enter transaction type.");
    }

    if(transaction_type_name !== 'Others'){
      if(!purchase_order_id){
        throw new Error("Please enter purchase order number");
      }
    }
   
    if (!created_by) {
      throw new Error(`Please enter who created this transaction.`);
    }

    return Object.freeze({
      getSupplier: () => supplier_name,
      getRawMaterial: () => raw_material_name,
      getTransactionType: () => transaction_type_name,
      getCreatedAt: () => created_at,
      getUpdatedAt: () => updated_at,
      getTrackingCode: () => tracking_code,
      getLocationDelivery: () => location_delivery,
      getWarehouseLocations: () => warehouse_location,
      getNumberOfBags: () => no_of_bags,
      getPurchaseOrderId: () => purchase_order_id,
      getTransmittalNumber: () => transmittal_number,
      getCreatedBy: () => created_by
    });
  };
};

module.exports = makeTransactionEntity;
