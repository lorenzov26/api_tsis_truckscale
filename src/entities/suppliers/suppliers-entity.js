const makeSuppliersEntity = ({}) => {
  return function make({ name, address, contact_number } = {}) {
    if (!name) {
      throw new Error("Please enter supplier name.");
    }
    if (!address) {
      throw new Error("Please enter supplier address.");
    }
    if (!contact_number) {
      throw new Error("Please enter supplier contact number.");
    }

    return Object.freeze({
      getSupplierName: () => name,
      getAddress: () => address,
      getContact: () => contact_number
    });
  };
};

module.exports = makeSuppliersEntity;
