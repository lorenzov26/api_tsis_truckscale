const makeDriverEntity = ({ encrypt }) => {
  return function make({
    firstname,
    middlename,
    lastname,
    name_extension,
    created_at = new Date().toISOString(),
    updated_at = new Date().toISOString()
  } = {}) {
    if (!firstname) {
      throw new Error("Please enter first name.");
    }
    // if (!middlename) {
    //   throw new Error("Please enter middle name.");
    // }
    if (!lastname) {
      throw new Error("Please enter last name.");
    }

    let nameExtension = "";
    let midName = "";

    // if there is name extension
    if (name_extension) {
      nameExtension = encrypt(name_extension);
    }

    // if there is middlename
    if (middlename) {
      midName = encrypt(middlename);
    }

    return Object.freeze({
      getFirstName: () => encrypt(firstname),
      getMiddleName: () => midName,
      getLastName: () => encrypt(lastname),
      getNameExtension: () => nameExtension,
      getCreatedAt: () => created_at,
      getUpdatedAt: () => updated_at
    });
  };
};

module.exports = makeDriverEntity;
