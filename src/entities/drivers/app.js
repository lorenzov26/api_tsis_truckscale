const makeDriverEntity = require("./drivers-entity");
const { encrypt } = require("../../../crypting/app");

const makeDriver = makeDriverEntity({ encrypt });

module.exports = makeDriver;
