const readingDataFromSerial = ({ returnIpAddress, hostName }) => {
  return function make({
    comPort,
    ip_address = returnIpAddress(),
    device_name = hostName()
  } = {}) {
    if (!comPort) {
      throw new Error("Please select COM port to connect.");
    }
    return Object.freeze({
      getCOMPort: () => comPort,
      getHostName: () => device_name,
      getIp: () => ip_address
    });
  };
};

module.exports = readingDataFromSerial;
