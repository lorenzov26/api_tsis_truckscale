const makeTransactionTypeEntity = ({}) => {
  return function make({
    name,
    created_at = new Date().toISOString(),
    updated_at = new Date().toISOString()
  } = {}) {
    if (!name) {
      throw new Error("Please enter transaction type.");
    }
    return Object.freeze({
      getTransactionType: () => name,
      getCreatedAt: () => created_at,
      getUpdatedAt: () => updated_at
    });
  };
};

module.exports = makeTransactionTypeEntity;
