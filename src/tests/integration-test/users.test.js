const { users, randomString, getRandomInt } = require("./users");
const { seeds } = require("./seeders");

let token = "";

const prereq = async () => {
  try {
    await seeds.seeders({});

    // seeded data
    const info = {
      employee_id: "154151000",
      password: "password"
    };

    const res = await users.loginUser({ info });
    token = res.data.patched.token;
  } catch (e) {
    console.log("Error: ", e);
  }
};

beforeAll(async () => {
  try {
    await prereq();
  } catch (e) {
    console.log(e);
  }
});

// functions
describe("User routes.", () => {
  it("POST - Insert new user.", async () => {
    const i = getRandomInt(999);
    const info = {
      employee_id: `154151${i}`,
      email: `${randomString(5)}.${randomString(6)}@biotechfarms.net`,
      firstname: randomString(5),
      middlename: "",
      lastname: randomString(5),
      name_extension: "",
      password: `154151${i}`,
      role_id: "1",
      employee_image: "",
      employee_signature: "",
      contact_number: `0${getRandomInt(10000)}`,
      users_id: "1"
    };

    const res = await users.insertUser({ token, info });
    expect(res.status).toBe(201);
  });

  it("GET - Select all users.", async () => {
    const res = await users.selectAllUsers({ token });
    expect(res.status).toBe(200);
  });

  it("GET - Select one users.", async () => {
    const res = await users.selectAllUsers({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random users id
    const id = rand.id;

    const ress = await users.selectOneUser({ id, token });
    expect(ress.status).toBe(200);
  });

  it("PUT - Update users.", async () => {
    const i = getRandomInt(999);
    const info = {
      employee_id: `154151${i}`,
      email: `${randomString(5)}.${randomString(6)}@biotechfarms.net`,
      firstname: randomString(5),
      middlename: "",
      lastname: randomString(5),
      name_extension: "",
      password: `154151${i}`,
      role_id: "1",
      employee_image: "",
      employee_signature: "",
      contact_number: `0${getRandomInt(10000)}`,
      users_id: "1"
    };
    // account id is not the admin; used for token
    const ress = await users.updateUser({ id: 2, token, info });
    expect(ress.status).toBe(200);
  });

  it("PUT - Login users.", async () => {
    const i = getRandomInt(999);
    const info = {
      employee_id: `154151${i}`,
      email: `${randomString(5)}.${randomString(6)}@biotechfarms.net`,
      firstname: randomString(5),
      middlename: "",
      lastname: randomString(5),
      name_extension: "",
      password: `154151${i}`,
      role_id: "1",
      employee_image: "",
      employee_signature: "",
      contact_number: `0${getRandomInt(10000)}`,
      users_id: "1"
    };

    // insert user
    await users.insertUser({ token, info });

    const data = {
      employee_id: `154151${i}`,
      password: `154151${i}`
    };

    const ress = await users.loginUser({ info: data });
    expect(ress.status).toBe(200);
  });

  it("POST - Insert new user: missing required fields.", async () => {
    const i = getRandomInt(999);
    const info = {
      employee_id: ``,
      email: `${randomString(5)}.${randomString(6)}@biotechfarms.net`,
      firstname: randomString(5),
      middlename: "",
      lastname: randomString(5),
      name_extension: "",
      password: `154151${i}`,
      role_id: "1",
      employee_image: "",
      employee_signature: "",
      contact_number: `0${getRandomInt(10000)}`,
      users_id: "1"
    };

    const res = await users.insertUser({ token, info });
    expect(res.status).toBe(400);
  });

  it("POST - Insert new user: no token.", async () => {
    const i = getRandomInt(999);
    const info = {
      employee_id: `154151${i}`,
      email: `${randomString(5)}.${randomString(6)}@biotechfarms.net`,
      firstname: randomString(5),
      middlename: "",
      lastname: randomString(5),
      name_extension: "",
      password: `154151${i}`,
      role_id: "1",
      employee_image: "",
      employee_signature: "",
      contact_number: `0${getRandomInt(10000)}`,
      users_id: "1"
    };

    const res = await users.insertUser({ info });
    expect(res.status).toBe(403);
  });

  it("GET - Select all users: no token.", async () => {
    const res = await users.selectAllUsers({});
    expect(res.status).toBe(403);
  });

  it("GET - Select one users: no token.", async () => {
    const res = await users.selectAllUsers({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random users id
    const id = rand.id;

    const ress = await users.selectOneUser({ id });
    expect(ress.status).toBe(403);
  });

  it("PUT - Update users: missing required fields.", async () => {
    const res = await users.selectAllUsers({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random users id
    const id = rand.id;

    const i = getRandomInt(999);
    const info = {
      employee_id: ``,
      email: `${randomString(5)}.${randomString(6)}@biotechfarms.net`,
      firstname: randomString(5),
      middlename: "",
      lastname: randomString(5),
      name_extension: "",
      password: `154151${i}`,
      role_id: "1",
      employee_image: "",
      employee_signature: "",
      contact_number: `0${getRandomInt(10000)}`,
      users_id: "1"
    };

    const ress = await users.updateUser({ id, token, info });
    expect(ress.status).toBe(400);
  });

  it("PUT - Update users: no token.", async () => {
    const res = await users.selectAllUsers({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random users id
    const id = rand.id;

    const i = getRandomInt(999);
    const info = {
      employee_id: `154151${i}`,
      email: `${randomString(5)}.${randomString(6)}@biotechfarms.net`,
      firstname: randomString(5),
      middlename: "",
      lastname: randomString(5),
      name_extension: "",
      password: `154151${i}`,
      role_id: "1",
      employee_image: "",
      employee_signature: "",
      contact_number: `0${getRandomInt(10000)}`,
      users_id: "1"
    };

    const ress = await users.updateUser({ id, info });
    expect(ress.status).toBe(403);
  });

  it("PUT - Login users: missing credentials.", async () => {
    const i = getRandomInt(999);
    const info = {
      employee_id: `154151${i}`,
      email: `${randomString(5)}.${randomString(6)}@biotechfarms.net`,
      firstname: randomString(5),
      middlename: "",
      lastname: randomString(5),
      name_extension: "",
      password: `154151${i}`,
      role_id: "1",
      employee_image: "",
      employee_signature: "",
      contact_number: `0${getRandomInt(10000)}`,
      users_id: "1"
    };

    // insert user
    await users.insertUser({ token, info });

    const data = {
      employee_id: `154151${i}`,
      password: ``
    };

    const ress = await users.loginUser({ info: data });
    expect(ress.status).toBe(400);
  });

  it("PUT - Update users' password.", async () => {
    const pw = randomString(10);

    const info = {
      password: pw,
      repassword: pw
    };
    // account id is not the admin; used for token
    const ress = await users.resetPassword({ id: 2, token, info });
    expect(ress.status).toBe(200);
  });

  it("PUT - Update users' password: missing required fields.", async () => {
    const pw = randomString(10);

    const info = {
      password: "",
      repassword: pw
    };
    // account id is not the admin; used for token
    const ress = await users.resetPassword({ id: 2, token, info });
    expect(ress.status).toBe(400);
  });

  it("PUT - Update users' password: password doesn't match.", async () => {
    const pw = randomString(10);

    const info = {
      password: pw,
      repassword: `${pw}s`
    };
    // account id is not the admin; used for token
    const ress = await users.resetPassword({ id: 2, token, info });
    expect(ress.status).toBe(400);
  });

  it("PUT - Update users' password: no token.", async () => {
    const pw = randomString(10);

    const info = {
      password: pw,
      repassword: pw
    };
    // account id is not the admin; used for token
    const ress = await users.resetPassword({ id: 2, info });
    expect(ress.status).toBe(403);
  });

  it("GET - Select all activity logs.", async () => {
    const res = await users.selectAllActivityLogs({ token });
    expect(res.status).toBe(200);
  });

  it("GET - Select all activity logs: no token.", async () => {
    const res = await users.selectAllActivityLogs({});
    expect(res.status).toBe(403);
  });
});
