const axios = require("axios");
const rs = require("randomstring"); // generate random string
const dotenv = require("dotenv");
dotenv.config();

// function to generate random string
const randomString = i => {
  const str = rs.generate({
    length: i,
    charset: "alphabetic"
  });
  return str;
};

// function generate random int
const getRandomInt = max => {
  return Math.floor(Math.random() * Math.floor(max));
};

const truckTypes = {
  selectAllTruckTypes: async ({ token }) => {
    try {
      const res = await axios({
        method: "GET",
        url: `${process.env.BASE_URL}/truck-types/select`,
        headers: {
          Authorization: `Bearer ${token}`
        }
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  selectOneTruckType: async ({ id, token }) => {
    try {
      const res = await axios({
        method: "GET",
        url: `${process.env.BASE_URL}/truck-types/select/${id}`,
        headers: {
          Authorization: `Bearer ${token}`
        }
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  insertTruckType: async ({ token, info }) => {
    try {
      const res = await axios({
        method: "POST",
        url: `${process.env.BASE_URL}/truck-types/add`,
        headers: {
          Authorization: `Bearer ${token}`
        },
        data: {
          ...info
        }
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  updateTruckType: async ({ id, token, info }) => {
    try {
      const res = await axios({
        method: "PUT",
        url: `${process.env.BASE_URL}/truck-types/update/${id}`,
        headers: {
          Authorization: `Bearer ${token}`
        },
        data: {
          ...info
        }
      });
      return res;
    } catch (e) {
      return e.response;
    }
  }
};

module.exports = { truckTypes, randomString, getRandomInt };
