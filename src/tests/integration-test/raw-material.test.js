const { rawMaterial, randomString, getRandomInt } = require("./raw-material");
const { users } = require("./users");
const { seeds } = require("./seeders");

let token = "";

const prereq = async () => {
  try {
    await seeds.seeders({});

    // seeded data
    const info = {
      employee_id: "154151000",
      password: "password"
    };

    const res = await users.loginUser({ info });
    token = res.data.patched.token;
  } catch (e) {
    console.log("Error: ", e);
  }
};

beforeAll(async () => {
  try {
    await prereq();
  } catch (e) {
    console.log(e);
  }
});

// functions
describe("Raw materials routes.", () => {
  it("POST - Insert new raw material.", async () => {
    const info = {
      description: randomString(5),
      created_by: "1",
      users_id: "1"
    };

    const res = await rawMaterial.insertRawMaterial({ token, info });
    expect(res.status).toBe(201);
  });

  it("GET - Select all raw materials.", async () => {
    const res = await rawMaterial.selectAllRawMaterial({ token });
    expect(res.status).toBe(200);
  });

  it("GET - Select one raw material.", async () => {
    const res = await rawMaterial.selectAllRawMaterial({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random raw material id
    const id = rand.id;

    const ress = await rawMaterial.selectOneRawMaterial({ id, token });
    expect(ress.status).toBe(200);
  });

  it("PUT - Update raw material.", async () => {
    const res = await rawMaterial.selectAllRawMaterial({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random raw material id
    const id = rand.id;

    const info = {
      description: randomString(5),
      updated_by: "1",
      users_id: "1"
    };

    const ress = await rawMaterial.updateRawMaterial({ id, token, info });
    expect(ress.status).toBe(200);
  });

  it("POST - Insert new raw material: missing required fields.", async () => {
    const info = {
      description: "",
      created_by: "1",
      users_id: "1"
    };

    const res = await rawMaterial.insertRawMaterial({ token, info });
    expect(res.status).toBe(400);
  });

  it("POST - Insert new raw material: no token.", async () => {
    const info = {
      description: randomString(5),
      created_by: "1",
      users_id: "1"
    };

    const res = await rawMaterial.insertRawMaterial({ info });
    expect(res.status).toBe(403);
  });

  it("GET - Select all raw materials: no token.", async () => {
    const res = await rawMaterial.selectAllRawMaterial({});
    expect(res.status).toBe(403);
  });

  it("GET - Select one raw material: no token.", async () => {
    const res = await rawMaterial.selectAllRawMaterial({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random raw material id
    const id = rand.id;

    const ress = await rawMaterial.selectOneRawMaterial({ id });
    expect(ress.status).toBe(403);
  });

  it("PUT - Update raw material: missing required fields.", async () => {
    const res = await rawMaterial.selectAllRawMaterial({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random raw material id
    const id = rand.id;

    const info = {
      description: "",
      updated_by: "1",
      users_id: "1"
    };

    const ress = await rawMaterial.updateRawMaterial({ id, token, info });
    expect(ress.status).toBe(400);
  });

  it("PUT - Update raw material: no token.", async () => {
    const res = await rawMaterial.selectAllRawMaterial({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random raw material id
    const id = rand.id;

    const info = {
      description: randomString(5),
      updated_by: "1",
      users_id: "1"
    };

    const ress = await rawMaterial.updateRawMaterial({ id, info });
    expect(ress.status).toBe(403);
  });
});
