const { truckInfo, randomString, getRandomInt } = require("./truck-info");
const { truckTypes } = require("./truck-types");
const { users } = require("./users");
const { seeds } = require("./seeders");

let token = "";

const prereq = async () => {
  try {
    await seeds.seeders({});

    // seeded data
    const info = {
      employee_id: "154151000",
      password: "password"
    };

    const res = await users.loginUser({ info });
    token = res.data.patched.token;
  } catch (e) {
    console.log("Error: ", e);
  }
};

beforeAll(async () => {
  try {
    await prereq();
  } catch (e) {
    console.log(e);
  }
});

// functions
describe("Truck info routes.", () => {
  it("POST - Insert new truck info.", async () => {
    // insert truck type
    const info = {
      truck_type: randomString(5),
      truck_model: randomString(4),
      truck_size: getRandomInt(10),
      users_id: "1"
    };

    await truckTypes.insertTruckType({ token, info });

    // select one truck type; then get id
    const res = await truckTypes.selectAllTruckTypes({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random truck types id
    const id = rand.id;

    const infos = {
      users_id: "1",
      truck_type_id: id,
      plate_number: randomString(5)
    };

    const ress = await truckInfo.insertTruckInfo({ token, info: infos });
    expect(ress.status).toBe(201);
  });

  it("GET - Select all truck info.", async () => {
    const res = await truckInfo.selectAllTruckInfo({ token });
    expect(res.status).toBe(200);
  });

  it("GET - Select one truck info.", async () => {
    const res = await truckInfo.selectAllTruckInfo({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random truck info id
    const id = rand.truckinfo_id;

    const ress = await truckInfo.selectOneTruckInfo({ id, token });
    expect(ress.status).toBe(200);
  });

  it("PUT - Update truck info.", async () => {
    const res = await truckInfo.selectAllTruckInfo({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random truck info id
    const id = rand.truckinfo_id;

    const info = {
      users_id: "1",
      truck_type_id: id,
      plate_number: randomString(5)
    };

    const ress = await truckInfo.updateTruckInfo({ id, token, info });
    expect(ress.status).toBe(200);
  });

  it("POST - Insert new truck info: missing required fields.", async () => {
    // insert truck type
    const info = {
      truck_type: randomString(5),
      truck_model: randomString(4),
      truck_size: getRandomInt(10),
      users_id: "1"
    };

    await truckTypes.insertTruckType({ token, info });

    // select one truck type; then get id
    const res = await truckTypes.selectAllTruckTypes({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random truck types id
    const id = rand.id;

    const infos = {
      users_id: "1",
      truck_type_id: "",
      plate_number: randomString(5)
    };

    const ress = await truckInfo.insertTruckInfo({ token, info: infos });
    expect(ress.status).toBe(400);
  });

  it("POST - Insert new truck info: no token.", async () => {
    // insert truck type
    const info = {
      truck_type: randomString(5),
      truck_model: randomString(4),
      truck_size: getRandomInt(10),
      users_id: "1"
    };

    await truckTypes.insertTruckType({ token, info });

    // select one truck type; then get id
    const res = await truckTypes.selectAllTruckTypes({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random truck types id
    const id = rand.id;

    const infos = {
      users_id: "1",
      truck_type_id: id,
      plate_number: randomString(5)
    };

    const ress = await truckInfo.insertTruckInfo({ info: infos });
    expect(ress.status).toBe(403);
  });

  it("GET - Select all truck info: no token.", async () => {
    const res = await truckInfo.selectAllTruckInfo({});
    expect(res.status).toBe(403);
  });

  it("GET - Select one truck info: no token.", async () => {
    const res = await truckInfo.selectAllTruckInfo({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random truck info id
    const id = rand.truckinfo_id;

    const ress = await truckInfo.selectOneTruckInfo({ id });
    expect(ress.status).toBe(403);
  });

  it("PUT - Update truck info: missing required fields.", async () => {
    const res = await truckInfo.selectAllTruckInfo({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random truck info id
    const id = rand.truckinfo_id;

    const info = {
      users_id: "1",
      truck_type_id: "",
      plate_number: randomString(5)
    };

    const ress = await truckInfo.updateTruckInfo({ id, token, info });
    expect(ress.status).toBe(400);
  });

  it("PUT - Update truck info: no token.", async () => {
    const res = await truckInfo.selectAllTruckInfo({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random truck info id
    const id = rand.truckinfo_id;

    const info = {
      users_id: "1",
      truck_type_id: id,
      plate_number: randomString(5)
    };

    const ress = await truckInfo.updateTruckInfo({ id, info });
    expect(ress.status).toBe(403);
  });
});
