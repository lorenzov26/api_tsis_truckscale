const { actions, randomString, getRandomInt } = require("./actions");
const { modules } = require("./modules");

const { users } = require("./users");
const { seeds } = require("./seeders");

let token = "";

const prereq = async () => {
  try {
    await seeds.seeders({});

    // seeded data
    const info = {
      employee_id: "154151000",
      password: "password"
    };

    const res = await users.loginUser({ info });
    token = res.data.patched.token;
  } catch (e) {
    console.log("Error: ", e);
  }
};

beforeAll(async () => {
  try {
    await prereq();
  } catch (e) {
    console.log(e);
  }
});

// functions
describe("Actions routes.", () => {
  it("POST - Insert new action.", async () => {
    const res = await modules.selectAllModules({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random module id
    const id = rand.id;

    const info = {
      module_id: id,
      description: randomString(10),
      created_by: "1",
      users_id: "1"
    };

    const ress = await actions.insertActions({ token, info });
    expect(ress.status).toBe(201);
  });

  it("GET - Select all actions.", async () => {
    const res = await actions.selectAllActions({ token });
    expect(res.status).toBe(200);
  });

  it("GET - Select one action.", async () => {
    const res = await actions.selectAllActions({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random action id
    const id = rand.id;

    const ress = await actions.selectOneAction({ id, token });
    expect(ress.status).toBe(200);
  });

  it("PUT - Update module.", async () => {
    const res = await modules.selectAllModules({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random module id
    const module_id = rand.id;

    // select action to update
    const res1 = await actions.selectAllActions({ token });
    const data1 = res1.data.view;
    const rand1 = data1[Math.floor(Math.random() * data1.length)];
    // random action id
    const id = rand1.id;

    const info = {
      module_id,
      description: randomString(10),
      modified_by: "1",
      users_id: "1",
      status: "active"
    };

    const ress = await actions.updateAction({ id, token, info });
    expect(ress.status).toBe(200);
  });

  it("POST - Insert new action: missing required fields.", async () => {
    const res = await modules.selectAllModules({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random module id
    const id = rand.id;

    const info = {
      module_id: "",
      description: randomString(10),
      created_by: "1",
      users_id: "1"
    };

    const ress = await actions.insertActions({ token, info });
    expect(ress.status).toBe(400);
  });

  it("POST - Insert new action: no token.", async () => {
    const res = await modules.selectAllModules({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random module id
    const id = rand.id;

    const info = {
      module_id: id,
      description: randomString(10),
      created_by: "1",
      users_id: "1"
    };

    const ress = await actions.insertActions({ info });
    expect(ress.status).toBe(403);
  });

  it("GET - Select all actions: no token.", async () => {
    const res = await actions.selectAllActions({});
    expect(res.status).toBe(403);
  });

  it("GET - Select one action.", async () => {
    const res = await actions.selectAllActions({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random action id
    const id = rand.id;

    const ress = await actions.selectOneAction({ id });
    expect(ress.status).toBe(403);
  });

  it("PUT - Update module: missing required fields.", async () => {
    const res = await modules.selectAllModules({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random module id
    const module_id = rand.id;

    // select action to update
    const res1 = await actions.selectAllActions({ token });
    const data1 = res1.data.view;
    const rand1 = data1[Math.floor(Math.random() * data1.length)];
    // random action id
    const id = rand1.id;

    const info = {
      module_id,
      description: "",
      modified_by: "1",
      users_id: "1",
      status: "active"
    };

    const ress = await actions.updateAction({ id, token, info });
    expect(ress.status).toBe(400);
  });

  it("PUT - Update module: no token.", async () => {
    const res = await modules.selectAllModules({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random module id
    const module_id = rand.id;

    // select action to update
    const res1 = await actions.selectAllActions({ token });
    const data1 = res1.data.view;
    const rand1 = data1[Math.floor(Math.random() * data1.length)];
    // random action id
    const id = rand1.id;

    const info = {
      module_id,
      description: randomString(10),
      modified_by: "1",
      users_id: "1",
      status: "active"
    };

    const ress = await actions.updateAction({ id, info });
    expect(ress.status).toBe(403);
  });
});
