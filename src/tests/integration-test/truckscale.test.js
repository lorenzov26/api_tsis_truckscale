const { truckscale, randomString, getRandomInt } = require("./truckscale");
const { users } = require("./users");
const { seeds } = require("./seeders");

let token = "";

const prereq = async () => {
  try {
    await seeds.seeders({});

    // seeded data
    const info = {
      employee_id: "154151000",
      password: "password"
    };

    const res = await users.loginUser({ info });
    token = res.data.patched.token;
  } catch (e) {
    console.log("Error: ", e);
  }
};

beforeAll(async () => {
  try {
    await prereq();
  } catch (e) {
    console.log(e);
  }
});

// functions
describe("Truckscales routes.", () => {
  it("POST - Insert new truckscale.", async () => {
    const info = {
      truckscale_name: randomString(5),
      truckscale_location: randomString(10),
      truckscale_length: getRandomInt(20),
      baudrate: "9600",
      parity: "none",
      databits: "8",
      stopbits: "1"
    };

    const res = await truckscale.insertTruckscale({ token, info });
    expect(res.status).toBe(201);
  });

  it("GET - Select all truckscale.", async () => {
    const res = await truckscale.selectAllTruckscale({ token });
    expect(res.status).toBe(200);
  });

  it("GET - Select one truckscale.", async () => {
    const res = await truckscale.selectAllTruckscale({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random truckscale id
    const id = rand.id;

    const ress = await truckscale.selectOneTruckscale({ id, token });
    expect(ress.status).toBe(200);
  });

  it("PUT - Update truckscale.", async () => {
    //   insert
    const ts_info = {
      truckscale_name: randomString(5),
      truckscale_location: randomString(10),
      truckscale_length: getRandomInt(20),
      baudrate: "9600",
      parity: "none",
      databits: "8",
      stopbits: "1"
    };

    await truckscale.insertTruckscale({ token, info: ts_info });

    //   select
    const res = await truckscale.selectAllTruckscale({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random truckscale id
    const id = rand.id;

    const info = {
      truckscale_name: randomString(5),
      truckscale_location: randomString(10),
      truckscale_length: getRandomInt(20),
      ip_address: "192.168.0.1",
      device_name: randomString(7),
      baudrate: "9600",
      parity: "none",
      databits: "8",
      stopbits: "1"
    };

    const ress = await truckscale.updateTruckscale({ id, token, info });
    expect(ress.status).toBe(200);
  });

  it("POST - Insert new truckscale: missing required fields.", async () => {
    const info = {
      truckscale_name: "",
      truckscale_location: randomString(10),
      truckscale_length: getRandomInt(20),
      baudrate: "9600",
      parity: "none",
      databits: "8",
      stopbits: "1"
    };

    const res = await truckscale.insertTruckscale({ token, info });
    expect(res.status).toBe(400);
  });

  it("POST - Insert new truckscale: no token.", async () => {
    const info = {
      truckscale_name: randomString(5),
      truckscale_location: randomString(10),
      truckscale_length: getRandomInt(20),
      baudrate: "9600",
      parity: "none",
      databits: "8",
      stopbits: "1"
    };

    const res = await truckscale.insertTruckscale({ info });
    expect(res.status).toBe(403);
  });

  it("GET - Select all truckscale: no token.", async () => {
    const res = await truckscale.selectAllTruckscale({});
    expect(res.status).toBe(403);
  });

  it("GET - Select one truckscale: no token.", async () => {
    const res = await truckscale.selectAllTruckscale({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random truckscale id
    const id = rand.id;

    const ress = await truckscale.selectOneTruckscale({ id });
    expect(ress.status).toBe(403);
  });

  it("PUT - Update truckscale: missing required fields.", async () => {
    //   insert
    const ts_info = {
      truckscale_name: randomString(5),
      truckscale_location: randomString(10),
      truckscale_length: getRandomInt(20),
      baudrate: "9600",
      parity: "none",
      databits: "8",
      stopbits: "1"
    };

    await truckscale.insertTruckscale({ token, info: ts_info });

    //   select
    const res = await truckscale.selectAllTruckscale({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random truckscale id
    const id = rand.id;

    const info = {
      truckscale_name: randomString(5),
      truckscale_location: randomString(10),
      truckscale_length: getRandomInt(20),
      ip_address: "",
      device_name: randomString(7),
      baudrate: "9600",
      parity: "none",
      databits: "8",
      stopbits: "1"
    };

    const ress = await truckscale.updateTruckscale({ id, token, info });
    expect(ress.status).toBe(400);
  });

  it("PUT - Update truckscale: no token.", async () => {
    //   insert
    const ts_info = {
      truckscale_name: randomString(5),
      truckscale_location: randomString(10),
      truckscale_length: getRandomInt(20),
      baudrate: "9600",
      parity: "none",
      databits: "8",
      stopbits: "1"
    };

    await truckscale.insertTruckscale({ token, info: ts_info });

    //   select
    const res = await truckscale.selectAllTruckscale({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random truckscale id
    const id = rand.id;

    const info = {
      truckscale_name: randomString(5),
      truckscale_location: randomString(10),
      truckscale_length: getRandomInt(20),
      ip_address: "192.168.0.1",
      device_name: randomString(7),
      baudrate: "9600",
      parity: "none",
      databits: "8",
      stopbits: "1"
    };

    const ress = await truckscale.updateTruckscale({ id, info });
    expect(ress.status).toBe(403);
  });
});
