const { tType, randomString, getRandomInt } = require("./transaction-type");
const { users } = require("./users");
const { seeds } = require("./seeders");

let token = "";

const prereq = async () => {
  try {
    await seeds.seeders({});

    // seeded data
    const info = {
      employee_id: "154151000",
      password: "password"
    };

    const res = await users.loginUser({ info });
    token = res.data.patched.token;
  } catch (e) {
    console.log("Error: ", e);
  }
};

beforeAll(async () => {
  try {
    await prereq();
  } catch (e) {
    console.log(e);
  }
});

// functions
describe("Transaction type routes.", () => {
  it("POST - Insert new transaction type.", async () => {
    const info = {
      name: randomString(5),
      users_id: "1"
    };

    const res = await tType.insertTtype({ token, info });
    expect(res.status).toBe(201);
  });

  it("GET - Select all transaction type.", async () => {
    const res = await tType.selectAllTtype({ token });
    expect(res.status).toBe(200);
  });

  it("GET - Select one transaction type.", async () => {
    const res = await tType.selectAllTtype({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random tType id
    const id = rand.id;

    const ress = await tType.selectOneTtype({ id, token });
    expect(ress.status).toBe(200);
  });

  it("PUT - Update transaction type.", async () => {
    const res = await tType.selectAllTtype({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random tType id
    const id = rand.id;

    const info = {
      name: randomString(5),
      address: randomString(10),
      contact_number: randomString(11),
      users_id: "1"
    };

    const ress = await tType.updateTtype({ id, token, info });
    expect(ress.status).toBe(200);
  });

  it("POST - Insert new transaction type: missing required fields.", async () => {
    const info = {
      name: "",
      users_id: "1"
    };

    const res = await tType.insertTtype({ token, info });
    expect(res.status).toBe(400);
  });

  it("POST - Insert new transaction type: no token.", async () => {
    const info = {
      name: randomString(5),
      users_id: "1"
    };

    const res = await tType.insertTtype({ info });
    expect(res.status).toBe(403);
  });

  it("GET - Select all transaction type: no token.", async () => {
    const res = await tType.selectAllTtype({});
    expect(res.status).toBe(403);
  });

  it("GET - Select one transaction type: no token.", async () => {
    const res = await tType.selectAllTtype({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random tType id
    const id = rand.id;

    const ress = await tType.selectOneTtype({ id });
    expect(ress.status).toBe(403);
  });

  it("PUT - Update transaction type: missing required fields.", async () => {
    const res = await tType.selectAllTtype({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random tType id
    const id = rand.id;

    const info = {
      name: "",
      address: randomString(10),
      contact_number: randomString(11),
      users_id: "1"
    };

    const ress = await tType.updateTtype({ id, token, info });
    expect(ress.status).toBe(400);
  });

  it("PUT - Update transaction type: no token.", async () => {
    const res = await tType.selectAllTtype({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random tType id
    const id = rand.id;

    const info = {
      name: randomString(5),
      address: randomString(10),
      contact_number: randomString(11),
      users_id: "1"
    };

    const ress = await tType.updateTtype({ id, info });
    expect(ress.status).toBe(403);
  });
});
