const axios = require("axios");
const dotenv = require("dotenv");
dotenv.config();

const seeds = {
  seeders: async ({ token }) => {
    try {
      const res = await axios({
        method: "POST",
        url: `${process.env.BASE_URL}/seeders/default`,
      });
      return res;
    } catch (e) {
      console.log("Error: ", e);
    }
  }
};

module.exports = { seeds };
