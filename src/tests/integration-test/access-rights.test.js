const { accessRights, randomString, getRandomInt } = require("./access-rights");
const { roles } = require("./roles");
const { users } = require("./users");
const { seeds } = require("./seeders");

let token = "";

const prereq = async () => {
  try {
    await seeds.seeders({});

    // seeded data
    const info = {
      employee_id: "154151000",
      password: "password"
    };

    const res = await users.loginUser({ info });
    token = res.data.patched.token;
  } catch (e) {
    console.log("Error: ", e);
  }
};

beforeAll(async () => {
  try {
    await prereq();
  } catch (e) {
    console.log(e);
  }
});

// functions
describe("Access rights routes.", () => {
  it("POST - Insert new access right.", async () => {
    const res = await roles.selectAllRoles({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random role id
    const id = rand.id;

    const info = {
      id,
      access_rights: [1, 2, 3]
    };

    const ress = await accessRights.insertAccessRights({ token, info });
    expect(ress.status).toBe(201);
  });

  it("GET - Select all access rights through role.", async () => {
    const res = await roles.selectAllRoles({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random role id
    const id = rand.id;

    const ress = await accessRights.selectAccessRights({ id, token });
    expect(ress.status).toBe(200);
  });

  it("POST - Insert new access right: missing required fields.", async () => {
    const res = await roles.selectAllRoles({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random role id
    const id = rand.id;

    const info = {
      id: "",
      access_rights: [1, 2, 3]
    };

    const ress = await accessRights.insertAccessRights({ token, info });
    expect(ress.status).toBe(400);
  });

  it("POST - Insert new access right: no token.", async () => {
    const res = await roles.selectAllRoles({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random role id
    const id = rand.id;

    const info = {
      id,
      access_rights: [1, 2, 3]
    };

    const ress = await accessRights.insertAccessRights({ info });
    expect(ress.status).toBe(403);
  });

  it("GET - Select all access rights through role: no token.", async () => {
    const res = await roles.selectAllRoles({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random role id
    const id = rand.id;

    const ress = await accessRights.selectAccessRights({ id });
    expect(ress.status).toBe(403);
  });
});
