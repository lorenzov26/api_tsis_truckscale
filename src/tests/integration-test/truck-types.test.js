const { truckTypes, randomString, getRandomInt } = require("./truck-types");
const { users } = require("./users");
const { seeds } = require("./seeders");

let token = "";

const prereq = async () => {
  try {
    await seeds.seeders({});

    // seeded data
    const info = {
      employee_id: "154151000",
      password: "password"
    };

    const res = await users.loginUser({ info });
    token = res.data.patched.token;
  } catch (e) {
    console.log("Error: ", e);
  }
};

beforeAll(async () => {
  try {
    await prereq();
  } catch (e) {
    console.log(e);
  }
});

// functions
describe("Truck types routes.", () => {
  it("POST - Insert new truck type.", async () => {
    const info = {
      truck_type: randomString(5),
      truck_model: randomString(4),
      truck_size: getRandomInt(10),
      users_id: "1"
    };

    const res = await truckTypes.insertTruckType({ token, info });
    expect(res.status).toBe(201);
  });

  it("GET - Select all truck types.", async () => {
    const res = await truckTypes.selectAllTruckTypes({ token });
    expect(res.status).toBe(200);
  });

  it("GET - Select one truck type.", async () => {
    const res = await truckTypes.selectAllTruckTypes({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random truck types id
    const id = rand.id;

    const ress = await truckTypes.selectOneTruckType({ id, token });
    expect(ress.status).toBe(200);
  });

  it("PUT - Update truck type.", async () => {
    const res = await truckTypes.selectAllTruckTypes({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random truck types id
    const id = rand.id;

    const info = {
      truck_type: randomString(5),
      truck_model: randomString(4),
      truck_size: getRandomInt(10),
      users_id: "1"
    };

    const ress = await truckTypes.updateTruckType({ id, token, info });
    expect(ress.status).toBe(200);
  });

  it("POST - Insert new truck type: missing required fields.", async () => {
    const info = {
      truck_type: "",
      truck_model: randomString(4),
      truck_size: getRandomInt(10),
      users_id: "1"
    };

    const res = await truckTypes.insertTruckType({ token, info });
    expect(res.status).toBe(400);
  });

  it("POST - Insert new truck type: no token.", async () => {
    const info = {
      truck_type: randomString(5),
      truck_model: randomString(4),
      truck_size: getRandomInt(10),
      users_id: "1"
    };

    const res = await truckTypes.insertTruckType({ info });
    expect(res.status).toBe(403);
  });

  it("GET - Select all truck types: no token.", async () => {
    const res = await truckTypes.selectAllTruckTypes({});
    expect(res.status).toBe(403);
  });

  it("GET - Select one truck type: no token.", async () => {
    const res = await truckTypes.selectAllTruckTypes({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random truck types id
    const id = rand.id;

    const ress = await truckTypes.selectOneTruckType({ id });
    expect(ress.status).toBe(403);
  });

  it("PUT - Update truck type: missing required fields.", async () => {
    const res = await truckTypes.selectAllTruckTypes({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random truck types id
    const id = rand.id;

    const info = {
      truck_type: "",
      truck_model: randomString(4),
      truck_size: getRandomInt(10),
      users_id: "1"
    };

    const ress = await truckTypes.updateTruckType({ id, token, info });
    expect(ress.status).toBe(400);
  });

  it("PUT - Update truck type: no token.", async () => {
    const res = await truckTypes.selectAllTruckTypes({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random truck types id
    const id = rand.id;

    const info = {
      truck_type: randomString(5),
      truck_model: randomString(4),
      truck_size: getRandomInt(10),
      users_id: "1"
    };

    const ress = await truckTypes.updateTruckType({ id, info });
    expect(ress.status).toBe(403);
  });
});
