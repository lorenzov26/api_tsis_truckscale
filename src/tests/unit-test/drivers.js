// import all functions from drivers
const {
  addNewDrivers,
  updateDrivers,
  driversSelectAlls,
  driversSelectOnes
} = require("../../use-cases/drivers/app");
const rs = require("randomstring"); // generate random string

// function to generate random string
const randomString = i => {
  const str = rs.generate({
    length: i,
    charset: "alphabetic"
  });
  return str;
};

const app = {
  // select all driver
  selectAllDriver: async () => {
    let bool = false;
    let res;
    try {
      res = await driversSelectAlls({});
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  // select single driver
  selectOneDriver: async ({ id }) => {
    let bool = false;
    let res;
    try {
      res = await driversSelectOnes(id);
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  // insert new driver
  insertNewDriver: async ({ info }) => {
    let bool = false;
    let res;
    try {
      res = await addNewDrivers(info);
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  // update a driver
  updateDriver: async ({ id, ...info }) => {
    let bool = false;
    let res;
    try {
      res = await updateDrivers({ id, ...info });
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  }
};

module.exports = { app, randomString };
