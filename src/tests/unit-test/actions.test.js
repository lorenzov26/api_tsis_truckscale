const { app, randomString } = require("./actions");
const mod = require("./modules").app;
const { db } = require("../../db/app");
const dotenv = require("dotenv");
dotenv.config();
const { uc_seedDefault } = require("../../use-cases/seeders/app");
const database = async () => {
  try {
    process.env.PGDATABASE = await db();
    await uc_seedDefault();
  } catch (e) {
    console.log(e);
  }
};

beforeAll(async () => {
  await database();
});


// functions
describe("Actions use cases functions.", () => {
  test("INSERT new action.", async () => {
    const res = await mod.selectAllModules({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    // random module id
    const id = rand.id;

    const info = {
      module_id: id,
      description: randomString(10),
      created_by: "1",
      users_id: "1"
    };

    const data = await app.insertNewAction({ info });
    expect(data.bool).toBe(true);
  });

  test("SELECT all actions.", async () => {
    const res = await app.selectAllAction({});
    expect(res.bool).toBe(true);
  });

  test("SELECT one action.", async () => {
    const res = await app.selectAllAction({});
    // select one id
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id;
    const data = await app.selectOneAction({ id });
    expect(data.res.length).not.toBe(0);
  });

  test("UPDATE action.", async () => {
    const req = await app.selectAllAction({});
    const rands = req.res[Math.floor(Math.random() * req.res.length)];
    // random action id
    const aid = rands.id;

    const res = await mod.selectAllModules({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    // random module id
    const id = rand.id;

    const info = {
      module_id: id,
      description: randomString(10),
      modified_by: "1",
      users_id: "1",
      status: "active"
    };

    const data = await app.updateAction({ id: aid, ...info });
    expect(data.bool).toBe(true);
  });

  test("SELECT one action - no id.", async () => {
    const res = await app.selectAllAction({});
    // select one id
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id;
    const data = await app.selectOneAction({});
    expect(data.res.length).toBe(0);
  });

  test("INSERT new action - missing required fields.", async () => {
    const res = await mod.selectAllModules({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    // random module id
    const id = rand.id;

    const info = {
      module_id: "",
      description: randomString(10),
      created_by: "1",
      users_id: "1"
    };

    const data = await app.insertNewAction({ info });
    expect(data.bool).toBe(false);
  });

  test("UPDATE action - missing required fields.", async () => {
    const req = await app.selectAllAction({});
    const rands = req.res[Math.floor(Math.random() * req.res.length)];
    // random action id
    const aid = rands.id;

    const res = await mod.selectAllModules({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    // random module id
    const id = rand.id;

    const info = {
      module_id: "",
      description: randomString(10),
      modified_by: "1",
      users_id: "1",
      status: "active"
    };

    const data = await app.updateAction({ id: aid, ...info });
    expect(data.bool).toBe(false);
  });
});
