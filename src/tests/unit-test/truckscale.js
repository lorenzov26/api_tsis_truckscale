const {
  addNewTruckScales,
  tsSelectAlls,
  tsSelectOnes,
  updateTruckScaless
} = require("../../use-cases/truckscale/app");
const rs = require("randomstring"); // generate random string

// function to generate random string
const randomString = i => {
  const str = rs.generate({
    length: i,
    charset: "alphabetic"
  });
  return str;
};

const app = {
  selectAllTruckScale: async () => {
    let bool = false;
    let res;
    try {
      res = await tsSelectAlls({});
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  selectOneTruckScale: async id => {
    let bool = false;
    let res;
    try {
      res = await tsSelectOnes({ id });
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  insertTruckScale: async ({ info }) => {
    let bool = false;
    let res;
    try {
      res = await addNewTruckScales(info);
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  updateTruckScale: async ({ id, ...info }) => {
    let bool = false;
    let res;
    try {
      res = await updateTruckScaless({ id, ...info });
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  }
};

module.exports = { app, randomString };
