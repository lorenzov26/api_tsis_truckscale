const {
  addAccessRightss,
  accessRightsSelectAllOnRoles
} = require("../../use-cases/access_rights/app");
const rs = require("randomstring"); // generate random string

// function to generate random string
const randomString = i => {
  const str = rs.generate({
    length: i,
    charset: "alphabetic"
  });
  return str;
};

const app = {
  // insert new accessrights
  inserAccessRights: async ({ info }) => {
    let bool = false;
    let res;
    try {
      res = await addAccessRightss(info);
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  // select all access rights base on role
  selectAllAccess: async ({ id }) => {
    let bool = false;
    let res;
    try {
      res = await accessRightsSelectAllOnRoles(id);
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  }
};

module.exports = { app, randomString };
