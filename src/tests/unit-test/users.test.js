const { app, randomString } = require("./users");
const { db } = require("../../db/app");
const dotenv = require("dotenv");
dotenv.config();
const { uc_seedDefault } = require("../../use-cases/seeders/app");
const database = async () => {
  try {
    process.env.PGDATABASE = await db();
    await uc_seedDefault();
  } catch (e) {
    console.log(e);
  }
};

beforeAll(async () => {
  await database();
});

// return an integer function
const getRandomInt = max => {
  return Math.floor(Math.random() * Math.floor(max));
};

// functions
describe("Users use cases functions.", () => {
  test("INSERT one user.", async () => {
    const i = getRandomInt(999);
    const info = {
      employee_id: `154151${i}`,
      email: `${randomString(5)}.${randomString(6)}@biotechfarms.net`,
      firstname: randomString(5),
      middlename: "",
      lastname: randomString(5),
      name_extension: "",
      password: `154151${i}`,
      role_id: "1",
      employee_image: "",
      employee_signature: "",
      contact_number: `0${getRandomInt(10000)}`,
      users_id: "1"
    };

    const res = await app.insertUser({ info });
    expect(res.bool).toBe(true);
  });

  test("SELECT all users.", async () => {
    const res = await app.selectAllUsers({});
    expect(res.bool).toBe(true);
  });

  test("SELECT one user.", async () => {
    const res = await app.selectAllUsers({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id; // random user id

    const data = await app.selectOneUser(id);
    expect(data.res.length).not.toBe(0);
  });

  test("UPDATE user.", async () => {
    const res = await app.selectAllUsers({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id; // random user id

    const i = getRandomInt(999);
    const info = {
      employee_id: `154151${i}`,
      email: `${randomString(5)}.${randomString(6)}@biotechfarms.net`,
      firstname: randomString(5),
      middlename: "",
      lastname: randomString(5),
      name_extension: "",
      password: `154151${i}`,
      role_id: "1",
      employee_image: "",
      employee_signature: "",
      contact_number: `0${getRandomInt(10000)}`,
      users_id: "1"
    };

    const data = await app.updateUser({ id, ...info });
    expect(data.bool).toBe(true);
  });

  test("LOGIN user.", async () => {
    const i = getRandomInt(999);
    const info = {
      employee_id: `154151${i}`,
      email: `${randomString(5)}.${randomString(6)}@biotechfarms.net`,
      firstname: randomString(5),
      middlename: "",
      lastname: randomString(5),
      name_extension: "",
      password: `154151${i}`,
      role_id: "1",
      employee_image: "",
      employee_signature: "",
      contact_number: `0${getRandomInt(10000)}`,
      users_id: "1"
    };

    // insert user
    await app.insertUser({ info });

    const data = {
      employee_id: `154151${i}`,
      password: `154151${i}`
    };
    const res = await app.login({ info: data });
    expect(res.bool).toBe(true);
  });

  test("INSERT one user: missing required fields.", async () => {
    const i = getRandomInt(999);
    const info = {
      employee_id: ``,
      email: `${randomString(5)}.${randomString(6)}@biotechfarms.net`,
      firstname: randomString(5),
      middlename: "",
      lastname: randomString(5),
      name_extension: "",
      password: `154151${i}`,
      role_id: "1",
      employee_image: "",
      employee_signature: "",
      contact_number: `0${getRandomInt(10000)}`,
      users_id: "1"
    };

    const res = await app.insertUser({ info });
    expect(res.bool).toBe(false);
  });

  test("SELECT one user: no id.", async () => {
    const res = await app.selectAllUsers({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id; // random user id

    const data = await app.selectOneUser();
    expect(data.res.length).toBe(0);
  });

  test("UPDATE user: missing required fields.", async () => {
    const res = await app.selectAllUsers({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id; // random user id

    const i = getRandomInt(999);
    const info = {
      employee_id: ``,
      email: `${randomString(5)}.${randomString(6)}@biotechfarms.net`,
      firstname: randomString(5),
      middlename: "",
      lastname: randomString(5),
      name_extension: "",
      password: `154151${i}`,
      role_id: "1",
      employee_image: "",
      employee_signature: "",
      contact_number: `0${getRandomInt(10000)}`,
      users_id: "1"
    };

    const data = await app.updateUser({ id, ...info });
    expect(data.bool).toBe(false);
  });

  test("LOGIN user: missing credentials.", async () => {
    const i = getRandomInt(999);
    const info = {
      employee_id: `154151${i}`,
      email: `${randomString(5)}.${randomString(6)}@biotechfarms.net`,
      firstname: randomString(5),
      middlename: "",
      lastname: randomString(5),
      name_extension: "",
      password: `154151${i}`,
      role_id: "1",
      employee_image: "",
      employee_signature: "",
      contact_number: `0${getRandomInt(10000)}`,
      users_id: "1"
    };

    // insert user
    await app.insertUser({ info });

    const data = {
      employee_id: `154151${i}`,
      password: ``
    };
    const res = await app.login({ info: data });
    expect(res.bool).toBe(false);
  });

  test("UPDATE user password.", async () => {
    const res = await app.selectAllUsers({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id; // random user id

    const pw = randomString(10);

    const info = {
      password: pw,
      repassword: pw
    };

    const data = await app.resetPassword({ id, ...info });
    expect(data.bool).toBe(true);
  });

  test("UPDATE user password: password doesn't match.", async () => {
    const res = await app.selectAllUsers({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id; // random user id

    const pw = randomString(10);

    const info = {
      password: pw,
      repassword: `${pw}s`
    };

    const data = await app.resetPassword({ id, ...info });
    expect(data.bool).toBe(false);
  });

  test("UPDATE user password: no password.", async () => {
    const res = await app.selectAllUsers({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id; // random user id

    const pw = randomString(10);

    const info = {
      password: "",
      repassword: pw
    };

    const data = await app.resetPassword({ id, ...info });
    expect(data.bool).toBe(false);
  });

  test("SELECT all users.", async () => {
    const res = await app.selectAllActivityLogs({});
    expect(res.bool).toBe(true);
  });
});
