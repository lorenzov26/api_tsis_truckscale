const {
  addNewRoless,
  updateRoless,
  rolesSelectAlls,
  rolesSelectOnes
} = require("../../use-cases/roles/app");
const rs = require("randomstring"); // generate random string

// function to generate random string
const randomString = i => {
  const str = rs.generate({
    length: i,
    charset: "alphabetic"
  });
  return str;
};

const app = {
  selectAllRoles: async () => {
    let bool = false;
    let res;
    try {
      res = await rolesSelectAlls({});
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  selectOneRole: async ({ id }) => {
    let bool = false;
    let res;
    try {
      res = await rolesSelectOnes({ id });
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  insertRole: async ({ info }) => {
    let bool = false;
    let res;
    try {
      res = await addNewRoless(info);
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  updateRole: async ({ id, ...info }) => {
    let bool = false;
    let res;
    try {
      res = await updateRoless({ id, ...info });
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  }
};

module.exports = { app, randomString };
