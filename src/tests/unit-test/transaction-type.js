const {
  addNewTransactionTypes,
  transactionTypeSelectAlls,
  transactionTypeSelectOnes,
  updateTransactionTypes
} = require("../../use-cases/transaction-type/app");
const rs = require("randomstring"); // generate random string

// function to generate random string
const randomString = i => {
  const str = rs.generate({
    length: i,
    charset: "alphabetic"
  });
  return str;
};

const app = {
  selectAllTtype: async () => {
    let bool = false;
    let res;
    try {
      res = await transactionTypeSelectAlls({});
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  selectOneTtype: async ({ id }) => {
    let bool = false;
    let res;
    try {
      res = await transactionTypeSelectOnes({ id });
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  insertTtype: async ({ info }) => {
    let bool = false;
    let res;
    try {
      res = await addNewTransactionTypes(info);
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  updateTtype: async ({ id, ...info }) => {
    let bool = false;
    let res;
    try {
      res = await updateTransactionTypes({ id, ...info });
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  }
};

module.exports = { app, randomString };
