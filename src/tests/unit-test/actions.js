const {
  addNewActions,
  updateActionss,
  actionsSelectAlls,
  actionsSelectOnes
} = require("../../use-cases/actions/app");

const rs = require("randomstring"); // generate random string

// function to generate random string
const randomString = i => {
  const str = rs.generate({
    length: i,
    charset: "alphabetic"
  });
  return str;
};

const app = {
  selectAllAction: async () => {
    let bool = false;
    let res;
    try {
      res = await actionsSelectAlls({});
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  selectOneAction: async ({ id }) => {
    let bool = false;
    let res;
    try {
      res = await actionsSelectOnes(id);
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  insertNewAction: async ({ info }) => {
    let bool = false;
    let res;
    try {
      res = await addNewActions(info);
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  updateAction: async ({ id, ...info }) => {
    let bool = false;
    let res;
    try {
      res = await updateActionss({ id, ...info });
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  }
};

module.exports = { app, randomString };
