const {
  completedTransactionSelectAlls
} = require("../../use-cases/reports/app");
const rs = require("randomstring"); // generate random string

// function to generate random string
const randomString = i => {
  const str = rs.generate({
    length: i,
    charset: "alphabetic"
  });
  return str;
};

const app = {
  selectAllCompletedTransactions: async () => {
    let bool = false;
    let res;
    try {
      res = await completedTransactionSelectAlls({});
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  }
};

module.exports = { app, randomString };
