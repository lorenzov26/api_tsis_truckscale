const { app, randomString } = require("./raw-material");
const { db } = require("../../db/app");
const dotenv = require("dotenv");
dotenv.config();
const { uc_seedDefault } = require("../../use-cases/seeders/app");
const database = async () => {
  try {
    process.env.PGDATABASE = await db();
    await uc_seedDefault();
  } catch (e) {
    console.log(e);
  }
};

beforeAll(async () => {
  await database();
});

// functions
describe("Raw Material use cases functions.", () => {
  it("INSERT new raw material.", async () => {
    const info = {
      description: randomString(5),
      created_by: "1",
      users_id: "1"
    };

    const res = await app.insertRawMaterial({ info });
    expect(res.bool).toBe(true);
  });

  it("SELECT all raw material.", async () => {
    const res = await app.selectAllRawMaterial({});
    expect(res.bool).toBe(true);
  });

  it("SELECT one raw material.", async () => {
    const res = await app.selectAllRawMaterial({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id;

    const data = await app.selectOneRawMaterial({ id });
    expect(data.res.length).not.toBe(0);
  });

  it("UPDATE one raw material.", async () => {
    const res = await app.selectAllRawMaterial({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id;

    const info = {
      description: randomString(5),
      updated_by: "1",
      users_id: "1"
    };

    const data = await app.updateRawMaterial({ id, ...info });
    expect(data.bool).toBe(true);
  });

  it("INSERT new raw material: missing required fields.", async () => {
    const info = {
      description: "",
      created_by: "1",
      users_id: "1"
    };

    const res = await app.insertRawMaterial({ info });
    expect(res.bool).toBe(false);
  });

  it("SELECT one raw material: no id.", async () => {
    const res = await app.selectAllRawMaterial({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id;

    const data = await app.selectOneRawMaterial({});
    expect(data.res.length).toBe(0);
  });

  it("UPDATE one raw material: missing required fields.", async () => {
    const res = await app.selectAllRawMaterial({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id;

    const info = {
      description: "",
      updated_by: "1",
      users_id: "1"
    };

    const data = await app.updateRawMaterial({ id, ...info });
    expect(data.bool).toBe(false);
  });
});
