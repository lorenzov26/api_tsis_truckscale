const {
  addNewUsers,
  usersSelectAlls,
  usersSelectOnes,
  loginUsers,
  uc_updateUser,
  resetPasswords,
  selectActivityLogss
} = require("../../use-cases/users/app");
const rs = require("randomstring"); // generate random string

// function to generate random string
const randomString = i => {
  const str = rs.generate({
    length: i,
    charset: "alphabetic"
  });
  return str;
};

const app = {
  insertUser: async ({ info }) => {
    let bool = false;
    let res;
    try {
      res = await addNewUsers(info);
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  selectAllUsers: async () => {
    let bool = false;
    let res;
    try {
      res = await usersSelectAlls({});
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  selectOneUser: async id => {
    let bool = false;
    let res;
    try {
      res = await usersSelectOnes(id);
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  updateUser: async ({ id, ...info }) => {
    let bool = false;
    let res;
    try {
      res = await uc_updateUser({ id, ...info });
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  login: async ({ info }) => {
    let bool = false;
    let res;
    try {
      res = await loginUsers(info);
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  resetPassword: async ({ id, ...info }) => {
    let bool = false;
    let res;
    try {
      res = await resetPasswords({ id, ...info });
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  selectAllActivityLogs: async () => {
    let bool = false;
    let res;
    try {
      res = await selectActivityLogss({});
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  }
};

module.exports = { app, randomString };
