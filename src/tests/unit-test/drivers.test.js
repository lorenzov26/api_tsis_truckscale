const { app, randomString } = require("./drivers");
const { db } = require("../../db/app");
const dotenv = require("dotenv");
dotenv.config();
const { uc_seedDefault } = require("../../use-cases/seeders/app");
const database = async () => {
  try {
    process.env.PGDATABASE = await db();
    await uc_seedDefault();
  } catch (e) {
    console.log(e);
  }
};

beforeAll(async () => {
  await database();
});


// functions
describe("Driver use cases functions.", () => {
  test("INSERT new driver.", async () => {
    // dummy data
    const info = {
      firstname: randomString(5),
      middlename: randomString(5),
      lastname: randomString(5),
      name_extension: randomString(2)
    };
    const res = await app.insertNewDriver({ info });
    expect(res.bool).toBe(true);
  });

  test("SELECT all drivers.", async () => {
    const res = await app.selectAllDriver({});
    expect(res.bool).toBe(true);
  });

  test("SELECT one driver.", async () => {
    const res = await app.selectAllDriver({});
    // select random driver; then get id
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id;
    const data = await app.selectOneDriver({ id });
    expect(data.res.length).not.toBe(0);
  });

  test("UPDATE one driver.", async () => {
    const res = await app.selectAllDriver({});
    // select random driver; then get id
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id;
    // dummy data
    const info = {
      firstname: randomString(5),
      middlename: randomString(5),
      lastname: randomString(5),
      name_extension: randomString(2)
    };
    const data = await app.updateDriver({ id, ...info });
    expect(data.bool).toBe(true);
  });

  test("SELECT one driver - no id.", async () => {
    const res = await app.selectAllDriver({});
    // select random driver; then get id
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id;
    const data = await app.selectOneDriver({});
    expect(data.res.length).toBe(0); // selected data must be always zero
  });

  test("INSERT new driver - missing required fields.", async () => {
    // dummy data
    const info = {
      firstname: "",
      middlename: randomString(5),
      lastname: randomString(5),
      name_extension: randomString(2)
    };
    const res = await app.insertNewDriver({ info });
    expect(res.bool).toBe(false);
  });

  test("UPDATE one driver - missing required fields.", async () => {
    const res = await app.selectAllDriver({});
    // select random driver; then get id
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id;
    // dummy data
    const info = {
      firstname: "",
      middlename: randomString(5),
      lastname: randomString(5),
      name_extension: randomString(2)
    };
    const data = await app.updateDriver({ id, ...info });
    expect(data.bool).toBe(false);
  });

  test("UPDATE one driver - no id.", async () => {
    const res = await app.selectAllDriver({});
    // select random driver; then get id
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id;
    // dummy data
    const info = {
      firstname: randomString(5),
      middlename: randomString(5),
      lastname: randomString(5),
      name_extension: randomString(2)
    };
    const data = await app.updateDriver({ ...info });
    expect(data.bool).toBe(false);
  });
});
