const {
  uc_addTruckInfo,
  uc_selectAllTruckInfos,
  uc_selectOneTruckInfo,
  uc_updateTruckInfo
} = require("../../use-cases/truck-infos/app");
const rs = require("randomstring"); // generate random string

// function to generate random string
const randomString = i => {
  const str = rs.generate({
    length: i,
    charset: "alphabetic"
  });
  return str;
};

const app = {
  selectAllTruckInfo: async () => {
    let bool = false;
    let res;
    try {
      res = await uc_selectAllTruckInfos({});
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  selectOneTruckInfo: async id => {
    let bool = false;
    let res;
    try {
      res = await uc_selectOneTruckInfo({ id });
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  insertTruckInfo: async ({ info }) => {
    let bool = false;
    let res;
    try {
      res = await uc_addTruckInfo(info);
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  updateTruckInfo: async ({ id, ...info }) => {
    let bool = false;
    let res;
    try {
      res = await uc_updateTruckInfo({ id, ...info });
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  }
};

module.exports = { app, randomString };
