const { app, randomString } = require("./reports");
const { db } = require("../../db/app");
const dotenv = require("dotenv");
dotenv.config();
const { uc_seedDefault } = require("../../use-cases/seeders/app");
const database = async () => {
  try {
    process.env.PGDATABASE = await db();
    await uc_seedDefault();
  } catch (e) {
    console.log(e);
  }
};

beforeAll(async () => {
  await database();
});


// functions
describe("Reports use cases functions.", () => {
  test("SELECT all completed transactions.", async () => {
    const res = await app.selectAllCompletedTransactions({});
    expect(res.bool).toBe(true);
  });
});
