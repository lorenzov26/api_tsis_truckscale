const { app, randomString } = require("./access-rights");
const apps = require("./roles").app; //roles

const { db } = require("../../db/app");
const dotenv = require("dotenv");
dotenv.config();
const { uc_seedDefault } = require("../../use-cases/seeders/app");
const database = async () => {
  try {
    process.env.PGDATABASE = await db();
    await uc_seedDefault();
  } catch (e) {
    console.log(e);
  }
};

beforeAll(async () => {
  await database();
});


// functions
describe("Access rights use cases functions.", () => {
  test("INSERT new access rights.", async () => {
    const res = await apps.selectAllRoles({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    // random role id
    const id = rand.id;
    const info = {
      id,
      access_rights: [1, 2, 3]
    };
    const data = await app.inserAccessRights({ info });
    expect(data.bool).toBe(true);
  });

  test("SELECT access rights base on role.", async () => {
    const res = await apps.selectAllRoles({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    // random role id
    const id = rand.id;
    const data = await app.selectAllAccess({ id });
    expect(data.bool).toBe(true);
  });
});
