const {
  addNewTransactions,
  transactionSelectAlls,
  uc_addInbound,
  uc_addOutbound
} = require("../../use-cases/transactions/app");
const rs = require("randomstring"); // generate random string

// function to generate random string
const randomString = i => {
  const str = rs.generate({
    length: i,
    charset: "alphabetic"
  });
  return str;
};

const app = {
  selectAllTransactions: async ({ info }) => {
    let bool = false;
    let res;
    try {
      res = await transactionSelectAlls(info);
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  insertTransactions: async ({ info }) => {
    let bool = false;
    let res;
    try {
      res = await addNewTransactions(info);
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  addInbound: async ({ info }) => {
    let bool = false;
    let res;
    try {
      res = await uc_addInbound(info);
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  addOutbound: async ({ info }) => {
    let bool = false;
    let res;
    try {
      res = await uc_addOutbound(info);
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  }
};

module.exports = { app, randomString };
