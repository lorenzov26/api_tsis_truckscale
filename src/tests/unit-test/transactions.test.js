const { app, randomString } = require("./transactions");
const truckType = require("./truck-types").app;
const truckInfo = require("./truck-info").app;
const drivers = require("./drivers").app;
const truckScale = require("./truckscale").app;

const { db } = require("../../db/app");
const dotenv = require("dotenv");
dotenv.config();
const { uc_seedDefault } = require("../../use-cases/seeders/app");
const database = async () => {
  try {
    process.env.PGDATABASE = await db();
    await uc_seedDefault();
  } catch (e) {
    console.log(e);
  }
};

beforeAll(async () => {
  await database();
});

// mock purchase ID; return an integer function
const getRandomInt = max => {
  return Math.floor(Math.random() * Math.floor(max));
};

// functions
describe("Transactions use cases functions.", () => {
  test("INSERT delivery transactions.", async () => {
    // insert truck type
    const t_info = {
      truck_type: randomString(5),
      truck_model: randomString(4),
      truck_size: 4.5,
      users_id: "1"
    };

    await truckType.insertTruckType({ info: t_info });

    const plateNumber = randomString(5);

    // insert plate number
    const res = await truckType.selectAllTruckTypes({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id; // random truck type id

    const info = {
      users_id: "1",
      truck_type_id: id,
      plate_number: plateNumber
    };

    await truckInfo.insertTruckInfo({ info });
    // end insert plate number

    // insert truckscale
    const ts_info = {
      truckscale_name: randomString(5),
      truckscale_location: randomString(10),
      truckscale_length: getRandomInt(20),
      baudrate: "9600",
      parity: "none",
      databits: "8",
      stopbits: "1"
    };
    await truckScale.insertTruckScale({ info: ts_info });
    

    const infos = {
      purchase_order_id:  randomString(5),
      transaction_type_name: "Delivery",
      created_by: "1",
      tracking_code: randomString(5),
      no_of_bags: getRandomInt(500),
      driver: {
        firstname: randomString(3),
        lastname: randomString(3)
      },
      plate_number: plateNumber,
      users_id: "1",
      truckscale_id: "1"
    };

    const data = await app.insertTransactions({ info: infos });

    expect(data.res.hasInbound).toBe(true);
  });

  test("INSERT others transactions.", async () => {
    const res = await truckInfo.selectAllTruckInfo({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const truckInfoId = rand.truckinfo_id; // random truck info id

    const ress = await drivers.selectAllDriver({});
    // select random driver; then get id
    const rands = ress.res[Math.floor(Math.random() * ress.res.length)];
    const driversId = rands.id;

    const info = {
     
      transaction_type_name: "Others",
      created_by: "1",
      drivers_id: driversId,
      truck_info_id: truckInfoId
    };

    const data = await app.insertTransactions({ info });
    expect(data.bool).toBe(true);
  });

  test("SELECT all transactions.", async () => {
    const info = {
      from: new Date(),
      to: new Date()
    };
    const res = await app.selectAllTransactions({ info });
    expect(res.bool).toBe(true);
  });

  test("INSERT inbound and outbound weight of transaction.", async () => {
    const plateNumber = randomString(5);

    // insert plate number
    const res = await truckType.selectAllTruckTypes({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id; // random truck type id

    const info = {
      users_id: "1",
      truck_type_id: id,
      plate_number: plateNumber
    };

    await truckInfo.insertTruckInfo({ info });
    // end insert plate number

    // insert truckscale;
    const ts_info = {
      truckscale_name: randomString(5),
      truckscale_location: randomString(10),
      truckscale_length: getRandomInt(20),
      baudrate: "9600",
      parity: "none",
      databits: "8",
      stopbits: "1"
    };
    await truckScale.insertTruckScale({ info: ts_info });

    // select random truckscale
    const ts = await truckScale.selectAllTruckScale({});
    const tsRand = ts.res[Math.floor(Math.random() * ts.res.length)];
    const tsId = tsRand.id; // random truck scale id

    const infos = {
      purchase_order_id:  randomString(5),
      transaction_type_name: "Delivery",
      created_by: "1",
      tracking_code: randomString(5),
      no_of_bags: getRandomInt(500),
      driver: {
        firstname: randomString(3),
        lastname: randomString(3)
      },
      plate_number: plateNumber,
      users_id: "1",
      truckscale_id: tsId
    };

    const data = await app.insertTransactions({ info: infos });
    const transactionId = data.res.transactionId;

    const ib = {
      transaction_id: transactionId,
      ib_weight: getRandomInt(20000)
    };

    // insert inbound
    const inbound = await app.addInbound({ info: ib });
    expect(inbound.bool).toBe(true);

    // # RESCAN QR code scenario

    await app.insertTransactions({ info: infos });

    const ob = {
      transaction_id: transactionId,
      ob_weight: getRandomInt(20000),
      truckscale_id: tsId
    };

    // insert outbound
    const outbound = await app.addOutbound({ info: ob });
    expect(outbound.bool).toBe(true);
  });
});
