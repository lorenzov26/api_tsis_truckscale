const {
  addNewWeightTypes,
  weightTypeSelectAlls,
  weightTypeSelectOnes,
  updateWeightTypess
} = require("../../use-cases/weight-types/app");
const rs = require("randomstring"); // generate random string

// function to generate random string
const randomString = i => {
  const str = rs.generate({
    length: i,
    charset: "alphabetic"
  });
  return str;
};

const app = {
  insertWeightType: async ({ info }) => {
    let bool = false;
    let res;
    try {
      res = await addNewWeightTypes(info);
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  selectAllWeightType: async () => {
    let bool = false;
    let res;
    try {
      res = await weightTypeSelectAlls({});
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  selectOneWeightType: async id => {
    let bool = false;
    let res;
    try {
      res = await weightTypeSelectOnes({ id });
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  updateWeightType: async ({ id, ...info }) => {
    let bool = false;
    let res;
    try {
      res = await updateWeightTypess({ id, ...info });
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  }
};

module.exports = { app, randomString };
