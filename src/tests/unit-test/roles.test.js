const { app, randomString } = require("./roles");
const { db } = require("../../db/app");
const dotenv = require("dotenv");
dotenv.config();
const { uc_seedDefault } = require("../../use-cases/seeders/app");
const database = async () => {
  try {
    process.env.PGDATABASE = await db();
    await uc_seedDefault();
  } catch (e) {
    console.log(e);
  }
};

beforeAll(async () => {
  await database();
});


// functions
describe("Roles use cases functions.", () => {
  test("INSERT new role.", async () => {
    // 1 is the admin id; seeded data
    const info = {
      name: randomString(5),
      created_by: "1",
      users_id: "1"
    };
    const res = await app.insertRole({ info });
    expect(res.bool).toBe(true);
  });

  test("SELECT all roles.", async () => {
    const res = await app.selectAllRoles({});
    expect(res.bool).toBe(true);
  });

  test("SELECT one role.", async () => {
    const res = await app.selectAllRoles({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    // random role id
    const id = rand.id;
    const data = await app.selectOneRole({ id });
    expect(data.res.length).not.toBe(0);
  });

  test("UPDATE a role.", async () => {
    const res = await app.selectAllRoles({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    // random role id
    const id = rand.id;

    // 1 is the admin id; seeded data; actions array is seeded
    const info = {
      name: randomString(5),
      status: "active",
      modified_by: "1",
      users_id: "1",
      access_rights: [1, 2, 3]
    };
    const data = await app.updateRole({ id, ...info });
    expect(data.bool).toBe(true);
  });

  test("SELECT one role - no id.", async () => {
    const res = await app.selectAllRoles({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    // random role id
    const id = rand.id;
    const data = await app.selectOneRole({});
    expect(data.res.length).toBe(0);
  });

  test("INSERT new role - missing required fields.", async () => {
    // 1 is the admin id; seeded data
    const info = {
      name: "",
      created_by: "1",
      users_id: "1"
    };
    const res = await app.insertRole({ info });
    expect(res.bool).toBe(false);
  });

  test("UPDATE a role - missing required fields.", async () => {
    const res = await app.selectAllRoles({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    // random role id
    const id = rand.id;

    // 1 is the admin id; seeded data; actions array is seeded
    const info = {
      name: randomString(5),
      status: "active",
      modified_by: "1",
      users_id: "1"
    };
    const data = await app.updateRole({ id, ...info });
    expect(data.bool).toBe(false);
  });
});
