const { app, randomString } = require("./modules");
const { db } = require("../../db/app");
const dotenv = require("dotenv");
dotenv.config();
const { uc_seedDefault } = require("../../use-cases/seeders/app");
const database = async () => {
  try {
    process.env.PGDATABASE = await db();
    await uc_seedDefault();
  } catch (e) {
    console.log(e);
  }
};

beforeAll(async () => {
  await database();
});


// functions
describe("Modules use cases functions.", () => {
  it("INSERT new module.", async () => {
    const info = {
      description: randomString(10),
      status: "active",
      created_by: "1",
      users_id: "1"
    };

    const res = await app.insertNewModule({ info });
    expect(res.bool).toBe(true);
  });

  it("SELECT all modules.", async () => {
    const res = await app.selectAllModules({});
    expect(res.bool).toBe(true);
  });

  it("SELECT one module.", async () => {
    const res = await app.selectAllModules({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id;

    const data = await app.selectOneModule({ id });
    expect(data.res.length).not.toBe(0);
  });

  it("UPDATE one module.", async () => {
    const res = await app.selectAllModules({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id;

    const info = {
      description: randomString(10),
      status: "active",
      modified_by: "1",
      users_id: "1"
    };
    const data = await app.updateModule({ id, ...info });
    expect(data.bool).toBe(true);
  });

  it("SELECT all modules with actions.", async () => {
    const res = await app.selectModuleWithActions({});
    expect(res.bool).toBe(true);
  });

  it("SELECT one module - no id.", async () => {
    const res = await app.selectAllModules({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id;

    const data = await app.selectOneModule({});
    expect(data.res.length).toBe(0);
  });

  it("INSERT new module - missing required fields.", async () => {
    const info = {
      description: "",
      status: "active",
      created_by: "1",
      users_id: "1"
    };

    const res = await app.insertNewModule({ info });
    expect(res.bool).toBe(false);
  });

  it("UPDATE one module - missing required fields.", async () => {
    const res = await app.selectAllModules({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id;

    const info = {
      description: "",
      status: "active",
      modified_by: "1",
      users_id: "1"
    };
    const data = await app.updateModule({ id, ...info });
    expect(data.bool).toBe(false);
  });
});
