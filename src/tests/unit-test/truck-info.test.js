const { app, randomString } = require("./truck-info");
const truckType = require("./truck-types").app;

const { db } = require("../../db/app");
const dotenv = require("dotenv");
dotenv.config();
const { uc_seedDefault } = require("../../use-cases/seeders/app");
const database = async () => {
  try {
    process.env.PGDATABASE = await db();
    await uc_seedDefault();
  } catch (e) {
    console.log(e);
  }
};

beforeAll(async () => {
  await database();
});


// functions
describe("Truck info use cases functions.", () => {
  test("INSERT truck info.", async () => {
    const infos = {
      truck_type: randomString(5),
      truck_model: randomString(4),
      truck_size: 4.5,
      users_id: "1"
    };

    await truckType.insertTruckType({ info: infos });

    const res = await truckType.selectAllTruckTypes({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id; // random truck type id

    const info = {
      users_id: "1",
      truck_type_id: id,
      plate_number: randomString(5)
    };

    const data = await app.insertTruckInfo({ info });
    expect(data.bool).toBe(true);
  });

  test("SELECT all truck info.", async () => {
    const res = await app.selectAllTruckInfo({});
    expect(res.bool).toBe(true);
  });

  test("SELECT one truck info.", async () => {
    const res = await app.selectAllTruckInfo({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.truckinfo_id; // random truck info id
    const data = await app.selectOneTruckInfo(id);
    expect(data.res.length).not.toBe(0);
  });

  test("UPDATE truck info.", async () => {
    const res = await truckType.selectAllTruckTypes({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id; // random truck type id

    const info = {
      users_id: "1",
      truck_type_id: id,
      plate_number: randomString(5)
    };

    const ress = await app.selectAllTruckInfo({});
    const rands = ress.res[Math.floor(Math.random() * ress.res.length)];
    const ids = rands.truckinfo_id; // random truck info id

    const data = await app.updateTruckInfo({ id: ids, ...info });
    expect(data.bool).toBe(true);
  });

  test("SELECT one truck info - no id.", async () => {
    const res = await app.selectAllTruckInfo({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.truckinfo_id; // random truck info id
    const data = await app.selectOneTruckInfo();
    expect(data.res.length).toBe(0);
  });

  test("INSERT truck info - missing required fields.", async () => {
    const res = await truckType.selectAllTruckTypes({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id; // random truck type id

    const info = {
      users_id: "1",
      truck_type_id: id,
      plate_number: ""
    };

    const data = await app.insertTruckInfo({ info });
    expect(data.bool).toBe(false);
  });

  test("UPDATE truck info - missing required fields.", async () => {
    const res = await truckType.selectAllTruckTypes({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id; // random truck type id

    const info = {
      users_id: "1",
      truck_type_id: id,
      plate_number: ""
    };

    const ress = await app.selectAllTruckInfo({});
    const rands = ress.res[Math.floor(Math.random() * ress.res.length)];
    const ids = rands.truckinfo_id; // random truck info id

    const data = await app.updateTruckInfo({ id: ids, ...info });
    expect(data.bool).toBe(false);
  });
});
