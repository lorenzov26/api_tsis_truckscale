const { app, randomString } = require("./weight-type");
const { db } = require("../../db/app");
const dotenv = require("dotenv");
dotenv.config();
const { uc_seedDefault } = require("../../use-cases/seeders/app");
const database = async () => {
  try {
    process.env.PGDATABASE = await db();
    await uc_seedDefault();
  } catch (e) {
    console.log(e);
  }
};

beforeAll(async () => {
  await database();
});

// functions
describe("Weight type use cases functions.", () => {
  test("INSERT one weight type.", async () => {
    const info = {
      weight_name: randomString(5),
      users_id: "1"
    };

    const res = await app.insertWeightType({ info });
    expect(res.bool).toBe(true);
  });

  test("SELECT all weight type.", async () => {
    const res = await app.selectAllWeightType({});
    expect(res.bool).toBe(true);
  });

  test("SELECT one weight type.", async () => {
    const res = await app.selectAllWeightType({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id; // random weight type id

    const data = await app.selectOneWeightType(id);
    expect(data.res.length).not.toBe(0);
  });

  test("UPDATE weight type.", async () => {
    const res = await app.selectAllWeightType({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id; // random weight type id

    const info = {
      weight_name: randomString(5),
      users_id: "1"
    };

    const data = await app.updateWeightType({ id, ...info });
    expect(data.bool).toBe(true);
  });

  test("INSERT one weight type - missing required fields.", async () => {
    const info = {
      weight_name: "",
      users_id: "1"
    };

    const res = await app.insertWeightType({ info });
    expect(res.bool).toBe(false);
  });

  test("SELECT one weight type - no id.", async () => {
    const res = await app.selectAllWeightType({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id; // random weight type id

    const data = await app.selectOneWeightType();
    expect(data.res.length).toBe(0);
  });

  test("UPDATE weight type - missing required fields.", async () => {
    const res = await app.selectAllWeightType({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id; // random weight type id

    const info = {
      weight_name: "",
      users_id: "1"
    };

    const data = await app.updateWeightType({ id, ...info });
    expect(data.bool).toBe(false);
  });
});
