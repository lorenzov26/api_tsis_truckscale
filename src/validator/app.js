
const validateAccessRight = require("./validate-access-right")

const validateAccessRights = validateAccessRight()

const services = Object.freeze({
    validateAccessRights
});

module.exports = services;
module.exports = {
    validateAccessRights
};