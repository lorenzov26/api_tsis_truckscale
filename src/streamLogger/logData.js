const logData = ({ io, fs, path, moment }) => {

  return function log() {


    const logsDir = path.resolve("logs");

    if (!fs.existsSync(logsDir)) {
      fs.mkdirSync(logsDir);
    }

    io.on("connect", (socket) => {

      socket.on("log", async (data) => {

        try {
          const d = new Date();
          const date = moment(d).format("YYYY-MM-DD");
          const time = moment(d).format("HH:mm:ss A")

          const transactionId = data.transaction_id
          const weight = data.value
          const type = data.type
          const path = `${logsDir}/${transactionId}-${type}-log.txt`


          const fileExists = await new Promise((resolve) => {
            fs.access(path, fs.F_OK, (err) => {
              if (err) {
                resolve(false)
              }
              resolve(true)
            })
          })

          if (fileExists) {

            const readFile = await new Promise((resolve) => {
              fs.readFile(path, 'utf8', (err, data) => {
                if (err) {
                  resolve(false)
                };
                resolve(data)
              });
            })

            const fileContents = readFile.split('\n')

            if (fileContents.length > 50) {

              let linesExceptFirst = fileContents.slice(1).join('\n');

              const newContent = linesExceptFirst + `\nDate: ${date} ||Time: ${time} ||Weight: ${weight} KGS`


              const writeFile = await new Promise((resolve) => {
                fs.writeFile(path, newContent, (err) => {
                  if (err) {
                    resolve(false)
                  };
                  resolve(true)
                });
              })

            } else {

              const appendFile = await new Promise((resolve) => {

                fs.appendFile(path, `\nDate: ${date} ||Time: ${time} ||Weight: ${weight} KGS`, (err) => {
                  if (err) {
                    resolve(false)
                  };
                  resolve(true)
                });

              })


            }
          } else {

            const createFile = await new Promise((resolve) => {

              fs.appendFile(path, ``, (err) => {
                if (err) {
                  resolve(false)
                };
                resolve(true)
              });

            })

          }
        } catch (e) {
          console.log(e)
        }

      });

    });

  }

};

module.exports = logData;
