const { io } = require("../app")



const fs = require('fs')

const path = require("path");

const logData = require("./logData")

const moment = require("moment")

const streamLogger = logData({io, fs, path, moment})



const services = Object.freeze({
    streamLogger
});

module.exports = services;
module.exports = {
    streamLogger
};