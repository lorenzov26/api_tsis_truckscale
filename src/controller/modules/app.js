const {
  addNewModules,
  updateModuless,
  modulesSelectAlls,
  modulesSelectOnes,
  uc_modulesSelectAllWithActions
} = require("../../use-cases/modules/app");

//####################
const modulesAddNew = require("./modules-add");
const moduleUpdate = require("./modules-update");
const selectAllModules = require("./modules-select-all");
const selectOneModule = require("./modules-select-one");
const selectAllModulesWithActions = require("./modules-select-all-with-actions")

//####################
const modulesAddNews = modulesAddNew({ addNewModules });
const moduleUpdates = moduleUpdate({ updateModuless });
const selectAllModuless = selectAllModules({ modulesSelectAlls });
const selectOneModules = selectOneModule({ modulesSelectOnes });
const c_modulesSelectAllWithActions = selectAllModulesWithActions({uc_modulesSelectAllWithActions})

const services = Object.freeze({
  modulesAddNews,
  moduleUpdates,
  selectAllModuless,
  selectOneModules,
  c_modulesSelectAllWithActions
});

module.exports = services;
module.exports = {
  modulesAddNews,
  moduleUpdates,
  selectAllModuless,
  selectOneModules,
  c_modulesSelectAllWithActions
};
