const {
    uc_itemsSelectAll,
    uc_importItems,
    u_uomSelectAll,
    u_insertUom,
    u_updateUom
  } = require("../../use-cases/items/app");
  
  //####################
  const selectAllItems = require("./items-select-all");
  const importItems = require("./import-items")
  const uomSelectAll = require("./uom-select-all")
  const insertUom = require("./uom-insert")
  const updateUom = require("./uom-update")

  
  
  //####################

  const c_selectAllItems = selectAllItems({uc_itemsSelectAll})
  const c_importItems = importItems ({uc_importItems})
  const c_uomSelectAll = uomSelectAll({u_uomSelectAll})
  const c_insertUom = insertUom({u_insertUom})
  const c_updateUom = updateUom({u_updateUom})

  
  const services = Object.freeze({
    c_selectAllItems,
    c_importItems,
    c_uomSelectAll,
    c_insertUom,
    c_updateUom
  });
  
  module.exports = services;
  module.exports = {
    c_selectAllItems,
    c_importItems,
    c_uomSelectAll,
    c_insertUom,
    c_updateUom
  };
  