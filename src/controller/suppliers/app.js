const {
  addNewSuppliers,
  suppliersSelectAlls,
  suppliersSelectOnes,
  updateSupplierss,
  uc_importSuppliers
} = require("../../use-cases/suppliers/app");

//####################
const supplierAddNew = require("./supplier-add");
const selectAllSupplier = require("./supplier-select-all");
const selectOneSupplier = require("./supplier-select-one");
const suppliersUpdate = require("./supplier-update");
const importSuppliers = require("./import-suppliers")
//####################
const supplierAddNews = supplierAddNew({ addNewSuppliers });
const selectAllSuppliers = selectAllSupplier({ suppliersSelectAlls });
const selectOneSuppliers = selectOneSupplier({ suppliersSelectOnes });
const suppliersUpdates = suppliersUpdate({ updateSupplierss });
const c_importSuppliers = importSuppliers({uc_importSuppliers})

const services = Object.freeze({
  supplierAddNews,
  selectAllSuppliers,
  selectOneSuppliers,
  suppliersUpdates,
  c_importSuppliers
});

module.exports = services;
module.exports = {
  supplierAddNews,
  selectAllSuppliers,
  selectOneSuppliers,
  suppliersUpdates,
  c_importSuppliers
};
