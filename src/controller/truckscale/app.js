const {
  addNewTruckScales,
  tsSelectAlls,
  tsSelectOnes,
  updateTruckScaless
} = require("../../use-cases/truckscale/app");

// ##########################
const truckScaleAddNew = require("./ts-add");
const selectAllTs = require("./ts-select-all");
const selectOneTs = require("./ts-select-one");
const truckScaleUpdate = require("./ts-update");
// ##########################
const truckScaleAddNews = truckScaleAddNew({ addNewTruckScales });
const selectAllTss = selectAllTs({ tsSelectAlls });
const selectOneTss = selectOneTs({ tsSelectOnes });
const truckScaleUpdates = truckScaleUpdate({ updateTruckScaless });

const services = Object.freeze({
  truckScaleAddNews,
  selectAllTss,
  selectOneTss,
  truckScaleUpdates
});

module.exports = services;
module.exports = {
  truckScaleAddNews,
  selectAllTss,
  selectOneTss,
  truckScaleUpdates
};
