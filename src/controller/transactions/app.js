const {
  addNewTransactions,
  transactionSelectAlls,
  uc_addInbound,
  uc_addOutbound,
  fetchPurchaseOrders,
  forTransmittalNotifs,
  uc_selectAllWarehouses,
  uc_scanTransaction,
  fsqrGrpos,
  updateTransactions,
  u_editTransaction,
  u_printTransaction,
  u_selectPrintCount,
  u_getLocations,
  u_cancelTransaction
} = require("../../use-cases/transactions/app");

//####################
const transactionAddNew = require("./transactions-add");
const selectAllTransactions = require("./transactions-select-all");
const addInbound = require("./transactions-inbound-add");
const addOutbound = require("./transactions-outbound-add");
const purchaseOrderFetch = require("./transaction-fetch-SAP");
const notifForTransmittal = require("./notification-for-transmittal");
const selectAllWarehouses = require("./select-all-warehouses");
const scanTransaction = require("./transactions-scan");
const grpoFsqr = require("./transactions-fsqr-grpo");
const transactionUpdate = require("./transactions-update");
const editTransaction = require("./transactions-edit")
const printTransaction = require("./transactions-print") 
const selectPrintCount = require("./transactions-select-print-count")
const getLocations = require("./get-locations")
const cancelTransaction = require("./transactions-cancel")
//####################
const c_getLocations = getLocations({u_getLocations})
const transactionAddNews = transactionAddNew({ addNewTransactions });
const selectAllTransactionss = selectAllTransactions({ transactionSelectAlls });
const c_addInbound = addInbound({ uc_addInbound });
const c_addOutbound = addOutbound({ uc_addOutbound });
const purchaseOrderFetchs = purchaseOrderFetch({ fetchPurchaseOrders });
const notifForTransmittals = notifForTransmittal({ forTransmittalNotifs });
const c_selectAllWarehouses = selectAllWarehouses({ uc_selectAllWarehouses });
const c_scanTransaction = scanTransaction({ uc_scanTransaction });
const grpoFsqrs = grpoFsqr({ fsqrGrpos });
const transactionUpdates = transactionUpdate({ updateTransactions });
const c_editTransaction = editTransaction({ u_editTransaction })
const c_printTransaction = printTransaction({ u_printTransaction })
const c_selectPrintCount = selectPrintCount({u_selectPrintCount})
const c_cancelTransaction = cancelTransaction({u_cancelTransaction})

//####################
const services = Object.freeze({
  transactionAddNews,
  selectAllTransactionss,
  c_addInbound,
  c_addOutbound,
  purchaseOrderFetchs,
  notifForTransmittals,
  c_selectAllWarehouses,
  c_scanTransaction,
  grpoFsqrs,
  transactionUpdates,
  c_editTransaction,
  c_printTransaction,
  c_selectPrintCount,
  c_getLocations,
  c_cancelTransaction
});

module.exports = services;
module.exports = {
  transactionAddNews,
  selectAllTransactionss,
  c_addInbound,
  c_addOutbound,
  purchaseOrderFetchs,
  notifForTransmittals,
  c_selectAllWarehouses,
  c_scanTransaction,
  grpoFsqrs,
  transactionUpdates,
  c_editTransaction,
  c_printTransaction,
  c_selectPrintCount,
  c_getLocations,
  c_cancelTransaction
};
