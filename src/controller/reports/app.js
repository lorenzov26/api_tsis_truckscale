const {
  completedTransactionSelectAlls,
  u_editReport
} = require("../../use-cases/reports/app");

//####################
const selectAllCompletedTransaction = require("./select-all-completed-transactions");
const editReport = require("./edit-report")
//####################
const selectAllCompletedTransactions = selectAllCompletedTransaction({
  completedTransactionSelectAlls
});
const c_editReport = editReport({ u_editReport })

const services = Object.freeze({ selectAllCompletedTransactions, c_editReport });

module.exports = services;
module.exports = { selectAllCompletedTransactions, c_editReport };
