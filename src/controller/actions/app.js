const {
  addNewActions,
  updateActionss,
  actionsSelectAlls,
  actionsSelectOnes
} = require("../../use-cases/actions/app");

//####################
const actionsAddNew = require("./actions-add");
const actionsUpdate = require("./actions-update");
const selectAllActions = require("./actions-select-all");
const selectOneAction = require("./actions-select-one");

//####################
const actionsAddNews = actionsAddNew({ addNewActions });
const actionsUpdates = actionsUpdate({ updateActionss });
const selectAllActionss = selectAllActions({ actionsSelectAlls });
const selectOneActions = selectOneAction({ actionsSelectOnes });

const services = Object.freeze({
  actionsAddNews,
  actionsUpdates,
  selectAllActionss,
  selectOneActions
});

module.exports = services;
module.exports = {
  actionsAddNews,
  actionsUpdates,
  selectAllActionss,
  selectOneActions
};
