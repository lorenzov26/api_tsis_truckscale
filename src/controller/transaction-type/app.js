const {
  addNewTransactionTypes,
  transactionTypeSelectAlls,
  transactionTypeSelectOnes,
  updateTransactionTypes,
  u_selectAllSubTypes
} = require("../../use-cases/transaction-type/app");

//####################
const transactionTypeAddNew = require("./transaction-type-add");
const selectAllTransactionType = require("./transaction-type-select-all");
const selectOneTransactionType = require("./transaction-type-select-one");
const transactionTypeUpdate = require("./transaction-type-update");
const selectAllSubTypes = require("./select-all-sub-types")
//####################
const transactionTypeAddNews = transactionTypeAddNew({
  addNewTransactionTypes
});
const selectAllTransactionTypes = selectAllTransactionType({
  transactionTypeSelectAlls
});
const selectOneTransactionTypes = selectOneTransactionType({
  transactionTypeSelectOnes
});
const transactionTypeUpdates = transactionTypeUpdate({
  updateTransactionTypes
});

const c_selectAllSubTypes = selectAllSubTypes({u_selectAllSubTypes})

const services = Object.freeze({
  transactionTypeAddNews,
  selectAllTransactionTypes,
  selectOneTransactionTypes,
  transactionTypeUpdates,
  c_selectAllSubTypes
});

module.exports = services;
module.exports = {
  transactionTypeAddNews,
  selectAllTransactionTypes,
  selectOneTransactionTypes,
  transactionTypeUpdates,
  c_selectAllSubTypes
};
