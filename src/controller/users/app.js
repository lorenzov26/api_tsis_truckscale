const {
  addNewUsers,
  usersSelectAlls,
  usersSelectOnes,
  loginUsers,
  uc_updateUser,
  resetPasswords,
  selectActivityLogss,
  uc_employeesInfoSelectAll,
  loginSapUsers,
  uc_checkPin,
  resetCodeAndPws,
  uc_importUsers,
  u_selectAllPrintLogs,
  u_updatePrintSettings
} = require("../../use-cases/users/app");

// ##########################

const usersAddNew = require("./users-add");
const selectAllUsers = require("./users-select-all");
const selectOneUser = require("./users-select-one");
const usersLogin = require("./users-login");
const updateUser = require("./users-update");
const passwordReset = require("./users-reset-password");
const selectAllActivityLog = require("./activity-logs-select");
const employeesInfoSelectAll = require("./employeesInfo-select-all");
const sapUsersLogin = require("./users-sap-login");
const checkPin = require("./check-pin");
const codeAndPwReset = require("./reset-password-code");
const importUsers = require("./import-users")
const selectAllPrintLogs = require("./print-logs-select")
const updatePrintSettings = require("./print-settings-update")

// ##########################
const c_selectAllPrintLogs = selectAllPrintLogs({ u_selectAllPrintLogs })

const c_updatePrintSettings = updatePrintSettings({u_updatePrintSettings})

const c_checkPin = checkPin({ uc_checkPin });
const usersAddNews = usersAddNew({ addNewUsers });
const selectAllUserss = selectAllUsers({ usersSelectAlls });
const selectOneUsers = selectOneUser({ usersSelectOnes });
const usersLogins = usersLogin({ loginUsers });
const c_updateUser = updateUser({ uc_updateUser });
const passwordResets = passwordReset({ resetPasswords });
const selectAllActivityLogs = selectAllActivityLog({ selectActivityLogss });
const c_importUsers = importUsers({uc_importUsers})

const c_employeesInfoSelectAll = employeesInfoSelectAll({
  uc_employeesInfoSelectAll
});
const sapUsersLogins = sapUsersLogin({ loginSapUsers });
const codeAndPwResets = codeAndPwReset({ resetCodeAndPws });

// ##############
const services = Object.freeze({
  usersAddNews,
  selectAllUserss,
  selectOneUsers,
  usersLogins,
  c_updateUser,
  passwordResets,
  selectAllActivityLogs,
  c_employeesInfoSelectAll,
  sapUsersLogins,
  c_checkPin,
  codeAndPwResets,
  c_importUsers,
  c_selectAllPrintLogs,
  c_updatePrintSettings
});

module.exports = services;
module.exports = {
  usersAddNews,
  selectAllUserss,
  selectOneUsers,
  usersLogins,
  c_updateUser,
  passwordResets,
  selectAllActivityLogs,
  c_employeesInfoSelectAll,
  sapUsersLogins,
  c_checkPin,
  codeAndPwResets,
  c_importUsers,
  c_selectAllPrintLogs,
  c_updatePrintSettings
};
