const portSelectAll = ({ listAllPortss }) => {
  return async function selects(httpRequest) {
    const headers = {
      "Content-Type": "application/json"
    };
    try {
      const views = await listAllPortss({});
      return {
        headers,
        statusCode: 200,
        body: views
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);
      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message
        }
      };
    }
  };
};

module.exports = portSelectAll;
