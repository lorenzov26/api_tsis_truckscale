
const { u_addRequest, u_selectAllRequests, u_approveRequest, u_disapproveRequest, u_validateRequest,
u_cancelRequest } = require("../../use-cases/requests/app")



//####################
const addRequest = require("./add-request")
const selectAllRequests = require("./select-all-requests")
const approveRequest = require("./approve-request")
const disapproveRequest = require("./disapprove-request")
const validateRequest = require("./validate-request")
const cancelRequest = require("./cancel-request")
//####################
const c_addRequest = addRequest({ u_addRequest })
const c_selectAllRequests = selectAllRequests({ u_selectAllRequests })
const c_approveRequest = approveRequest({ u_approveRequest })
const c_disapproveRequest = disapproveRequest({ u_disapproveRequest })
const c_validateRequest = validateRequest({u_validateRequest})
const c_cancelRequest = cancelRequest({u_cancelRequest})
const services = Object.freeze({
    c_addRequest,
    c_selectAllRequests,
    c_approveRequest,
    c_disapproveRequest,
    c_validateRequest,
    c_cancelRequest
});

module.exports = services;
module.exports = {
    c_addRequest,
    c_selectAllRequests,
    c_approveRequest,
    c_disapproveRequest,
    c_validateRequest,
    c_cancelRequest
};
