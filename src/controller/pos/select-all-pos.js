const selectAllPos = ({ uc_selectAllPos }) => {
    return async function get(httpRequest) {
      const headers = {
        "Content-Type": "application/json"
      };
      try {
        
        const { source = {}, ...employeeInfo } = httpRequest.body;
        source.ip = httpRequest.ip;
        source.browser = httpRequest.headers["User-Agent"];
        if (httpRequest.headers["Referer"]) {
          source.referrer = httpRequest.headers["Referer"];
        }
        const toView = {
          ...employeeInfo,
          source
        };
        
        const view = await uc_selectAllPos(toView);
        return {
          headers: {
            "Content-Type": "application/json"
          },
          statusCode: 200,
          body: { view }
        };
      } catch (e) {
        
        return {
          headers,
          statusCode: 400,
          body: {
            error: e.message
          }
        };
      }
    };
  };
  
  module.exports = selectAllPos;
  