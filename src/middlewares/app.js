// for image download
const multer = require("multer");
const uniqueString = require("unique-string");
const fs = require("fs");
const path = require("path");
// const sharp = require("sharp");
const resizeImg = require('resize-img');

// #############################

const uploadFiles = require("./file-uploader");
const fsqrValidate = require("./fsqr-credentials");
const resizeImage = require("./image-resizer")
// #############################
const uploadFiless = uploadFiles({ multer, path, uniqueString, fs });
const fsqrValidates = fsqrValidate({});
const m_resizeImage = resizeImage({ uniqueString, resizeImg, fs })

const services = Object.freeze({
  uploadFiless,
  fsqrValidates,
  m_resizeImage
});

module.exports = services;
module.exports = {
  uploadFiless,
  fsqrValidates,
  m_resizeImage
};
