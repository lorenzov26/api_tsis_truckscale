const fsqrValidate = ({}) => {
  return async function fsqr(req, res, next) {
    const info = req.body;

    if (!info.user) {
      return res.sendStatus(403);
    }

    if (!info.pw) {
      return res.sendStatus(403);
    }

    if (info.user == "fsqr" && info.pw == "superpw64") {
      next();
    } else {
      return res.sendStatus(403);
    }
  };
};

module.exports = fsqrValidate;
