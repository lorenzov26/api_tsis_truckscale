const modulesSelectAllWithActions = ({
  modulesDb,
  modules,
  validateAccessRights
}) => {
  return async function selects(info) {
    const { mode } = info;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    // const allowed = await validateAccessRights(
    //   info.modules,
    //   "admin",
    //   "view modules"
    // );

    // if (!allowed) {
    //   throw new Error(`Access denied`);
    // }

    const result = await modulesDb.selectAllModulesWithActions();

    let modules = [];

    let actions = [];

    for (let i = 0; i < result.rows.length; i++) {
      modules.push({
        moduleid: result.rows[i].module_id,
        moduledescription: result.rows[i].module_name,
        modulestatus: result.rows[i].module_status
      });

      actions.push({
        actionid: result.rows[i].action_id,
        actiondescription: result.rows[i].action_name,
        actionstatus: result.rows[i].action_status,
        actionmoduleid: result.rows[i].module_id
      });
    }

    const filteredModules = await filterArrayNoDuplicates(
      modules,
      "moduleid"
    );

    const filteredActions = await filterArrayNoDuplicates(
      actions,
      "actionid"
    );

    for (let i = 0; i < filteredModules.length; i++) {
      let temp = [];

      for (let i2 = 0; i2 < filteredActions.length; i2++) {
        if (
          filteredModules[i].moduleid === filteredActions[i2].actionmoduleid
        ) {
          temp.push(filteredActions[i2]);
        }
      }

      filteredModules[i].actions = temp;
    }

    return filteredModules;

  };
};

const filterArrayNoDuplicates = (arr, comp) => {
  const unique = arr
    .map(e => e[comp])

    .map((e, i, final) => final.indexOf(e) === i && i)

    .filter(e => arr[e])
    .map(e => arr[e]);

  return unique;
};

module.exports = modulesSelectAllWithActions;
