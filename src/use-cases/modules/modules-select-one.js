const modulesSelectOne = ({ modulesDb, decrypt, modules, validateAccessRights }) => {
  return async function selects(info) {
    const { id, mode } = info;

    
    if(!info.modules){
      throw new Error (`Access denied`)
    }

    const allowed = await validateAccessRights(info.modules,'admin','view modules')


    if(!allowed){
      throw new Error (`Access denied`)
    }

    delete info.mode; // remove mode from req body

    if (mode == 1) {
      
      delete info.modules
      const res = await modules.modulesSelectOne({ info });
      return res.data;
    } else {
      const result = await modulesDb.selectSingleModule({ id });
      const modules = result.rows;

      let data = []; //declare empty array

      for await (let i of modules) {
        // created by
        const created_by = [
          {
            firstname: i.create_fn ? decrypt(i.create_fn) : "",
            middlename: i.create_mn ? decrypt(i.create_mn) : "",
            lastname: i.create_ln ? decrypt(i.create_ln) : "",
            created_at: i.created_at
          }
        ];

        // modified by
        const modified_by = [
          {
            firstname: i.modify_fn ? decrypt(i.modify_fn) : "",
            middlename: i.modify_mn ? decrypt(i.modify_mn) : "",
            lastname: i.modify_ln ? decrypt(i.modify_ln) : "",
            updated_at: i.updated_at
          }
        ];

        data.push({
          id: i.module_id,
          description: i.descriptions,
          status: i.status,
          created_by,
          modified_by
        });
      }

      return data;
    }
  };
};

module.exports = modulesSelectOne;
