const updateModules = ({
  modulesDb,
  makeModuleUpdate,
  insertActivityLogss,
  decrypt,
  modules,
  objectLowerCaser,
  validateAccessRights
}) => {
  return async function put({ id, ...info } = {}) {
    const { mode } = info;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    const allowed = await validateAccessRights(
      info.modules,
      "admin",
      "edit module"
    );

    if (!allowed) {
      throw new Error(`Access denied`);
    }

    if (mode == 1) {
      delete info.modules;
      delete info.mode; // remove mode from req body
      delete info.source;
      info.id = id; //add id to object

      await objectLowerCaser(info);

      val({ info });

      // select if module name exist
      const check = await modules.modulesUpdateSelectByName({ info });
      if (check.status == 401) {
        // session expired
        throw new Error("Session expired, please login again.");
      }
      // number of data
      const length = check.length;
      // if exist
      if (length > 0) {
        throw new Error(`Module name already exists.`);
      }

      // update
      const res = await modules.modulesUpdate({ info });
      return res;
    } else {
      const result = makeModuleUpdate(info);

      const moduleExist = await modulesDb.selectModuleUpdate({
        description: result.getDesc(),
        id
      });

      if (moduleExist.rowCount !== 0) {
        throw new Error("The module name already exist.");
      }

      // query previous values
      const previous = await modulesDb.selectSingleModule({ id });
      const prev_data = previous.rows[0];
      const prev_values = {
        module_id: prev_data.module_id,
        descriptions: prev_data.descriptions,
        status: prev_data.status,
        created_by: {
          create_fn: prev_data.create_fn ? decrypt(prev_data.create_fn) : "",
          create_mn: prev_data.create_mn ? decrypt(prev_data.create_mn) : "",
          create_ln: prev_data.create_ln ? decrypt(prev_data.create_ln) : "",
          created_at: prev_data.created_at
        },
        modified_by: {
          modify_fn: prev_data.modify_fn ? decrypt(prev_data.modify_fn) : "",
          modify_mn: prev_data.modify_mn ? decrypt(prev_data.modify_mn) : "",
          modify_ln: prev_data.modify_ln ? decrypt(prev_data.modify_ln) : "",
          updated_at: prev_data.updated_at
        }
      };

      // update to db
      const insert = await modulesDb.updateModule({
        description: result.getDesc(),
        status: result.getStatus(),
        modified_by: result.getModifiedBy(),
        updated_at: result.getUpdatedAt(),
        id
      });

      const count = insert.rowCount; // get the number of inserted data

      const data = {
        msg: `Updated successfully ${count} module.`
      };

      // logs
      const user = await modulesDb.returnCreatedBy({
        id: result.getModifiedBy()
      });
      const create = user.rows[0];
      const modified_by = {
        id: create.id,
        employee_id: create.employee_id ? decrypt(create.employee_id) : "",
        firstname: create.firstname ? decrypt(create.firstname) : "",
        lastname: create.lastname ? decrypt(create.lastname) : ""
      };

      // logs
      // new values
      const new_values = {
        description: result.getDesc(),
        status: result.getStatus(),
        updated_at: result.getUpdatedAt(),
        modified_by
      };

      const logs = {
        action_type: "UPDATE MODULE",
        table_affected: "ts_modules",
        new_values,
        prev_values,
        created_at: null,
        updated_at: new Date().toISOString(),
        users_id: result.getModifiedBy()
      };

      await insertActivityLogss({ logs });

      return data;
    }
  };
};

// SL validation
const val = ({ info }) => {
  const { U_TS_DESC, U_TS_MODIFYBY, U_TS_STATUS } = info;
  if (!U_TS_DESC) {
    const d = {
      msg: "Please enter module name."
    };
    throw new Error(JSON.stringify(d));
  }
  if (!U_TS_MODIFYBY) {
    const d = {
      msg: "Please enter who updated the module."
    };
    throw new Error(JSON.stringify(d));
  }
  if (!U_TS_STATUS) {
    const d = {
      msg: "Please enter status."
    };
    throw new Error(JSON.stringify(d));
  }
};

module.exports = updateModules;
