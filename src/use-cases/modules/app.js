const { makeModule, makeModuleUpdate } = require("../../entities/modules/app"); // entity
const modulesDb = require("../../data-access/db-layer/modules/app"); //db
const { decrypt } = require("../../../crypting/app"); // decrypt
const { insertActivityLogss } = require("../users/app"); // logs
const { modules } = require("../../data-access/sl-layer/modules/app");

const { objectLowerCaser } = require("../../lowerCaser/app");

//#######################
const addNewModule = require("./modules-add");
const updateModules = require("./modules-update");
const modulesSelectAll = require("./modules-select-all");
const modulesSelectOne = require("./modules-select-one");
const modulesSelectAllWithActions = require("./modules-select-all-with-actions");

const { validateAccessRights } = require("../../validator/app")
//#######################
const addNewModules = addNewModule({
  modulesDb,
  makeModule,
  insertActivityLogss,
  decrypt,
  modules,
  objectLowerCaser,
  validateAccessRights
});
const updateModuless = updateModules({
  modulesDb,
  makeModuleUpdate,
  insertActivityLogss,
  decrypt,
  modules,
  objectLowerCaser,
  validateAccessRights
});
const modulesSelectAlls = modulesSelectAll({ modulesDb, decrypt, modules, validateAccessRights });
const modulesSelectOnes = modulesSelectOne({ modulesDb, decrypt, modules, validateAccessRights });
const uc_modulesSelectAllWithActions = modulesSelectAllWithActions({
  modulesDb,
  modules, validateAccessRights
});

const services = Object.freeze({
  addNewModules,
  updateModuless,
  modulesSelectAlls,
  modulesSelectOnes,
  uc_modulesSelectAllWithActions
});

module.exports = services;
module.exports = {
  addNewModules,
  updateModuless,
  modulesSelectAlls,
  modulesSelectOnes,
  uc_modulesSelectAllWithActions
};
