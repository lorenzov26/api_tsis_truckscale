const addNewTruckScale = ({
  truckScaleDb,
  makeTruckScale,
  insertActivityLogss,
  truckscale,
  objectLowerCaser,
  returnIpAddress,
  hostName
}) => {
  return async function posts(info) {
    const mode = info.mode;

    if (mode == 1) {
      delete info.source;
      delete info.mode;

      info.U_TS_TR_IP = returnIpAddress();
      info.U_TS_DEVICE = hostName();

      info = await objectLowerCaser(info);

      val({ info });

      const max = await truckscale.truckscaleGetMaxCode({});
      const maxCode = max[0].maxCode;

      info.Code = maxCode;
      info.Name = maxCode;

      const check = await truckscale.truckscaleAddSelectByIPAndDeviceName({
        info
      });

      const length = check.data.length;

      if (length > 0) {
        throw new Error(`Device already exists.`);
      }

      const res = await truckscale.truckscaleAdd({ info });

      const reqStatus = res.status;
      if (reqStatus == 201) {
        return res;
      } else {
        // throw error from SL
        throw new Error(res.data.error);
      }
    } else {

      if (!info.truckscale_id) {
        throw new Error(`Please input truckscale ID`)
      }

      if (!info.truckscale_name) {
        throw new Error(`Please input truckscale name`)
      }

      if (!info.truckscale_location) {
        throw new Error(`Please input truckscale location`)
      }

      if (!info.truckscale_length) {
        throw new Error(`Please input truckscale length`)
      }

      if (!info.baudrate) {
        throw new Error(`Please input baudrate`)
      }
  

      const ip = returnIpAddress()
      const host = hostName()

      let configureTruckscale

      // const truckscales = await truckScaleDb.selectAllTruckscales({ info })

      // if (truckscales.rowCount == 0) {

      //   configureTruckscale = await truckScaleDb.addTruckscale({
      //     info: {
      //       truckscale_id: info.truckscale_id,
      //       truckscale_name: info.truckscale_name,
      //       truckscale_location: info.truckscale_location,
      //       truckscale_length: info.truckscale_length,
      //       truckscale_ip: ip,
      //       truckscale_device: host,
      //       truckscale_baudrate: info.baudrate,
      //       truckscale_parity: info.parity,
      //       truckscale_databits: info.databits,
      //       truckscale_stopbits: info.stopbits
      //     }
      //   })

      // } else {

      //   configureTruckscale = await truckScaleDb.updateTruckscale({
      //     info: {
      //       id: truckscales.rows[0].id,
      //       truckscale_id: info.truckscale_id,
      //       truckscale_name: info.truckscale_name,
      //       truckscale_location: info.truckscale_location,
      //       truckscale_length: info.truckscale_length,
      //       truckscale_ip: ip,
      //       truckscale_device: host,
      //       truckscale_baudrate: info.baudrate,
      //       truckscale_parity: info.parity,
      //       truckscale_databits: info.databits,
      //       truckscale_stopbits: info.stopbits
      //     }
      //   })

      // }


      configureTruckscale = await truckScaleDb.addTruckscale({
        info: {
          truckscale_id: info.truckscale_id,
          truckscale_name: info.truckscale_name,
          truckscale_location: info.truckscale_location,
          truckscale_length: info.truckscale_length,
          truckscale_ip: ip,
          truckscale_device: host,
          truckscale_baudrate: info.baudrate,
          truckscale_parity: info.parity,
          truckscale_databits: info.databits,
          truckscale_stopbits: info.stopbits
        }
      })

      if (configureTruckscale.rowCount == 0) {
        throw new Error(`Truckscale configuration failed`)
      }

      const data = {
        msg: `Truckscale configured successfully`
      };

      return data
    }
  };
};

const val = ({ info }) => {
  const {
    U_TS_TRSCALE,
    U_TS_TR_LOCATION,
    U_TS_TR_LENGTH,
    U_TS_BAUDRATE
  } = info;

  if (!U_TS_TRSCALE) {
    const d = {
      msg: "Please enter truckscale name."
    };
    throw new Error(JSON.stringify(d));
  }

  if (!U_TS_TR_LOCATION) {
    const d = {
      msg: "Please enter truckscale location."
    };
    throw new Error(JSON.stringify(d));
  }
  if (!U_TS_TR_LENGTH) {
    const d = {
      msg: "Please enter truckscale length."
    };
    throw new Error(JSON.stringify(d));
  }

  if (!U_TS_BAUDRATE) {
    const d = {
      msg: "Please enter baudrate."
    };
    throw new Error(JSON.stringify(d));
  }
};

module.exports = addNewTruckScale;
