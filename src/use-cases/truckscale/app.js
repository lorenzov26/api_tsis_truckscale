const os = require("os");
const address = require("address");
const isIp = require("is-ip");


const returnIpAddress = () => {
  return address.ip();
};

// get host name of the device
const hostName = () => {
  return os.hostname();
};

// check if valid ip address
const checkIp = ip => {
  return isIp.v4(ip);
};
// only numbers allowed
const allNumbers = text => {
  const isnum = /^\d*(\.\d+)?$/.test(text);
  return isnum;
};

const {
  makeTruckScale,
  makeTruckScaleUpdate
} = require("../../entities/truckscale/app"); // entity
const truckScaleDb = require("../../data-access/db-layer/truckscale/app"); //db




const { insertActivityLogss } = require("../users/app");

const { truckscale } = require("../../data-access/sl-layer/truckscale/app");

const { objectLowerCaser } = require("../../lowerCaser/app"); 

//#######################
const addNewTruckScale = require("./ts-add");
const tsSelectAll = require("./ts-select-all");
const tsSelectOne = require("./ts-select-one");
const updateTruckScales = require("./ts-update");
//#######################
const addNewTruckScales = addNewTruckScale({
  truckScaleDb,
  makeTruckScale,
  insertActivityLogss,
  truckscale,
  objectLowerCaser,
  returnIpAddress, 
  hostName
});
const tsSelectAlls = tsSelectAll({ truckScaleDb, truckscale });
const tsSelectOnes = tsSelectOne({ truckScaleDb, truckscale });
const updateTruckScaless = updateTruckScales({
  truckScaleDb,
  makeTruckScaleUpdate,
  insertActivityLogss,
  truckscale,
  objectLowerCaser,
});

const services = Object.freeze({
  addNewTruckScales,
  tsSelectAlls,
  tsSelectOnes,
  updateTruckScaless
});

module.exports = services;
module.exports = {
  addNewTruckScales,
  tsSelectAlls,
  tsSelectOnes,
  updateTruckScaless
};
