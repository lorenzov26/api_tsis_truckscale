const transactionSelectAll = ({
  transactionDb,
  decrypt,
  transactions,
  transactionTypes,
  drivers,
  truckInfos,
  weightTypes,
  validateAccessRights,
  users
}) => {
  return async function selects(info) {
    const mode = info.mode;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    // const allowed = await validateAccessRights(
    //   info.modules,
    //   "transaction",
    //   "view transactions"
    // );

    // if (!allowed) {
    //   throw new Error(`Access denied`);
    // }
 
      if (typeof info.id !== "undefined") {


        const transactionDetails = await transactionDb.selectOneTransaction({info})

        const view = transactionDetails.rows[0]

        return view

      }else {

      const transactions = await transactionDb.selectAllTransactions({info})

      const view = transactions.rows

      

      return view
      

    }
  };
};

module.exports = transactionSelectAll;
