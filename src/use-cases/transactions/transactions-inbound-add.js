const addInbound = ({
  transactionDb,
  weightDb,
  e_addInbound,
  e_addOutbound,
  insertActivityLogss,
  transactions,
  transactionTypes,
  weightTypes,
  validateAccessRights,
  moment
}) => {
  return async function post(info) {
    // check if there is transaction id; doesn't pass thru entity

    const mode = info.mode

    // if (!info.modules) {
    //   throw new Error(`Access denied`)
    // }

    // const allowed = await validateAccessRights(info.modules, 'transaction', 'edit transaction')


    // if (!allowed) {
    //   throw new Error(`Access denied`)
    // }

    if (!info.ib_weight) {
      throw new Error(`Please input inbound weight`)
    }

    const d = new Date();
    info.dateToday = moment(d).format("YYYY-MM-DD HH:mm:ss");

    const updateInbound = await transactionDb.updateInboundTransaction({ info })

    if (updateInbound.rowCount == 0) {
      throw new Error(`Adding weight failed`)
    }

    const updateRemarks = await transactionDb.updateTransactionRemarks({
      info: {
        id: info.transaction_id,
        remarks: info.remarks
      }
    })

    const data = {
      msg: `Inbound transaction successfull`
    };


    return data


  };
};

module.exports = addInbound;
