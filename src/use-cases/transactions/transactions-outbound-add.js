const addOutbound = ({
  transactionDb,
  weightDb,
  e_addOutbound,
  insertActivityLogss,
  transactions,
  transactionTypes,
  weightTypes,
  validateAccessRights,
  moment
}) => {
  return async function post(info) {
    const mode = info.mode;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    // const allowed = await validateAccessRights(
    //   info.modules,
    //   "transaction",
    //   "edit transaction"
    // );

    // if (!allowed) {
    //   throw new Error(`Access denied`);
    // }


    if (!info.ob_weight) {
      throw new Error(`Please input outbound weight`)
    }

    if (!info.sign) {
      throw new Error(`Please sign`)
    }

    const d = new Date();
    info.dateToday = moment(d).format("YYYY-MM-DD HH:mm:ss");

    info.status = 'completed'

    const updateOutbound = await transactionDb.updateOutboundTransaction({ info })

    if (updateOutbound.rowCount == 0) {
      throw new Error(`Adding weight failed`)
    }

    const updateRemarks = await transactionDb.updateTransactionRemarks({
      info: {
        id: info.transaction_id,
        remarks: info.remarks
      }
    })

    const updateTransactionStatus = await transactionDb.updateTransactionStatus({ info })

    const data = {
      msg: `Outbound transaction successfull`
    };


    return data


  };
};

module.exports = addOutbound;
