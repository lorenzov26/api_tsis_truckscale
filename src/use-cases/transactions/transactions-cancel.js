const cancelTransaction = ({
    transactionDb, moment
}) => {
    return async function put({ id, ...info } = {}) {

        if (!info.modules) {
            throw new Error(`Access denied`);
        }

        const d = new Date();
        info.dateToday = moment(d).format("YYYY-MM-DD HH:mm:ss");

        info.status = 'cancelled'

        const updateStatus = await transactionDb.updateTransactionStatus({info: {
            status: info.status,
            mainTransactionId: info.transaction_id,
            created_by: info.updated_by,
            dateToday: info.dateToday
        }})

        if(updateStatus.rowCount == 0){
            throw new Error(`Cancel transaction failed`)
        }

        return true

    };
};


module.exports = cancelTransaction;
