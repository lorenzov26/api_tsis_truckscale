const forTransmittalNotif = ({ transactionDb, io }) => {
  return async function selects() {
    const result = await transactionDb.forTransmittalNotif({});
    const count = result.rows[0].count;
    
    io.sockets.emit("for-transmittal", {
      count
    });

    const data = { msg: `Emit notif` };
    return data;
  };
};

module.exports = forTransmittalNotif;
