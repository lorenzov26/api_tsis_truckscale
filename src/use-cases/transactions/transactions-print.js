const dotenv = require("dotenv");
dotenv.config();

const printTransaction = ({
  transactionTypeDb,
  transactionDb,
  moment,
  escpos,
  path,
  fs
}) => {
  return async function post(info) {
    const mode = info.mode;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    try {

      const transactionTypeDetails = await transactionTypeDb.selectOneTransactionTypeByName({
        info: {
          t_type: info.transaction_type.toLowerCase()
        }
      })

      const transactionType = transactionTypeDetails.rows[0].name

      const transactionTypeCount = transactionTypeDetails.rows[0].print_count

      const printLogsDetails = await transactionDb.selectLogsByTransactionId({
        info: {
          transaction_id: info.ticket_number,
        }
      })

      const printedDocuments = printLogsDetails.rowCount

      if (info.print_count <= printedDocuments) {
        throw new Error(`Maximum amount of printing is used`)
      }

      const d = new Date();
      info.dateToday = moment(d).format("YYYY-MM-DD HH:mm:ss");

      const device = new escpos.USB(process.env.printParams1, process.env.printParams2);

      const options = { encoding: "GB18030" /* default */ }

      const printer = new escpos.Printer(device, options);

      let headerImage, printCount = '', location = ''

      location = info.location

      const biotechLogo = await new Promise((resolve) => {
        escpos.Image.load(`biotech.png`, function (image) {
          resolve(image)
        });
      });

      const unahcoLogo = await new Promise((resolve) => {
        escpos.Image.load(`unahco.png`, function (image) {
          resolve(image)
        });
      });

      const whiteBackground = await new Promise((resolve) => {
        escpos.Image.load(`white.png`, function (image) {
          resolve(image)
        });
      });

      const fileExists = await new Promise((resolve) => {
        fs.access(info.signature.substring(1), fs.F_OK, (err) => {
          if (err) {
            resolve(false)
          }
          resolve(true)
        })
      })

      let signature = whiteBackground


      if (fileExists) {
        signature = await new Promise((resolve) => {
          escpos.Image.load(`http://${process.env.HOST}:301/${info.signature.substring(1)}`, function (image) {
            resolve(image)
          });
        });
      }

      if (transactionType == 'unahco') {
        headerImage = unahcoLogo
      } else {
        headerImage = biotechLogo
      }

      if (printedDocuments == 0) {
        printCount = '1'
      } else {
        printCount = 'RP# ' + printedDocuments
      }


      if (transactionType == 'biotech farms' && info.location == '') {

        const envLocations = process.env.PRINTLOCATIONS.split(",")

        if (printedDocuments < envLocations.length) {

          const matches = envLocations[printedDocuments].match(/\b(\w)/g)
          const acronym = matches.join('')
          location = acronym


        } else {
          location = ''
        }

      }

      //Printing
      device.open(function () {
        printer
          .align('ct')
          .hoiImage(whiteBackground)
          .hoiImage(headerImage)
          .size(2, 2)
          .text('Weight Slip')
          .size(1, 1)
          .text('--------------------------------')
          .text(`${location}                        ${printCount}`)
          .text('')
          .text(`${process.env.TRUCKSCALEADDRESS}`)
          .text('')
          .align('lt')
          .text(`Ticket Number: ${info.ticket_number}`)
          .text('')
          .text(`Customer: ${info.customer}`)
          .text('')
          .text(`Address: ${info.address}`)
          .text('')
          .text(`Commodity: ${info.commodity}`)
          .text('')
          .text(`Plate Number: ${info.plate_number}`)
          .text('')
          .text(`Driver: ${info.driver}`)
          .text('')
          .text(`Quantity: ${info.number_of_bags}`)
          .text('')
          .text(` Inbound Wt kg: ${info.inbound_weight}`)
          .text('')
          .text(`  Date: ${info.inbound_date}`)
          .text('')
          .text(` Outbound Wt kg: ${info.outbound_weight}`)
          .text('')
          .text(`  Date: ${info.outbound_date}`)
          .text('')
          .text(` Net Weight: ${info.net_weight}`)
          .text('')
          .text('Remarks: ')
          .text('')
          .text(`${info.remarks ? info.remarks : ''}`)
          .align('ct')
          .hoiImage(signature)
          .text(`Weigher: ${info.weigher}`)
          .text('')
          .text('')
          .text('')
          .text('')
          .cut()
        // .close()
      });

      const insertLog = await transactionDb.insertPrintLog({
        info: {
          employee_id: info.printed_by,
          transaction_id: info.ticket_number,
          date: info.dateToday
        }
      })


      return true

    } catch (e) {
      throw new Error(e)
    }


  };
};

module.exports = printTransaction;
