const fsqrGrpo = ({ transactions }) => {
  return async function selects(info) {
    const transaction = await transactions.transactionsSelectOneForGrpo({
      info,
    });
    const t = transaction;
    let array = [];
    for (let i = 0; i < t.length; i++) {
      if (i < 1) {
        let weights = [];
        weights.push({
          ibWeight: t[i].IB_WEIGHT,
          ibWeigher: t[i].IB_WEIGHER,
          obWeight: t[i].OB_WEIGHT,
          obWeigher: t[i].OB_WEIGHER,
          netWeight: parseFloat((t[i].IB_WEIGHT - t[i].OB_WEIGHT).toFixed(3)),
        });
        array.push({
          id: t[i].DocEntry,
          trackingCode: t[i].U_TS_TRCK_CODE,
          plate_number: t[i].U_TS_PLATE_NUM,
          truckModel: t[i].U_TS_TRMODEL,
          driver_name: t[i].DRIVER_NAME,
          num_bags: t[i].U_TS_NUM_BAGS,
          items: t[i].ITEM_LIST,
          weights,
        });
      } else {
        array[0].weights.push({
          ibWeight: t[i].IB_WEIGHT,
          ibWeigher: t[i].IB_WEIGHER,
          obWeight: t[i].OB_WEIGHT,
          obWeigher: t[i].OB_WEIGHER,
          netWeight: parseFloat((t[i].IB_WEIGHT - t[i].OB_WEIGHT).toFixed(3)),
        });
      }
    }

    return array;
  };
};

module.exports = fsqrGrpo;
