const addNewTransaction = ({
  transactionDb,
  makeTransaction,
  validateAccessRights,
  driversDb,
  encrypt,
  weightDb,
  insertActivityLogss,
  forTransmittalNotifs,
  posDb,
  transactions,
  transactionTypes,
  weightTypes,
  transactionTypeDb,
  moment,
  suppliersDb,
  itemsDb,
  truckInfosDb
}) => {
  return async function posts(info) {
    const mode = info.mode

    if (!info.modules) {
      throw new Error(`Access denied`)
    }

    // const allowed = await validateAccessRights(info.modules, 'transaction', 'add transaction')


    // if (!allowed) {
    //   throw new Error(`Access denied`)
    // }


    if (!info.t_type) {
      throw new Error(`Please input transaction type`)
    }

    if (!info.truck_info_id) {
      throw new Error(`Please input truck`)
    }

    if (!info.driver_id) {
      throw new Error(`Please input driver`)
    }

    if (!info.supplier) {
      throw new Error(`Please input supplier`)
    }

    if (!info.supplier_address) {
      throw new Error(`Please input supplier address`)
    }



    if (info.t_type === 'biotech farms' || info.t_type === 'unahco') {
      if (!info.transaction_sub_type) {
        throw new Error('Please input transaction sub type')
      }
    }


    const transactionType = await transactionTypeDb.selectOneTransactionTypeByName({ info })

    info.transactionTypeId = transactionType.rows[0].id

    info.print_count = transactionType.rows[0].print_count

    const d = new Date();
    info.dateToday = moment(d).format("YYYY-MM-DD HH:mm:ss");

    info.status = 'pending'

    const transactionSubTypeDetails = await transactionTypeDb.selectOneTransactionSubType({ info })

    let transactionSubType

    if (transactionSubTypeDetails.rowCount > 0) {

      transactionSubType = transactionSubTypeDetails.rows[0].description

    }

    if (transactionSubType !== 'withdrawal') {

      // if(!info.number_of_bags){
      //   throw new Error(`Please input number of bags`)
      // }

      if (!info.item) {
        throw new Error(`Please input item`)
      }

      if (!info.uom) {
        throw new Error(`Please input unit of measure`)
      }

    }

    if (transactionSubType === 'withdrawal') {
      info.inboundWeightType = 'Tare'
      info.outboundWeightType = 'Gross'
    } else {
      info.inboundWeightType = 'Gross'
      info.outboundWeightType = 'Tare'
    }


    const supplierExist = await suppliersDb.selectOneSupplierByName({ info })

    // if(supplierExist.rowCount === 0){

    //   if(info.address){
    //   const insertSupplier = await suppliersDb.insertSupplierWithAddress({info})
    //   info.supplier = insertSupplier.rows[0].id
    //   }else {
    //     return {newSupplier: true}
    //   }

    // } else {
    //   info.supplier = supplierExist.rows[0].id
    // }

    if (supplierExist.rowCount === 0) {
      const insertSupplier = await suppliersDb.insertSupplier({ info })
      info.supplier = insertSupplier.rows[0].id
    } else {
      info.supplier = supplierExist.rows[0].id
    }

    const itemExist = await itemsDb.selectOneItemByName({ info })

    if (info.item) {

      if (itemExist.rowCount === 0) {
        const insertItem = await itemsDb.insertItem({ info })
        info.item = insertItem.rows[0].id
      } else {
        info.item = itemExist.rows[0].id
      }

    }

    const truckInfoExist = await truckInfosDb.selectTruckInfoByPlateNumber({ info })

    if (truckInfoExist.rowCount === 0) {
      const insertTruckInfo = await truckInfosDb.insertTruckInfosForAutoAdd({ info })
      info.truck_info_id = insertTruckInfo.rows[0].id
    } else {
      info.truck_info_id = truckInfoExist.rows[0].id
    }

    const splitName = info.driver_id.split(", ")

    if (splitName.length !== 2) {
      throw new Error(`Invalid name format, must be 'lastname, firstname'`)
    }

    info.firstname = splitName[1]
    info.lastname = splitName[0]

    const driverExist = await driversDb.selectOneDriverByNameForAdd({ info })

    if (driverExist.rowCount === 0) {
      const insertDriver = await driversDb.insertDriverForAutoAdd({ info })
      info.driver_id = insertDriver.rows[0].id
    } else {
      info.driver_id = driverExist.rows[0].id
    }

    const insertTransaction = await transactionDb.insertTransaction({ info })

    if (insertTransaction.rowCount == 0) {
      throw new Error(`Transaction add failed`)
    }

    info.mainTransactionId = insertTransaction.rows[0].id

    const insertInboundTransaction = await transactionDb.insertInboundTransaction({ info })

    const insertOutboundTransaction = await transactionDb.insertOutboundTransaction({ info })


    const data = {
      msg: `Transaction inserted successfully`,
      id: info.mainTransactionId
    };


    return data


  };
};

module.exports = addNewTransaction;
