
const selectPrintCount = ({
    transactionDb,
  }) => {
    return async function post(info) {
      const mode = info.mode;
  
      if (!info.modules) {
        throw new Error(`Access denied`);
      }


      const printedDocuments = await transactionDb.selectPrintCountByTransactionId({info})

      const data = {
        printed_documents: printedDocuments.rows[0].printed_documents,
        print_limit: printedDocuments.rows[0].print_limit
      }

      return data
  
  
    };
  };
  
  module.exports = selectPrintCount;
  