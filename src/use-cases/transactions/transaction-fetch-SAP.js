// this file is a worker; fetch data from the API of SAP; to get purchase order...
const fetchPurchaseOrder = ({
  transactionDb,
  transactionTypeDb,
  forTransmittalNotifs,
  transactions
}) => {
  return async function selects(info) {

    const mode = info.mode

    if(mode == 1){

      delete info.source;
      delete info.mode;


      const purchaseOrders = await transactions.selectAllPurchaseOrders({info})


      const presentPurchaseOrders = await transactions.selectAllPurchaseOrdersTaggedInTransactionTable({info})


      for(let i = 0; i < purchaseOrders.length; i++){

        let flag = false
        
        for(let i2 = 0; i2<presentPurchaseOrders.length; i2++){

          if(purchaseOrders[i].DocEntry === presentPurchaseOrders[i2].U_APP_PO_ID){
            flag = true
          }



        }

        if(!flag){

          let details = {
            U_TS_STATUS: "for transmittal",
            U_APP_PO_ID: purchaseOrders[i].DocEntry,
            cookie: info.cookie
          }

          await transactions.transactionsAdd(details)
        }

      }


    
       return {purchaseOrders}


    }else {



    // const id = getRandomInt(9999); // mock id; must be the return from route of sap

    // select id of transaction type withdrawal
    const withdrawal = await transactionTypeDb.selectByName({
      name: "withdrawal"
    });
    // transaction type id; withdrawal
    const withdrawalId = withdrawal.rows[0].id;

    // check if that purchase order id exist in db
    const exist = await transactionDb.selectPurchaseOrderId({
      ttype_id: withdrawalId,
      po_id: id
    });

    if (exist.rowCount > 0) {
      throw new Error(`Purchase Order I.D. already exist..`);
    }

    // insert into db with type withdrawal
    const insert = await transactionDb.insertPurchaseOrderId({
      ttype_id: withdrawalId,
      po_id: id,
      created_at: new Date().toISOString(),
      status: "for transmittal"
    });

    const data = {
      msg: `Fetch purchase order from SAP: ${insert.rowCount} transaction.`
    };

    // emit to client count of for transmittal; notif
    await forTransmittalNotifs({});

    return data;
  }
  };
};

module.exports = fetchPurchaseOrder;

// mock purchase ID; return an integer function
const getRandomInt = max => {
  return Math.floor(Math.random() * Math.floor(max));
};
