const getLocations = ({

}) => {
    return async function get(info) {

        const locations = process.env.PRINTLOCATIONS.split(",")

        let view = []

        for (let i = 0; i < locations.length; i++) {

            const matches = locations[i].match(/\b(\w)/g)
            const acronym = matches.join('')
            view.push({ acronym, location: locations[i] })
        }



        return view

    };
};

module.exports = getLocations;
