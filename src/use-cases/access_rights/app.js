const { makeAccessRights } = require("../../entities/access_rights/app"); // entity
const accessRightsDb = require("../../data-access/db-layer/access_rights/app"); //db
const {
  accessRights
} = require("../../data-access/sl-layer/access-rights/app"); // SL
//####################
const addAccessRights = require("./access-rights-add");
const accessRightsSelectAllOnRole = require("./access-rights-select-on-role");

//####################
const addAccessRightss = addAccessRights({
  accessRightsDb,
  makeAccessRights,
  accessRights
});
const accessRightsSelectAllOnRoles = accessRightsSelectAllOnRole({
  accessRightsDb,
  accessRights
});

const services = Object.freeze({
  addAccessRightss,
  accessRightsSelectAllOnRoles
});

module.exports = services;
module.exports = {
  addAccessRightss,
  accessRightsSelectAllOnRoles
};
