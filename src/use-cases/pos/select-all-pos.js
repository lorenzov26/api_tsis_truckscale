const selectAllPos = ({ posDb }) => {
    return async function select() {

      const result = await posDb.selectAllPos()


      const view = result.rows
   
      return view;
    };
  };
  
  module.exports = selectAllPos;
  