const addNewWeightType = ({ weightDb, makeWeight, insertActivityLogss }) => {
  return async function posts(info) {
    const result = makeWeight(info);

    const weightExist = await weightDb.selectByName({
      weight_name: result.getWeightName()
    });

    if (weightExist.rowCount !== 0) {
      throw new Error("The weight name already exist.");
    }

    // insert to db
    const insert = await weightDb.insertNewWeight({
      weight_name: result.getWeightName(),
      created_at: result.getCreatedAt()
    });

    const count = insert.rowCount; // get the number of inserted data

    const data = {
      msg: `Inserted successfully ${count} weight type.`
    };

    // activity logs
    const weight = {
      weight_name: result.getWeightName(),
      created_at: result.getCreatedAt()
    };

    const logs = {
      action_type: "CREATE WEIGHT TYPE",
      table_affected: "ts_weight_types",
      new_values: weight,
      prev_values: null,
      created_at: new Date().toISOString(),
      updated_at: null,
      users_id: info.users_id
    };
    
    await insertActivityLogss({ logs });

    return data;
  };
};

module.exports = addNewWeightType;
