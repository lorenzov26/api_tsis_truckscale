const updateRoles = ({
  rolesDb,

  validateAccessRights,
  moment
}) => {
  return async function put({ id, ...info } = {}) {
    const { mode } = info;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    // const allowed = await validateAccessRights(
    //   info.modules,
    //   "admin",
    //   "edit role"
    // );

    // if (!allowed) {
    //   throw new Error(`Access denied`);
    // }

    //id
    //name
    //updated_by
    //status

    if (!info.name) {
      throw new Error(`Please input role name`)
    }

    const d = new Date();
    info.dateToday = moment(d).format("YYYY-MM-DD HH:mm:ss");

    const roleExists = await rolesDb.selectByRoleNameUpdate({
      info: {
        name: info.name.toLowerCase(),
        id: id
      }
    })

    if (roleExists.rowCount > 0) {
      throw new Error(`Role already exists`)
    }

    const updateRole = await rolesDb.updateRole({
      info: {
        name: info.name.toLowerCase(),
        date_today: info.dateToday,
        status: info.status,
        updated_by: info.updated_by,
        id
      }
    })

    if (updateRole.rowCount == 0) {
      throw new Error(`Update failed`)
    }

    const aR = info.access_rights

    const removeAccessRights = await rolesDb.deleteAccessRightsByRoleId({id})

    for (let i = 0; i < aR.length; i++) {
      const insertAccessRight = await rolesDb.insertAccessRight({
        info: {
          action_id: aR[i],
          role_id: id
        }
      })

    }

    return true

  };
};


module.exports = updateRoles;
