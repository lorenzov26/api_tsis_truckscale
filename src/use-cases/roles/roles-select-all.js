const rolesSelectAll = ({ rolesDb, decrypt, validateAccessRights, roles }) => {
  return async function selects(info) {
 
    const mode = info.mode;

    if(!info.modules){
      throw new Error (`Access denied`)
    }

    // const allowed = await validateAccessRights(info.modules,'admin','view roles')


    // if(!allowed){
    //   throw new Error (`Access denied`)
    // }


    const roles = await rolesDb.selectAllRoles({});

    const view = roles.rows;

    return view
  
  };
};

module.exports = rolesSelectAll;
