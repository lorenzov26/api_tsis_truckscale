const addNewRoles = ({
  rolesDb,
  makeRoles,
  validateAccessRights,
  insertActivityLogss,
  decrypt,
  roles,
  objectLowerCaser,
  moment
}) => {
  return async function posts(info) {


    const mode = info.mode

    if (!info.modules) {
      throw new Error(`Access denied`)
    }

    // const allowed = await validateAccessRights(info.modules, 'admin', 'add role')


    // if (!allowed) {
    //   throw new Error(`Access denied`)
    // }

    if (!info.name) {
      throw new Error(`Please input role name`)
    }

    const d = new Date();
    info.dateToday = moment(d).format("YYYY-MM-DD HH:mm:ss");

    const roleExists = await rolesDb.selectRoleByName({
      info: {
        name: info.name.toLowerCase()
      }
    })

    if (roleExists.rowCount > 0) {
      throw new Error(`Role already exists`)
    }

    const insertRole = await rolesDb.insertRole({
      info: {
        name: info.name.toLowerCase(),
        date_today: info.dateToday,
        created_by: info.created_by
      }
    })


    return true

  };
};

module.exports = addNewRoles;
