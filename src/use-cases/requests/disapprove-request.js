const disapproveRequest = ({
    requestDb, transactionDb, moment, validateAccessRights, userDb, sendEmail
}) => {
    return async function put({ id, ...info } = {}) {

        if (!info.modules) {
            throw new Error(`Access denied`);
        }

        // const allowed = await validateAccessRights(
        //     info.modules,
        //     "request",
        //     "disapprove request"
        // );

        // if (!allowed) {
        //     throw new Error(`Access denied`);
        // }


        if (['cancelled', 'approved', 'disapproved'].includes(info.status.toLowerCase())) {
            throw new Error(`Request can't be disapproved`)
        }

        const d = new Date();
        info.dateToday = moment(d).format("YYYY-MM-DD HH:mm:ss");

        info.status = 'disapproved'

        info.id = id

        const updateRequestStatus = await requestDb.updateRequestStatus({ info })

        if (updateRequestStatus.rowCount == 0) {
            throw new Error(`Updating request status failed`)
        }

        //notifs
        let receivers = []

        const controllers = await userDb.selectEmployeeByRole({
            info: {
                role: 'controller'
            }
        })

        for (let i = 0; i < controllers.rows.length; i++) {
            receivers.push(controllers.rows[i].id)
        }

        receivers.push(updateRequestStatus.rows[0].created_by)

        let tempArr = []

        for (let i = 0; i < receivers.length; i++) {
            if (!tempArr.includes(receivers[i])) {
                tempArr.push(receivers[i])
            }

        }

        receivers = tempArr

        for (let i = 0; i < receivers.length; i++) {

            const insertNotif = await requestDb.addRequestNotif({
                info: {
                    request_id: updateRequestStatus.rows[0].id,
                    receiver_id: receivers[i],
                    request_status: info.status,
                    date: info.dateToday,
                    sender_id: info.updated_by
                }
            })

        }

        //email


        let emailAddressesTo = []

        let doer

        for (let i = 0; i < receivers.length; i++) {

            const userDetails = await userDb.selectEmployeeById({
                info: {
                    id: receivers[i]
                }
            })

            if (userDetails.rows[0].email) {
                emailAddressesTo.push(userDetails.rows[0].email)
            }
        }

        const doerDetails = await userDb.selectEmployeeById({
            info: {
                id: info.updated_by
            }
        })

        doer = `${doerDetails.rows[0].first_name} ${doerDetails.rows[0].last_name}`

        const mailOptions = {
            from: `Biotech Farms Inc. <lafbelmonte@gmail.com>`,
            to: emailAddressesTo,
            subject: "Request disapproved from Truckscale Integrated System",
            text: `Good Day Ma'am/Sir,\n\nRequest for Ticket Number ${info.ticket_no} have been disapproved by ${doer}.\n\nThank you.`
        }

        const sendEmailNotification = await sendEmail(mailOptions)

        console.log(sendEmailNotification)

        const data = {
            msg: `Request disapproved successfully`
        };

        return data

    };
};


module.exports = disapproveRequest;
