const approveRequest = ({
    requestDb, transactionDb, moment, validateAccessRights, userDb, sendEmail
}) => {
    return async function put({ id, ...info } = {}) {

        if (!info.modules) {
            throw new Error(`Access denied`);
        }

        // const allowed = await validateAccessRights(
        //     info.modules,
        //     "request",
        //     "approve request"
        // );

        // if (!allowed) {
        //     throw new Error(`Access denied`);
        // }

        if (['cancelled', 'approved', 'disapproved'].includes(info.status.toLowerCase())) {
            throw new Error(`Request can't be approved`)
        }

        const d = new Date();
        info.dateToday = moment(d).format("YYYY-MM-DD HH:mm:ss");

        info.status = 'approved'

        const transactionDetails = await transactionDb.selectOneTransaction({
            info: {
                id: info.ticket_no
            }
        })

        const currentPrintCount = transactionDetails.rows[0].print_count

        info.print_count = info.print_count + currentPrintCount
        info.id = id

        // const requestDetails = await requestDb.selectOneRequestById({ info })

        // if (requestDetails.rows[0].is_edit) {

        //     const transaction = transactionDetails.rows[0]

        //     const oldData = JSON.parse(requestDetails.rows[0].current_data)

        //     const newData = {
        //         plate_number: transaction.plate_number,
        //         driver_first_name: transaction.driver_first_name,
        //         driver_last_name: transaction.driver_last_name,
        //         supplier: transaction.supplier_name,
        //         supplier_address: transaction.supplier_address,
        //         item: transaction.item,
        //         unit_of_measure: transaction.unit_of_measure,
        //         quantity: transaction.number_of_bags,
        //         remarks: transaction.remarks
        //     }

        //     if (isObjectEquivalent(oldData, newData)) {
        //         throw new Error(`Report was not yet edited`)
        //     }

        // }


        const updateTransactionPrintCount = await transactionDb.updatePrintCountById({ info })

        if (updateTransactionPrintCount.rowCount == 0) {
            throw new Error(`Updating print count failed`)
        }

        const updateRequestStatus = await requestDb.updateRequestStatus({ info })

        if (updateRequestStatus.rowCount == 0) {
            throw new Error(`Updating request status failed`)
        }

        //notif
        let receivers = []

        const controllers = await userDb.selectEmployeeByRole({
            info: {
                role: 'controller'
            }
        })

        for (let i = 0; i < controllers.rows.length; i++) {
            receivers.push(controllers.rows[i].id)
        }

        receivers.push(updateRequestStatus.rows[0].created_by)

        let tempArr = []

        for (let i = 0; i < receivers.length; i++) {
            if (!tempArr.includes(receivers[i])) {
                tempArr.push(receivers[i])
            }

        }

        receivers = tempArr


        for (let i = 0; i < receivers.length; i++) {

            const insertNotif = await requestDb.addRequestNotif({
                info: {
                    request_id: updateRequestStatus.rows[0].id,
                    receiver_id: receivers[i],
                    request_status: info.status,
                    date: info.dateToday,
                    sender_id: info.updated_by
                }
            })

        }

        //email

        let emailAddressesTo = []

        let doer


        for (let i = 0; i < receivers.length; i++) {

            const userDetails = await userDb.selectEmployeeById({
                info: {
                    id: receivers[i]
                }
            })

            if (userDetails.rows[0].email) {
                emailAddressesTo.push(userDetails.rows[0].email)
            }
        }

        const doerDetails = await userDb.selectEmployeeById({
            info: {
                id: info.updated_by
            }
        })

        doer = `${doerDetails.rows[0].first_name} ${doerDetails.rows[0].last_name}`

        const mailOptions = {
            from: `Biotech Farms Inc. <lafbelmonte@gmail.com>`,
            to: emailAddressesTo,
            subject: "Request approved from Truckscale Integrated System",
            text: `Good Day Ma'am/Sir,\n\nRequest for Ticket Number ${info.ticket_no} have been approved by ${doer}.\n\nThank you.`
        }

        const sendEmailNotification = await sendEmail(mailOptions)

        console.log(sendEmailNotification)

        const data = {
            msg: `Request approved successfully`
        };

        return data

    };
};

function isObjectEquivalent(a, b) {

    let aProps = Object.getOwnPropertyNames(a);
    let bProps = Object.getOwnPropertyNames(b);


    if (aProps.length != bProps.length) {
        return false;
    }

    for (let i = 0; i < aProps.length; i++) {
        let propName = aProps[i];


        if (a[propName] !== b[propName]) {
            return false;
        }
    }

    return true;
}


module.exports = approveRequest;
