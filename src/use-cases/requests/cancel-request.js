const cancelRequest = ({
    requestDb, transactionDb, moment, validateAccessRights, userDb
}) => {
    return async function put({ id, ...info } = {}) {

        if (!info.modules) {
            throw new Error(`Access denied`);
        }

        // const allowed = await validateAccessRights(
        //     info.modules,
        //     "request",
        //     "disapprove request"
        // );

        // if (!allowed) {
        //     throw new Error(`Access denied`);
        // }

        if(info.status.toLowerCase() !== 'pending'){
            throw new Error(`Request can't be cancelled`)
        }
                
        const d = new Date();
        info.dateToday = moment(d).format("YYYY-MM-DD HH:mm:ss");

        info.status = 'cancelled'

        info.id = id

        const updateRequestStatus = await requestDb.updateRequestStatus({ info })

        if (updateRequestStatus.rowCount == 0) {
            throw new Error(`Updating request status failed`)
        }

        const data = {
            msg: `Request cancelled successfully`
        };

        return data


    };
};


module.exports = cancelRequest;
