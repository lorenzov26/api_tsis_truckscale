const selectAllRequests = ({ requestDb, validateAccessRights }) => {
    return async function get(info) {
  
   
      const mode = info.mode;
  
      if(!info.modules){
        throw new Error (`Access denied`)
      }
  
      // const allowed = await validateAccessRights(info.modules,'request','view requests')
  
  
      // if(!allowed){
      //   throw new Error (`Access denied`)
      // }

      const requests = await requestDb.selectAllRequests({info})

      const view = requests.rows

      return view
  
    
    };
  };
  
  module.exports = selectAllRequests;
  