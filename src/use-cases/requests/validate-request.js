const validateRequest = ({
    requestDb, transactionDb, moment, validateAccessRights, userDb
}) => {
    return async function put({ id, ...info } = {}) {


        if (!info.modules) {
            throw new Error(`Access denied`);
        }


        if (info.status.toLowerCase() !== 'pending') {
            throw new Error(`Request can't be validated`)
        }

        const d = new Date();
        info.dateToday = moment(d).format("YYYY-MM-DD HH:mm:ss");

        info.status = 'validated'

        info.id = id

        const updateRequestStatus = await requestDb.updateRequestStatus({ info })

        if (updateRequestStatus.rowCount == 0) {
            throw new Error(`Updating request status failed`)
        }

        let receivers = []

        const supervisors = await userDb.selectEmployeeByRole({
            info: {
                role: 'supervisor'
            }
        })

        for (let i = 0; i < supervisors.rows.length; i++) {
            receivers.push(supervisors.rows[i].id)
        }

        receivers.push(updateRequestStatus.rows[0].created_by)

        for (let i = 0; i < receivers.length; i++) {

            const insertNotif = await requestDb.addRequestNotif({
                info: {
                    request_id: updateRequestStatus.rows[0].id,
                    receiver_id: receivers[i],
                    request_status: info.status,
                    date: info.dateToday,
                    sender_id: info.updated_by
                }
            })
        }

        const data = {
            msg: `Request validated successfully`
        };

        return data

    };
};


module.exports = validateRequest;
