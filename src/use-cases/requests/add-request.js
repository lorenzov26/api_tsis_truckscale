const addRequest = ({
  requestDb, moment, validateAccessRights, transactionDb, userDb, sendEmail
}) => {
  return async function post(info) {

    if (!info.modules) {
      throw new Error(`Access denied`)
    }

    // const allowed = await validateAccessRights(info.modules, 'request', 'add request')


    // if (!allowed) {
    //   throw new Error(`Access denied`)
    // }

    if (!info.printCount) {
      info.printCount = 0
    }

    if (info.printCount < 0) {
      throw new Error(`Invalid count`)
    }

    if (!info.reason) {
      throw new Error(`Please input reason`)
    }

    if (!info.isEdit && !info.isReprint) {
      throw new Error(`Please select if reprint, edit or both`)
    }

    let draft = null

    if (info.isEdit) {

      if (!info.draft.plateNumber) {

        throw new Error(`Please input plate number`)

      }

      if (!info.draft.driver) {

        throw new Error(`Please input driver`)

      }

      if (!info.draft.supplier) {

        throw new Error(`Please input supplier`)

      }

      if (!info.draft.supplierAddress) {

        throw new Error(`Please input supplier address`)

      }

      if (!info.draft.item) {

        throw new Error(`Please input item`)

      }

      if (info.draft.numberOfBags < 1) {

        throw new Error(`Invalid quantity`)

      }

      if (!info.draft.selectedUom) {

        throw new Error(`Please input unit of measure`)

      }

      const transactionDetails = await transactionDb.selectOneTransaction({
        info: {
          id: info.transactionId
        }
      })

      const transaction = transactionDetails.rows[0]

      const currentDriver = `${transaction.driver_last_name}, ${transaction.driver_first_name}`


      const currentData = {
        plate_number: transaction.plate_number,
        driver: currentDriver,
        supplier: transaction.supplier_name,
        supplier_address: transaction.supplier_address,
        item: transaction.item,
        unit_of_measure: transaction.uom_id,
        quantity: transaction.number_of_bags,
        remarks: transaction.remarks,
        id: transaction.transaction_id
      }

      draft = {
        plate_number: info.draft.plateNumber,
        driver: info.draft.driver,
        supplier: info.draft.supplier,
        supplier_address: info.draft.supplierAddress,
        item: info.draft.item,
        unit_of_measure: info.draft.selectedUom,
        quantity: info.draft.numberOfBags,
        remarks: info.draft.remarks,
        id: info.draft.id
      }

      const match = await isObjectEquivalent(currentData, draft)

      if (match) {
        throw new Error(`No difference found`)
      }

      info.draft = JSON.stringify(draft)

    }

    if (info.isReprint) {
      if (info.printCount < 1) {
        throw new Error(`Please input number of additional print`)
      }
    }

    const d = new Date();
    info.dateToday = moment(d).format("YYYY-MM-DD HH:mm:ss");

    info.status = 'pending'

    const pendingRequestExists = await requestDb.selectOneRequestByTransactionIdThatIsPending({
      info: {
        id: info.transactionId
      }
    })

    if (pendingRequestExists.rowCount > 0) {
      throw new Error(`There is still unfinished request for this transaction`)
    }

    const insertRequest = await requestDb.addRequest({ info })

    if (insertRequest.rowCount == 0) {
      throw new Error(`Request add failed`)
    }


    //Notifs

    let receivers = []

    const controllers = await userDb.selectEmployeeByRole({
      info: {
        role: 'controller'
      }
    })

    for (let i = 0; i < controllers.rows.length; i++) {
      receivers.push(controllers.rows[i].id)
    }

    const supervisors = await userDb.selectEmployeeByRole({
      info: {
        role: 'supervisor'
      }
    })

    for (let i = 0; i < supervisors.rows.length; i++) {
      receivers.push(supervisors.rows[i].id)
    }

    if (receivers.includes(parseInt(info.created_by))) {
      receivers.splice(receivers.indexOf(parseInt(info.created_by)), 1)
    }

    for (let i = 0; i < receivers.length; i++) {

      const insertNotif = await requestDb.addRequestNotif({
        info: {
          request_id: insertRequest.rows[0].id,
          receiver_id: receivers[i],
          request_status: info.status,
          date: info.dateToday,
          sender_id: info.created_by
        }
      })
    }


    //email
    let emailAddressesTo = []

    let doer

    for (let i = 0; i < receivers.length; i++) {

      const userDetails = await userDb.selectEmployeeById({
        info: {
          id: receivers[i]
        }
      })

      if (userDetails.rows[0].email) {
        emailAddressesTo.push(userDetails.rows[0].email)
      }
    }

    const doerDetails = await userDb.selectEmployeeById({
      info: {
        id: info.created_by
      }
    })

    doer = `${doerDetails.rows[0].first_name} ${doerDetails.rows[0].last_name}`

    const mailOptions = {
      from: `Biotech Farms Inc. <lafbelmonte@gmail.com>`,
      to: emailAddressesTo,
      subject: "Request received from Truckscale Integrated System",
      text: `Good Day Ma'am/Sir,\n\nA Request from ${doer} was received.\n\nTicket Number: ${info.transactionId} \nReason: ${info.reason}\n\nPlease address this request as soon as possible.\n\nThank you.`
    }

    const sendEmailNotification = await sendEmail(mailOptions)

    console.log(sendEmailNotification)

    const data = {
      msg: `Request added successfully`
    };

    return data

  };
};

async function isObjectEquivalent(a, b) {

  let aProps = Object.getOwnPropertyNames(a);
  let bProps = Object.getOwnPropertyNames(b);


  if (aProps.length != bProps.length) {
    return false;
  }

  for (let i = 0; i < aProps.length; i++) {
    let propName = aProps[i];


    const loweredValueA = a[propName] && !Number.isInteger(a[propName]) ? await a[propName].toLowerCase() : a[propName]
    const loweredValueB = b[propName] && !Number.isInteger(b[propName]) ? await b[propName].toLowerCase() : b[propName]

    if (loweredValueA !== loweredValueB) {
      return false;
    }
  }

  return true;
}


module.exports = addRequest;
