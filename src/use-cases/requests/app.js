const moment = require("moment");


const requestDb = require("../../data-access/db-layer/requests/app")
const transactionDb = require("../../data-access/db-layer/transactions/app");
const userDb = require("../..//data-access/db-layer/users/app")

const { validateAccessRights } = require("../../validator/app")

const { sendEmail } = require("../../mailer/app")




//####################
const addRequest = require("./add-request")
const selectAllRequests = require("./select-all-requests")
const approveRequest = require("./approve-request")
const disapproveRequest = require("./disapprove-request")
const validateRequest = require("./validate-request")
const cancelRequest = require("./cancel-request")
//####################
const u_addRequest = addRequest({ requestDb, moment, validateAccessRights, transactionDb, userDb, sendEmail })
const u_selectAllRequests = selectAllRequests({requestDb, validateAccessRights})
const u_approveRequest = approveRequest({requestDb, transactionDb, moment, validateAccessRights, userDb, sendEmail})
const u_disapproveRequest = disapproveRequest({requestDb, transactionDb, moment, validateAccessRights, userDb, sendEmail})
const u_validateRequest = validateRequest({requestDb, transactionDb, moment, validateAccessRights, userDb})
const u_cancelRequest = cancelRequest({requestDb, transactionDb, moment, validateAccessRights, userDb})


const services = Object.freeze({
    u_addRequest,
    u_selectAllRequests,
    u_approveRequest,
    u_disapproveRequest,
    u_validateRequest,
    u_cancelRequest
});

module.exports = services;
module.exports = {
    u_addRequest,
    u_selectAllRequests,
    u_approveRequest,
    u_disapproveRequest,
    u_validateRequest,
    u_cancelRequest
};
