const closeSerialPort = ({ port }) => {
  return async function get() {
    try {
      if (port.isOpen === true) {
        await port.close(err => {
          if (err) {
            return console.log("Error on closing port", err);
          }
        });
        const data = { msg: `${port.path} is closed.` };
        return data;
      }
    } catch (e) {
      throw new Error(e);
    }
  };
};

module.exports = closeSerialPort;
