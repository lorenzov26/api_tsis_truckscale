const serialComPort = ({ SerialPort, comPortDb, readData, port, ports }) => {
  return async function get({ info }) {
    try {
      const { mode } = info;
      delete info.mode;
      if (mode == 1) {
        const { cookie } = info;
        const result = readData(info);

        const ip = result.getIp();
        const device = result.getHostName();

        const devices = {
          ip,
          device,
          cookie
        };

        const conf = await ports.selectConfigOfDevice({
          info: devices
        });

        if (conf.status == 200) {
          const config = conf.data[0];

          // com port to connect
          const comPort = result.getCOMPort();

          const options = {
            baudRate: config.U_TS_BAUDRATE,
            parity: config.U_TS_PARITY,
            dataBits: config.U_TS_DATABITS,
            stopBits: config.U_TS_STOPBITS
          };

          const port = new SerialPort(comPort, options, async err => {
            try {
              if (err) {
                return console.log("Error on connecting: ", err);
              }
              console.log(`Connected successfully to ${comPort}.`);
            } catch (e) {
              throw new Error(e);
            }
          });
          return port;
        } else {
          throw new Error(`No device found..`);
        }
      } else {

        const result = readData(info);

        const ip = result.getIp();
        const device = result.getHostName();

        const devices = {
          ip,
          device
        };

        const configDetails = await comPortDb.selectConfigOfDevice({info: devices})

        if(configDetails.rows.length === 0){
          throw new Error(`Device not found`)
        }
        
        const config = configDetails.rows[0]
      
        const comPort = result.getCOMPort();

        const options = {
          baudRate: config.truckscale_baudrate,
          parity: config.truckscale_parity,
          dataBits: config.truckscale_databits,
          stopBits: config.truckscale_stopbits
        };

        const port = new SerialPort(comPort, options, async err => {
          try {
            if (err) {
              return console.log("Error on connecting: ", err);
            }
            console.log(`Connected successfully to ${comPort}.`);
          } catch (e) {
            throw new Error(e);
          }
        });

       
        return port;
      }
    } catch (e) {
      console.log(e)
      throw new Error(e);
    }
  };
};

module.exports = serialComPort;
