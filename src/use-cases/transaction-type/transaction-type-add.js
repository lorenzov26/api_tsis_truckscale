const addNewTransactionType = ({
  transactionTypeDb,
  makeTransactionType,
  insertActivityLogss
}) => {
  return async function posts(info) {
    const result = makeTransactionType(info);

    const transactionTypeExist = await transactionTypeDb.selectByName({
      name: result.getTransactionType()
    });

    if (transactionTypeExist.rowCount !== 0) {
      throw new Error("The transaction type already exist.");
    }

    // insert to db
    const insert = await transactionTypeDb.insertNewTransactionType({
      name: result.getTransactionType(),
      created_at: result.getCreatedAt()
    });

    const count = insert.rowCount; // get the number of inserted data

    const data = {
      msg: `Inserted successfully ${count} transaction type.`
    };

    // logs
    // new values
    const new_values = {
      name: result.getTransactionType(),
      created_at: result.getCreatedAt()
    };

    const logs = {
      action_type: "CREATE TRANSACTION TYPE",
      table_affected: "ts_transaction_types",
      new_values,
      prev_values: null,
      created_at: new Date().toISOString(),
      updated_at: null,
      users_id: info.users_id
    };

    await insertActivityLogss({ logs });

    return data;
  };
};

module.exports = addNewTransactionType;
