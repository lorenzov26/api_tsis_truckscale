const selectAllSubTypes = ({ transactionTypeDb }) => {
    return async function get(info) {
  
      const mode = info.mode
  
      if(mode == 1){
        
        return true
       
      }else{

       

        const transactionSubTypes = await transactionTypeDb.selectAllTransactionSubType()

        const view = transactionSubTypes.rows
  
        return view
      }
    };
  };
  
  module.exports = selectAllSubTypes;
  