const updateDriver = ({
  driversDb,
  makeDriver,
  validateAccessRights,
  insertActivityLogss,
  decrypt,
  objectLowerCaser,
  drivers,
  moment
}) => {
  return async function put({ id, ...info } = {}) {

    const mode = info.mode

    if (!info.modules) {
      throw new Error(`Access denied`)
    }

    // const allowed = await validateAccessRights(info.modules, 'truck and driver', 'edit driver')


    // if (!allowed) {
    //   throw new Error(`Access denied`)
    // }


    if (!info.firstname) {
      throw new Error(`Please input firstname`)
    }

    if (!info.lastname) {
      throw new Error(`Please input lastname`)
    }


    const d = new Date();
    info.dateToday = moment(d).format("YYYY-MM-DD HH:mm:ss");

    info.id = id
    const driverExist = await driversDb.selectOneDriverByNameForUpdate({ info })


    if (driverExist.rows.length > 0) {
      throw new Error(`Driver already exists`)
    }


    const updateDriver = await driversDb.updateDriver({ info })


    const count = updateDriver.rowCount; // get the number of updated data

    const data = {
      msg: `Updated successfully ${count} driver.`
    };

    return data

  };
};


module.exports = updateDriver;
