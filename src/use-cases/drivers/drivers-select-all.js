const driversSelectAll = ({
  driversDb,
  decrypt,
  validateAccessRights,
  drivers
}) => {
  return async function selects(info) {
    const mode = info.mode;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    // const allowed = await validateAccessRights(
    //   info.modules,
    //   "truck and driver",
    //   "view drivers"
    // );

    // if (!allowed) {
    //   throw new Error(`Access denied`);
    // }





    const result = await driversDb.selectAllDrivers({})

    const drivers = result.rows

    return drivers


  };
};

module.exports = driversSelectAll;
