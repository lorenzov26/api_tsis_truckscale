const driversSelectOne = ({ driversDb, decrypt, drivers, validateAccessRights }) => {
  return async function selects(info) {


    const mode = info.mode

    if(!info.modules){
      throw new Error (`Access denied`)
    }

    const allowed = await validateAccessRights(info.modules,'truck and driver','view drivers')


    if(!allowed){
      throw new Error (`Access denied`)
    }

    if(mode == 1){


      
      delete info.modules
      delete info.source;
      delete info.mode;


     const res = await drivers.driversSelectOne({info});

      return res

    }else {

    
    const result = await driversDb.selectOneDriver({ id });
    const drivers = result.rows;

    let data = []; //declare empty array to push decrypted data

    // loop thru drivers encrypted data
    for await (let d of drivers) {
      // no middle name and name extension
      if (!d.middlename && !d.name_extension) {
        data.push({
          id: d.id,
          firstname: decrypt(d.firstname),
          middlename: "",
          lastname: decrypt(d.lastname),
          name_extension: ""
        });
      }
      // has middle name no name extension
      if (d.middlename && !d.name_extension) {
        data.push({
          id: d.id,
          firstname: decrypt(d.firstname),
          middlename: decrypt(d.middlename),
          lastname: decrypt(d.lastname),
          name_extension: ""
        });
      }
      // no middle name has name extension
      if (!d.middlename && d.name_extension) {
        data.push({
          id: d.id,
          firstname: decrypt(d.firstname),
          middlename: "",
          lastname: decrypt(d.lastname),
          name_extension: decrypt(d.name_extension)
        });
      }
      // has both
      if (d.middlename && d.name_extension) {
        data.push({
          id: d.id,
          firstname: decrypt(d.firstname),
          middlename: decrypt(d.middlename),
          lastname: decrypt(d.lastname),
          name_extension: decrypt(d.name_extension)
        });
      }
    }
    return data;
  }
  };
};

module.exports = driversSelectOne;
