const addNewDriver = ({
  driversDb,
  makeDriver,
  validateAccessRights,
  insertActivityLogss,
  decrypt,
  drivers,
  objectLowerCaser,
  moment
}) => {
  return async function posts(info) {
    const mode = info.mode;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    // const allowed = await validateAccessRights(
    //   info.modules,
    //   "truck and driver",
    //   "add driver"
    // );

    // if (!allowed) {
    //   throw new Error(`Access denied`);
    // }




    if (!info.firstname) {
      throw new Error(`Please input first name`)
    }

    if (!info.lastname) {
      throw new Error(`Please input last name`)
    }

    if (!info.created_by) {
      throw new Error(`Please input who created this driver`)
    }


    const d = new Date();
    info.dateToday = moment(d).format("YYYY-MM-DD HH:mm:ss");


    const driverExist = await driversDb.selectOneDriverByNameForAdd({ info })


    if (driverExist.rows.length > 0) {
      throw new Error(`Driver already exists`)
    }


    const insertDriver = await driversDb.insertDriver({ info })

    const count = insertDriver.rowCount; // get the number of inserted data

    const data = {
      msg: `Inserted successfully ${count} driver.`
    };

    return data


  };
};



module.exports = addNewDriver;
