const addNewRawMaterial = ({
  rawMaterialDb,
  makeRawMaterial,
  insertActivityLogss,
  decrypt
}) => {
  return async function posts(info) {
    const result = makeRawMaterial(info);

    const rawMaterialExist = await rawMaterialDb.selectByName({
      description: result.getDescription()
    });

    if (rawMaterialExist.rowCount !== 0) {
      throw new Error("The raw material already exist.");
    }

    // insert to db
    const insert = await rawMaterialDb.insertNewRawMaterial({
      description: result.getDescription(),
      created_by: result.getCreatedBy(),
      created_at: result.getCreatedAt()
    });

    const count = insert.rowCount; // get the number of inserted data

    const data = {
      msg: `Inserted successfully ${count} raw material.`
    };

    const user = await rawMaterialDb.returnCreatedBy({
      id: result.getCreatedBy()
    });
    const create = user.rows[0];
    const created_by = {
      id: create.id,
      employee_id: create.employee_id ? decrypt(create.employee_id) : "",
      firstname: create.firstname ? decrypt(create.firstname) : "",
      lastname: create.lastname ? decrypt(create.lastname) : ""
    };

    // logs
    // new values
    const new_values = {
      description: result.getDescription(),
      created_at: result.getCreatedAt(),
      created_by
    };

    const logs = {
      action_type: "CREATE RAW MATERIAL",
      table_affected: "ts_raw_materials",
      new_values,
      prev_values: null,
      created_at: new Date().toISOString(),
      updated_at: null,
      users_id: info.users_id
    };

    await insertActivityLogss({ logs });

    return data;
  };
};

module.exports = addNewRawMaterial;
