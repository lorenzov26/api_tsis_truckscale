const selectAllTruckInfos = ({ truckInfosDb, truckInfos, validateAccessRights }) => {
  return async function get(info) {

    const mode = info.mode

    if (!info.modules) {
      throw new Error(`Access denied`)
    }

    // const allowed = await validateAccessRights(info.modules,'truck and driver','view trucks')


    // if(!allowed){
    //   throw new Error (`Access denied`)
    // }


    const truckInfos = await truckInfosDb.selectAllTruckInfos({})



    const view = truckInfos.rows

    return view


  };
};

module.exports = selectAllTruckInfos;
