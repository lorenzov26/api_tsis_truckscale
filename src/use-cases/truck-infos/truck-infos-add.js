const addTruckInfo = ({
  truckInfosDb,
  truckTypesDb,
  e_addTruckInfo,
  insertActivityLogss,
  truckInfos,
  objectLowerCaser,
  validateAccessRights,
  moment
}) => {
  return async function post(info) {

    const mode = info.mode

    if (!info.modules) {
      throw new Error(`Access denied`)
    }

    // const allowed = await validateAccessRights(info.modules, 'truck and driver', 'add truck')


    // if (!allowed) {
    //   throw new Error(`Access denied`)
    // }

    if (!info.plate_number) {
      throw new Error(`Please input plate number`)
    }

    if (!info.truck_type_id) {
      throw new Error(`Please input truck type ID`)
    }

    const d = new Date();
    info.dateToday = moment(d).format("YYYY-MM-DD HH:mm:ss");

    const plateNumberExist = await truckInfosDb.selectOneTruckInfosByPlateNumberForAdd({ info })

    if (plateNumberExist.rows.length > 0) {
      throw new Error(`Plate number already exists`)
    }

    const insertTruckInfos = await truckInfosDb.insertTruckInfos({ info })

    const count = insertTruckInfos.rowCount;

    const data = {
      msg: `Inserted successfully ${count} truck info.`
    };

    return data


  };
};


module.exports = addTruckInfo;
