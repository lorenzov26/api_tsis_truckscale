const updateTruckInfo = ({
  truckInfosDb,
  truckTypesDb,
  e_updateTruckInfo,
  insertActivityLogss,
  truckInfos,
  objectLowerCaser,
  validateAccessRights,
  moment
}) => {
  return async function put({ id, ...info } = {}) {

    const mode = info.mode

    if (!info.modules) {
      throw new Error(`Access denied`)
    }

    // const allowed = await validateAccessRights(info.modules, 'truck and driver', 'edit truck')


    // if (!allowed) {
    //   throw new Error(`Access denied`)
    // }



    if (!info.plate_number) {
      throw new Error(`Please input plate number`)
    }


    if (!info.truck_type_id) {
      throw new Error(`Please input truck type ID`)
    }

    const d = new Date();
    info.dateToday = moment(d).format("YYYY-MM-DD HH:mm:ss");

    info.id = id


    const plateNumberExist = await truckInfosDb.selectOneTruckInfosByPlateNumberForUpdate({ info })

    if (plateNumberExist.rows.length > 0) {
      throw new Error(`Plate number already exists`)
    }

    const updateTruckInfo = await truckInfosDb.updateTruckInfo({ info })

    const count = updateTruckInfo.rowCount;

    const data = {
      msg: `Updated successfully ${count} truck info.`
    };


    return data


  };
};

module.exports = updateTruckInfo;
