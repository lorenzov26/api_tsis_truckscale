const importItems = ({
    itemsDb, parser, moment
    }) => {
      return async function selects(info) {

        const d = new Date();
        const dateToday = moment(d).format("YYYY-MM-DD HH:mm:ss");

        const doc = parser.parseXls2Json(`C:\\truckscale offline mode\\api_tsis\\master_data\\item_final.xlsx`); 

        const excelData = doc[0]

        const finalData = []



       for(let i = 0; i<excelData.length; i++){
          

          if(!finalData.includes(excelData[i].Item_Description)){
            finalData.push(excelData[i].Item_Description)
           
          }
        }

        for(let i = 0; i<finalData.length; i++){

          const exists = await itemsDb.selectOneItemByName({info: {
            item: finalData[i]
          }})


          if(exists.rowCount == 0){
          const insertItem = await itemsDb.insertItem({
            info: {
              item: finalData[i],
              dateToday: dateToday,
              createdby: 1
            }
          })
          }
        }

        return true
      };
    };
    
module.exports = importItems;
    