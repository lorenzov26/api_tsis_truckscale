const insertUom = ({
    itemsDb, moment
  }) => {
    return async function post(info) {

        if(!info.uomName){
            throw new Error(`Please input unit of measure name`)
        }

        if(!info.created_by){
            throw new Error(`Please input created by`)
        }

        const d = new Date();
        info.dateToday = moment(d).format("YYYY-MM-DD HH:mm:ss");

        const uomExist = await itemsDb.selectOneUomByName({info})

        if(uomExist.rowCount > 0) {
            throw new Error(`Unit of measure already exists`)
        }
        
        const insertUom = await itemsDb.insertUom({info})

        return true
      
    };
  };
  
  module.exports = insertUom;
  