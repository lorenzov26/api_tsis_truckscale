const uomSelectAll = ({
    itemsDb
    }) => {
      return async function get(info) {


        const uoms = await itemsDb.selectAllUoms({info})

        const view = uoms.rows

        return view
       
      };
    };
    
module.exports = uomSelectAll;
    