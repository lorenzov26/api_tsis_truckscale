
const itemsDb = require("../../data-access/db-layer/items/app"); //db

const parser = require('simple-excel-to-json')

const moment = require("moment")


//####################

const itemsSelectAll = require("./items-select-all")
const importItems = require("./import-items")
const uomSelectAll = require("./uom-select-all")
const insertUom = require("./uom-insert")
const updateUom = require("./uom-update")

//####################

const uc_itemsSelectAll = itemsSelectAll({itemsDb})
const uc_importItems = importItems({itemsDb, parser, moment})
const u_uomSelectAll = uomSelectAll({itemsDb})
const u_insertUom = insertUom({itemsDb, moment})
const u_updateUom = updateUom({itemsDb, moment})

const services = Object.freeze({
    uc_itemsSelectAll,
    uc_importItems,
    u_uomSelectAll,
    u_insertUom,
    u_updateUom
});

module.exports = services;
module.exports = {
    uc_itemsSelectAll,
    uc_importItems,
    u_uomSelectAll,
    u_insertUom,
    u_updateUom
};
