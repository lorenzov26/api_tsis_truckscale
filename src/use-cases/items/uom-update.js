const updateUom = ({
    itemsDb,
    moment
  }) => {
    return async function put({ id, ...info }) {

        if(!info.name){
            throw new Error(`Please input name`)
        }

        info.id = id

        const d = new Date();
        info.dateToday = moment(d).format("YYYY-MM-DD HH:mm:ss");

        const uomExists = await itemsDb.selectOneUomByNameForUpdate({info})

        if(uomExists.rowCount > 0){
            throw new Error(`Unit of measure already exists`)
        }

        const updateUom = await itemsDb.updateUom({info})

        return true
      
  };
  

  };
  
  module.exports = updateUom;
  