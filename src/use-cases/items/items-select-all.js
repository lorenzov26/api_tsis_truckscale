const itemsSelectAll = ({
  itemsDb
  }) => {
    return async function selects(info) {

      if(info.mode == 1){
        return info
      }else {

        const items = await itemsDb.selectAllItems({info})

        const view = items.rows

        return view
      }  
    };
  };
  
  module.exports = itemsSelectAll;
  