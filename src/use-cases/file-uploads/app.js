const imageUpload = require("./uploads");

const imageUploads = imageUpload({});

const services = Object.freeze({
  imageUploads
});

module.exports = services;
module.exports = {
  imageUploads
};
