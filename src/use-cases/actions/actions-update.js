const updateActions = ({
  actionsDb,
  makeActionsUpdate,
  insertActivityLogss,
  decrypt,
  actions,
  objectLowerCaser,
  validateAccessRights
}) => {
  return async function put({ id, ...info } = {}) {
    const { mode } = info;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    const allowed = await validateAccessRights(
      info.modules,
      "admin",
      "edit action"
    );

    if (!allowed) {
      throw new Error(`Access denied`);
    }

    if (mode == 1) {
      delete info.modules;

      delete info.mode;
      delete info.source;
      info.id = id;

      info = await objectLowerCaser(info);

      val({ info });

      const check = await actions.actionsAddSelectByName({ info });

      if (check.status == 401) {
        // session expired
        throw new Error("Session expired, please login again.");
      }

      const length = check.length;

      if (length > 0) {
        throw new Error(`Action name already exists.`);
      }

      const res = await actions.actionsUpdate({ info });
      return res;
    } else {
      // offline mode
      const result = makeActionsUpdate(info);

      const actionExist = await actionsDb.selectActionNameUpdate({
        module_id: result.getModuleId(),
        description: result.getDesc(),
        id
      });
      if (actionExist.rowCount !== 0) {
        throw new Error("The action name already exist.");
      }

      // logs
      const previous = await actionsDb.selectOneAction({ id });
      const prev_data = previous.rows[0];
      const prev_values = {
        actions_id: prev_data.actions_id,
        description: prev_data.description,
        status: prev_data.status,
        module: {
          modules_id: prev_data.modules_id,
          descriptions: prev_data.descriptions,
          status: prev_data.modules_status
        },
        created_by: {
          create_fn: prev_data.create_fn ? decrypt(prev_data.create_fn) : "",
          create_mn: prev_data.create_mn ? decrypt(prev_data.create_mn) : "",
          create_ln: prev_data.create_ln ? decrypt(prev_data.create_ln) : "",
          created_at: prev_data.created_at
        },
        modified_by: {
          modify_fn: prev_data.modify_fn ? decrypt(prev_data.modify_fn) : "",
          modify_mn: prev_data.modify_mn ? decrypt(prev_data.modify_mn) : "",
          modify_ln: prev_data.modify_ln ? decrypt(prev_data.modify_ln) : "",
          updated_at: prev_data.updated_at
        }
      };

      // update to db
      const update = await actionsDb.updateAction({
        module_id: result.getModuleId(),
        description: result.getDesc(),
        status: result.getStatus(),
        modified_by: result.getModifiedBy(),
        updated_at: result.getUpdatedAt(),
        id
      });

      const count = update.rowCount; // get the number of inserted data

      const data = {
        msg: `Updated successfully ${count} action.`
      };

      // logs
      // users
      const user = await actionsDb.returnCreatedBy({
        id: result.getModifiedBy()
      });
      const create = user.rows[0];
      const modified_by = {
        id: create.id,
        employee_id: create.employee_id ? decrypt(create.employee_id) : "",
        firstname: create.firstname ? decrypt(create.firstname) : "",
        lastname: create.lastname ? decrypt(create.lastname) : ""
      };

      // modules
      const modules = await actionsDb.returnModule({
        id: result.getModuleId()
      });
      const moduless = modules.rows[0];
      const module = {
        id: moduless.id,
        descriptions: moduless.descriptions,
        status: moduless.status
      };

      // logs
      // new values
      const new_values = {
        description: result.getDesc(),
        status: result.getStatus(),
        updated_at: result.getUpdatedAt(),
        module,
        modified_by
      };

      const logs = {
        action_type: "UPDATE ACTION",
        table_affected: "ts_actions",
        new_values,
        prev_values,
        created_at: null,
        updated_at: new Date().toISOString(),
        users_id: result.getModifiedBy()
      };

      await insertActivityLogss({ logs });

      return data;
    }
  };
};

const val = ({ info }) => {
  const { U_TS_MOD_ID, U_TS_DESC, U_TS_STATUS, U_TS_MODIFYBY } = info;

  if (!U_TS_MOD_ID) {
    const d = {
      msg: "Please enter module of the action."
    };
    throw new Error(JSON.stringify(d));
  }

  if (!U_TS_DESC) {
    const d = {
      msg: "Please enter action name."
    };
    throw new Error(JSON.stringify(d));
  }
  if (!U_TS_MODIFYBY) {
    const d = {
      msg: "Please enter who modified the action."
    };
    throw new Error(JSON.stringify(d));
  }
  if (!U_TS_STATUS) {
    const d = {
      msg: "Please enter status."
    };
    throw new Error(JSON.stringify(d));
  }
};

module.exports = updateActions;
