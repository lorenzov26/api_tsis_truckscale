const actionsSelectAll = ({ actionsDb, decrypt, actions, validateAccessRights }) => {
  return async function selects(info) {
 
    const mode = info.mode;

    if(!info.modules){
      throw new Error (`Access denied`)
    }

    const allowed = await validateAccessRights(info.modules,'admin','view actions')


    if(!allowed){
      throw new Error (`Access denied`)
    }

    if(mode == 1){

      
      delete info.modules
      delete info.source;
      delete info.mode;

      const res = await actions.actionsSelectAll({info});

     
      return res;


    } else {
    const result = await actionsDb.selectAllActions();
    const modules = result.rows;

    let data = []; //declare empty array

    for await (let i of modules) {
      // created by
      const created_by = [
        {
          firstname: decrypt(i.create_fn),
          middlename: i.create_mn ? decrypt(i.create_mn) : i.create_mn,
          lastname: decrypt(i.create_ln),
          created_at: i.created_at
        }
      ];

      // modified by
      const modified_by = [
        {
          firstname: i.modify_fn ? decrypt(i.modify_fn) : i.modify_fn,
          middlename: i.modify_mn ? decrypt(i.modify_mn) : i.modify_mn,
          lastname: i.modify_ln ? decrypt(i.modify_ln) : i.modify_ln,
          updated_at: i.updated_at
        }
      ];

      // module it belongs
      const module = [
        {
          modules_id: i.modules_id,
          descriptions: i.descriptions,
          modules_status: i.modules_status
        }
      ];
      data.push({
        id: i.actions_id,
        description: i.description,
        status: i.status,
        module,
        created_by,
        modified_by
      });
    }

    return data;
  }
}
};

module.exports = actionsSelectAll;
