const importSuppliers = ({
    suppliersDb, parser, moment
    }) => {
      return async function selects(info) {

        const d = new Date();
        const dateToday = moment(d).format("YYYY-MM-DD HH:mm:ss");

        const doc = parser.parseXls2Json(`C:\\truckscale offline mode\\api_tsis\\master_data\\supplier_final.xlsx`); 

        const excelData = doc[0]


        const finalData = []


        for(let i = 0; i<excelData.length; i++){
          
          const exists = await suppliersDb.selectOneSupplierByName({info: {
            supplier: excelData[i].BP_Name
          }})
         
          
          if(exists.rowCount == 0){
    
            const insertSupplier = await suppliersDb.insertSupplierWithAddress({
              info: {
                supplier: excelData[i].BP_Name,
                address:  excelData[i].Complete_Address,
                dateToday: dateToday,
                createdby: 1
              }
            })
          }
        }


        return excelData
      };
    };
    
module.exports = importSuppliers;
    