const { makeSupplier } = require("../../entities/suppliers/app"); // entity
const suppliersDb = require("../../data-access/db-layer/suppliers/app"); //db
const { insertActivityLogss } = require("../users/app"); // logs

const parser = require('simple-excel-to-json')

const moment = require("moment")

//#######################
const addNewSupplier = require("./supplier-add");
const suppliersSelectAll = require("./supplier-select-all");
const suppliersSelectOne = require("./supplier-select-one");
const updateSuppliers = require("./supplier-update");
const importSuppliers = require("./import-suppliers")
//#######################

const uc_importSuppliers = importSuppliers({  suppliersDb, parser, moment})

const addNewSuppliers = addNewSupplier({
  suppliersDb,
  makeSupplier,
  insertActivityLogss
});
const suppliersSelectAlls = suppliersSelectAll({ suppliersDb });
const suppliersSelectOnes = suppliersSelectOne({ suppliersDb });
const updateSupplierss = updateSuppliers({
  suppliersDb,
  makeSupplier,
  insertActivityLogss
});

const services = Object.freeze({
  addNewSuppliers,
  suppliersSelectAlls,
  suppliersSelectOnes,
  updateSupplierss,
  uc_importSuppliers
});

module.exports = services;
module.exports = {
  addNewSuppliers,
  suppliersSelectAlls,
  suppliersSelectOnes,
  updateSupplierss,
  uc_importSuppliers
};
