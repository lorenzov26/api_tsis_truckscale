const suppliersSelectAll = ({ suppliersDb }) => {
  return async function selects(info) {
    if(info.mode == 1){
      return true
    }else {

      const suppliers = await suppliersDb.selectAllSuppliers({})

      const view = suppliers.rows

      return view


    }
  };
};

module.exports = suppliersSelectAll;
