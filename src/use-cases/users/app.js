const dotenv = require("dotenv");
const { identifyDb } = require("../../db/app");
const { dbs } = require("../../data-access/db-layer/app");
const { users } = require("../../data-access/sl-layer/users/app");
const { objectLowerCaser } = require("../../lowerCaser/app");
// ##########

const parser = require('simple-excel-to-json')

const moment = require("moment")

const {
  makeUser,
  userLoginEntitys,
  e_updateUser
} = require("../../entities/users/app"); // entity
const usersDb = require("../../data-access/db-layer/users/app"); //db
const transactionTypeDb = require("../../data-access/db-layer/transaction-type/app"); //db
const { decrypt, encrypt } = require("../../../crypting/app");
const { makeTokens } = require("../../token/app");
const { returnIpAddress, hostName } = require("../../entities/truckscale/app"); // entity

const { validateAccessRights } = require("../../validator/app"); //validator

const { accessRightsSelectAllOnRoles } = require("../access_rights/app"); // query for access rights during login
//####################
const updatePrintSettings = require("./print-settings-update")
const addNewUser = require("./users-add");
const usersSelectAll = require("./users-select-all");
const usersSelectOne = require("./users-select-one");
const loginUser = require("./users-login");
const updateUser = require("./users-update");
const insertActivityLogs = require("./activity-logs-insert");
const resetPassword = require("./users-reset-password");
const selectActivityLogs = require("./activity-logs-select");
const employeesInfoSelectAll = require("./employeesInfo-select-all");
const loginSapUser = require("./users-sap-login");
const checkPin = require("./check-pin");
const resetCodeAndPw = require("./reset-password-code");
const importUsers = require("./import-users")
const selectAllPrintLogs = require("./print-logs-select")

//####################
const insertActivityLogss = insertActivityLogs({ usersDb }); // function to insert into activity logs
const resetCodeAndPws = resetCodeAndPw({ users, usersDb, encrypt });
const loginSapUsers = loginSapUser({ users });
const u_selectAllPrintLogs = selectAllPrintLogs({ usersDb })
const u_updatePrintSettings = updatePrintSettings ({ transactionTypeDb, usersDb, moment })

const uc_checkPin = checkPin({ users, usersDb });

const uc_importUsers = importUsers({usersDb , parser, moment, encrypt})

const resetPasswords = resetPassword({
  usersDb,
  encrypt,
  insertActivityLogss,
  users
});

const uc_employeesInfoSelectAll = employeesInfoSelectAll({
  users,
  validateAccessRights
});

const addNewUsers = addNewUser({
  usersDb,
  makeUser,
  insertActivityLogss,
  decrypt,
  users,
  objectLowerCaser,
  validateAccessRights,
  encrypt,
  moment
});
const usersSelectAlls = usersSelectAll({
  usersDb,
  decrypt,
  users,
  validateAccessRights
});
const usersSelectOnes = usersSelectOne({
  usersDb,
  decrypt,
  users,
  validateAccessRights
});
const loginUsers = loginUser({
  usersDb,
  makeTokens,
  accessRightsSelectAllOnRoles,
  userLoginEntitys,
  returnIpAddress,
  hostName,
  decrypt,
  encrypt,
  insertActivityLogss,
  dotenv,
  identifyDb,
  dbs,
  users
});
const uc_updateUser = updateUser({
  usersDb,
  e_updateUser,
  insertActivityLogss,
  decrypt,
  users,
  objectLowerCaser,
  validateAccessRights,
  moment
});
const selectActivityLogss = selectActivityLogs({ usersDb, decrypt });

// ##############
const services = Object.freeze({
  addNewUsers,
  usersSelectAlls,
  usersSelectOnes,
  loginUsers,
  uc_updateUser,
  insertActivityLogss,
  resetPasswords,
  selectActivityLogss,
  uc_employeesInfoSelectAll,
  loginSapUsers,
  uc_checkPin,
  resetCodeAndPws,
  uc_importUsers,
  u_selectAllPrintLogs,
  u_updatePrintSettings
});

module.exports = services;
module.exports = {
  addNewUsers,
  usersSelectAlls,
  usersSelectOnes,
  loginUsers,
  uc_updateUser,
  insertActivityLogss,
  resetPasswords,
  selectActivityLogss,
  uc_employeesInfoSelectAll,
  loginSapUsers,
  uc_checkPin,
  resetCodeAndPws,
  uc_importUsers,
  u_selectAllPrintLogs,
  u_updatePrintSettings
};
