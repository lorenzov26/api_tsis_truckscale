const resetPassword = ({ usersDb, encrypt, insertActivityLogss, users }) => {
  return async function put({ id, ...info }) {
    const mode = info.mode;

    if (!info.password) {
      throw new Error(`Please enter password`);
    }
    if (!info.repassword) {
      throw new Error(`Please re-enter password`);
    }
    if (info.password !== info.repassword) {
      throw new Error(`Password doesn't match, please try again..`);
    }

    if (!info.U_TS_PIN) {
      throw new Error(`Please input pin number`)
    }



    const password = encrypt(info.password)
    const pin = info.U_TS_PIN
    await usersDb.resetPasswords({ id, password, pin });

    const data = { msg: "Updated password successfully." };
    return data;


  };
};

module.exports = resetPassword;
