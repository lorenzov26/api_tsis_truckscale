const selectAllPrintLogs = ({ usersDb, decrypt }) => {
    return async function selects(dateRange) {

      const logs = await usersDb.selectAllPrintLogs(dateRange)

      const view = logs.rows
  
      return view

    };
  };
  
  module.exports = selectAllPrintLogs;
  