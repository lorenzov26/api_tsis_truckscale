const resetCodeAndPw = ({ users, usersDb, encrypt }) => {
  return async function put({ id, ...info }) {
    const mode = info.mode;

    info.id = id
    info.employeeId = await encrypt(info.employeeId)

    const resetAccount = usersDb.resetLoginCountAndPassword({ info })

    return true


  };
};

module.exports = resetCodeAndPw;
