const insertActivityLogs = ({ usersDb }) => {
  return async function posts(info) {
    const data = info.logs;

    await usersDb.insertActivityLogs({ data });
  };
};

module.exports = insertActivityLogs;
