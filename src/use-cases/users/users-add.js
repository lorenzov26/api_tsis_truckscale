const addNewUser = ({
  usersDb,
  makeUser,
  insertActivityLogss,
  decrypt,
  users,
  objectLowerCaser,
  validateAccessRights,
  encrypt,
  moment
}) => {
  return async function posts(info) {
    const mode = info.mode;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    // const allowed = await validateAccessRights(
    //   info.modules,
    //   "admin",
    //   "add user"
    // );

    // if (!allowed) {
    //   throw new Error(`Access denied`);
    // }

    if (!info.employee_id) {
      throw new Error(`Please input employee ID`)
    }

    if (!info.first_name) {
      throw new Error(`Please input first name`)
    }

    if (!info.last_name) {
      throw new Error(`Please input last name`)
    }

    if (!info.role) {
      throw new Error(`Please input role`)
    }

    if (!info.created_by) {
      throw new Error(`Please input created by`)
    }

    if(!validateEmail(info.email) && info.email){
      throw new Error(`Invalid email address format`)
    }
 

    const d = new Date();
    info.dateToday = moment(d).format("YYYY-MM-DD HH:mm:ss");

    info.password = await encrypt(info.employee_id)

    info.login_count = 0

    const userExists = await usersDb.selectUserByFirstnameAndLastname({ info })


    if (userExists.rows.length > 0) {
      throw new Error(`Employee already exists`)
    }

    const addUser = await usersDb.addUser({ info })

    const count = addUser.rowCount;

    const data = {
      msg: `Inserted successfully ${count} employee.`
    };

    return data

  };
};

function validateEmail(email) {
  var re = /\S+@\S+\.\S+/;
  return re.test(email);
}

module.exports = addNewUser;
