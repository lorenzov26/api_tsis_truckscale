const selectActivityLogs = ({ usersDb, decrypt }) => {
  return async function selects(dateRange) {

    const logs = await usersDb.selectAllActivityLogs(dateRange)

    const view = logs.rows

    return view
  };
};

module.exports = selectActivityLogs;
