
  const updatePrintSettings = ({
    transactionTypeDb,
    usersDb,
    moment
  }) => {
    return async function post(info) {
      const mode = info.mode;
  
      if (!info.modules) {
        throw new Error(`Access denied`);
      }

      const settingsToUpdate = []

      const transactionTypesDetails = await transactionTypeDb.selectAllTransactionType()

      const transactionTypes = transactionTypesDetails.rows

      const d = new Date();
      info.dateToday = moment(d).format("YYYY-MM-DD HH:mm:ss");

      const settings = info.settings

      for(let i = 0 ; i < settings.length; i++){
     

        const foundTransaction = await transactionTypes.find(transactionType => {
            return transactionType.id == settings[i].id
        })
        
        if(foundTransaction.print_count != settings[i].print_count){
            settingsToUpdate.push(settings[i])
        }


      }


      for(let i = 0 ; i < settingsToUpdate.length; i++){

          const updatePrintCount = await usersDb.updateSetting({info: {
              id: settingsToUpdate[i].id,
              print_count: settingsToUpdate[i].print_count,
              updated_by: info.updated_by,
              dateToday: info.dateToday
          }})

      }
      
      return true

    };
  };
  module.exports = updatePrintSettings;
  