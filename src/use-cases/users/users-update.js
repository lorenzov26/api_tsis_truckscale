const updateUser = ({
  usersDb,
  e_updateUser,
  insertActivityLogss,
  decrypt,
  users,
  objectLowerCaser,
  validateAccessRights,
  moment
}) => {
  return async function put({ id, ...info }) {
    const mode = info.mode;
    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    // const allowed = await validateAccessRights(
    //   info.modules,
    //   "admin",
    //   "edit user"
    // );

    // if (!allowed) {
    //   throw new Error(`Access denied`);
    // }

    if (!info.employee_id) {
      throw new Error(`Please input employee ID`)
    }

    if (!info.first_name) {
      throw new Error(`Please input first name`)
    }

    if (!info.last_name) {
      throw new Error(`Please input last name`)
    }

    if (!info.role) {
      throw new Error(`Please input role`)
    }

    if (!info.updated_by) {
      throw new Error(`Please input updated by`)
    }

    if (!validateEmail(info.email) && info.email) {
      throw new Error(`Invalid email address format`)
    }


    const d = new Date();
    info.dateToday = moment(d).format("YYYY-MM-DD HH:mm:ss");

    const updateUser = await usersDb.updateUser({ info })

    const count = updateUser.rowCount;

    const data = {
      msg: `Updated successfully ${count} employee.`
    };

    return data
  };
};


function validateEmail(email) {
  var re = /\S+@\S+\.\S+/;
  return re.test(email);
}

module.exports = updateUser;
