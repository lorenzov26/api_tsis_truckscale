const importUsers = ({
  usersDb, parser, moment, encrypt
    }) => {
      return async function selects(info) {

        const d = new Date();
        const dateToday = moment(d).format("YYYY-MM-DD HH:mm:ss");

        const doc = parser.parseXls2Json(`C:\\truckscale offline mode\\api_tsis\\master_data\\users_final.xlsx`); 

        const excelData = doc[0]


        for(let i = 0; i<excelData.length; i++){    
            const insertUser = await usersDb.addUser({
              info: {
                employee_id: excelData[i].id_number,
                first_name:  excelData[i].first_name,
                last_name: excelData[i].last_name,
                password: encrypt(excelData[i].id_number.toString()),
                login_count: 0
              }
            }) 
        }


       
        return true
      };
    };
    
module.exports = importUsers;
    