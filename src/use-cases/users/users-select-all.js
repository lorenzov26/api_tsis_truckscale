const usersSelectAll = ({ usersDb, decrypt, users, validateAccessRights }) => {
  return async function selects(info) {

    const mode = info.mode

    if (!info.modules) {
      throw new Error(`Access denied`)
    }

    // const allowed = await validateAccessRights(info.modules, 'admin', 'view users')


    // if (!allowed) {
    //   throw new Error(`Access denied`)
    // }


    const employees = await usersDb.selectAllUsers({})

    const view = employees.rows

    return view;

  };
};

module.exports = usersSelectAll;
