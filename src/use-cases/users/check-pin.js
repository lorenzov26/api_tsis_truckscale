const checkPin = ({ users, usersDb }) => {
  return async function selects(info) {
    const mode = info.mode;


    if (isNaN(info.pin)) {
      throw new Error(`Only number allowed.`);
    }

    const exist = await usersDb.selectUserByPin({ info })

    if (exist.rows.length === 0) {
      throw new Error(`Invalid pin`);
    }

    return true



  };
};

module.exports = checkPin;
