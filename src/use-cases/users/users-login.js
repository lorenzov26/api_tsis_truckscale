const loginUser = ({
  usersDb,
  makeTokens,
  accessRightsSelectAllOnRoles,
  userLoginEntitys,
  returnIpAddress,
  hostName,
  decrypt,
  encrypt,
  insertActivityLogss,
  dotenv,
  identifyDb,
  dbs,
  users
}) => {
  return async function posts(info) {
    // in the try; login in the service layer; if no network; login in local
    // dotenv.config();
    // await delay(100);
    // // close connection
    // await dbs();
    // // set db to connect
    // const res = await identifyDb({ env: process.env.DM_ENV });
    // const db = await res();
    // await delay(500);
    // // connect to local db; only when empty
    // if (!process.env.PGDATABASE) {
    //   process.env.PGDATABASE = db.DB;
    // }
    // // connect to SL DB; only when empty
    // if (!process.env.DB) {
    //   process.env.DB = db.SL;
    // }
    // // open connection
    // await dbs();

    // // #########
    // // determine mode; offline or online
    // // 1 is online; 0 is offline
    if (!info.mode) {
      throw new Error(`Please enter mode of access.`);
    }
    const mode = info.mode;


    info.password = await encrypt(info.password)

    const checkEmployeeAccount = await usersDb.selectEmployeeByIdAndPassword({ info })

    if (checkEmployeeAccount.rowCount > 0) {

      info.employeeDbId = checkEmployeeAccount.rows[0].id

      const token = await makeTokens({
        employee_id: info.employee_id,
        password: info.password
      });

      info.token = token

      const updateTokenAndLoginCount = await usersDb.updateEmployeeTokenAndLoginCount({ info });

      const ip = await returnIpAddress();
      const device_name = await hostName();

      const truckscaleDetails = await usersDb.selectTruckScaleByIpandDeviceName({ ip, device_name });
      const truckscale = truckscaleDetails.rows[0];

      const modules = await accessRightsSelectAllOnRoles(checkEmployeeAccount.rows[0].role_id)

      if (modules.length == 0) {
        throw new Error(`You dont have access to the system`)
      }

      const data = {
        id: checkEmployeeAccount.rows[0].id,
        role: checkEmployeeAccount.rows[0].user_role,
        count: checkEmployeeAccount.rows[0].login_count,
        msg: "Login successfully.",
        db: `DB layer -> connected to ${process.env.PGDATABASE}.`,
        token,
        modules,
        truckscale: truckscale ? truckscale : null,
        name: `${checkEmployeeAccount.rows[0].last_name}, ${checkEmployeeAccount.rows[0].first_name}`
      };


      return data

    } else {
      throw new Error("Invalid account, please try again.");
    }
  };
};

const delay = s => {
  return new Promise(resolve => setTimeout(resolve, s));
};

const val = ({ info }) => {
  const { employee_id, password } = info;

  if (!employee_id) {
    const d = {
      msg: "Please enter Employee ID."
    };
    throw new Error(JSON.stringify(d));
  }

  if (!password) {
    const d = {
      msg: "Please enter password"
    };
    throw new Error(JSON.stringify(d));
  }
};

module.exports = loginUser;
