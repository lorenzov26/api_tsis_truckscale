const updateTruckType = ({
  truckTypesDb,
  e_updateTruckType,
  insertActivityLogss,
  truckTypes,
  objectLowerCaser,
  validateAccessRights,
  moment
}) => {
  return async function put({ id, ...info } = {}) {

    const mode = info.mode

    if (!info.modules) {
      throw new Error(`Access denied`)
    }

    // const allowed = await validateAccessRights(info.modules,'truck and driver','edit truck type')


    // if(!allowed){
    //   throw new Error (`Access denied`)
    // }


    if (!info.truck_type) {
      throw new Error(`Please input truck type`)
    }

    if (!info.truck_model) {
      throw new Error(`Please input truck model`)
    }

    if (!info.truck_size) {
      throw new Error(`Please input truck size`)
    }

    if (!info.updated_by) {
      throw new Error(`Please input updated by`)
    }

    const d = new Date();
    info.dateToday = moment(d).format("YYYY-MM-DD HH:mm:ss");

    info.id = id

    const truckTypeExists = await truckTypesDb.selectOneTruckTypeByTypeAndModelForUpdate({ info })

    if (truckTypeExists.rows.length > 0) {
      throw new Error(`Truck type already exists`)
    }

    const updateTruckType = await truckTypesDb.updateTruckType({ info })

    const count = updateTruckType.rowCount;

    const data = {
      msg: `Updated successfully ${count} truck type.`
    };

    return data


  };
};


module.exports = updateTruckType;
