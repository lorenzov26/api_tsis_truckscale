const selectAllTruckTypes = ({
  truckTypesDb,
  truckTypes,
  validateAccessRights
}) => {
  return async function get(info) {
    const mode = info.mode;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    // const allowed = await validateAccessRights(
    //   info.modules,
    //   "truck and driver",
    //   "view truck types"
    // );

    // if (!allowed) {
    //   throw new Error(`Access denied`);
    // }


    const truckTypes = await truckTypesDb.selectAllTruckTypes({})

    const view = truckTypes.rows


    return view

  
};
};

module.exports = selectAllTruckTypes;
