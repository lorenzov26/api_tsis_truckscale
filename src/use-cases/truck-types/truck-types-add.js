const addTruckType = ({
  truckTypesDb,
  e_addTruckType,
  insertActivityLogss,
  truckTypes,
  objectLowerCaser,
  validateAccessRights,
  moment
}) => {
  return async function post(info) {


    const mode = info.mode


    if (!info.modules) {
      throw new Error(`Access denied`)
    }

    // const allowed = await validateAccessRights(info.modules, 'truck and driver', 'add truck type')


    // if (!allowed) {
    //   throw new Error(`Access denied`)
    // }


    if (!info.truck_model) {
      throw new Error(`Please input truck model`)
    }

    if (!info.truck_type) {
      throw new Error(`Please input truck type`)
    }


    if (!info.truck_size) {
      throw new Error(`Please input truck size`)
    }

    if (!info.created_by) {
      throw new Error(`Please input created by`)
    }

    const d = new Date();
    info.dateToday = moment(d).format("YYYY-MM-DD HH:mm:ss");

    const truckTypeExists = await truckTypesDb.selectOneTruckTypeByTypeAndModelForAdd({ info })

    if (truckTypeExists.rows.length > 0) {
      throw new Error(`Truck type already exists`)
    }


    const insertTruckType = await truckTypesDb.insertTruckType({ info })

    const count = insertTruckType.rowCount;

    const data = {
      msg: `Inserted successfully ${count} truck type.`
    };

    return data


  };
};


module.exports = addTruckType;
