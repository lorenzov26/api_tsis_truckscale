const editReport = ({
  truckInfosDb, transactionDb, driversDb, itemsDb, suppliersDb, validateAccessRights, moment, reportsDb
}) => {
  return async function put({ id, ...info } = {}) {

    if (!info.modules) {
      throw new Error(`Access denied`)
    }

    // const allowed = await validateAccessRights(info.modules, 'report', 'edit report')


    // if (!allowed) {
    //   throw new Error(`Access denied`)
    // }

    if (!id) {
      throw new Error(`Please input transaction ID`)
    }

    if (!info.truck_info_id) {
      throw new Error(`Please input truck`)
    }

    if (!info.driver_id) {
      throw new Error(`Please input driver`)
    }

    if (!info.supplier) {
      throw new Error(`Please input supplier`)
    }

    if (!info.number_of_bags) {
      throw new Error(`Please input number of bags`)
    }

    if (!info.item) {
      throw new Error(`Please input item`)
    }

    if (!info.supplier_address) {
      throw new Error(`Please input supplier address`)
    }

    if (!info.uom_id) {
      throw new Error(`Please input unit of measure`)
    }


    info.id = id
    info.createdby = info.updated_by

    const d = new Date();
    info.dateToday = moment(d).format("YYYY-MM-DD HH:mm:ss");

    const supplierExist = await suppliersDb.selectOneSupplierByName({ info })
    if (supplierExist.rowCount === 0) {
      const insertSupplier = await suppliersDb.insertSupplier({ info })
      info.supplier = insertSupplier.rows[0].id
    } else {
      info.supplier = supplierExist.rows[0].id
    }

    const itemExist = await itemsDb.selectOneItemByName({ info })
    if (itemExist.rowCount === 0) {
      const insertItem = await itemsDb.insertItem({ info })
      info.item = insertItem.rows[0].id
    } else {
      info.item = itemExist.rows[0].id
    }

    const truckInfoExist = await truckInfosDb.selectTruckInfoByPlateNumber({ info })

    if (truckInfoExist.rowCount === 0) {
      const insertTruckInfo = await truckInfosDb.insertTruckInfosForAutoAdd({ info })
      info.truck_info_id = insertTruckInfo.rows[0].id
    } else {
      info.truck_info_id = truckInfoExist.rows[0].id
    }

    const splitName = info.driver_id.split(", ")

    if (splitName.length !== 2) {
      throw new Error(`Invalid name format, must be 'lastname, firstname'`)
    }

    info.firstname = splitName[1]
    info.lastname = splitName[0]

    const driverExist = await driversDb.selectOneDriverByNameForAdd({ info })

    if (driverExist.rowCount === 0) {
      const insertDriver = await driversDb.insertDriverForAutoAdd({ info })
      info.driver_id = insertDriver.rows[0].id
    } else {
      info.driver_id = driverExist.rows[0].id
    }

    const updateReport = await reportsDb.updateReport({ info })

    // if(updateReport.rowCount > 0){

    //   const updateOutboundRemarks = await transactionDb.updateOutboundTransactionRemarksByTransactionId({info})

    // }

    return true
  };
};

module.exports = editReport;
