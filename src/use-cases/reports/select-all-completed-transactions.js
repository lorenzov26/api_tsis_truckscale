const completedTransactionSelectAll = ({
  reportsDb,
  decrypt,
  transactions,
  transactionTypes,
  weightTypes,
  drivers,
  truckInfos,
  validateAccessRights,
  users
}) => {
  return async function selects(info) {
    const mode = info.mode;



    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    // const allowed = await validateAccessRights(
    //   info.modules,
    //   "report",
    //   "view reports"
    // );

    // if (!allowed) {
    //   throw new Error(`Access denied`);
    // }


    const reports = await reportsDb.selectAllReports({ info })

    const view = reports.rows

    return view


  };
};

// function to remove duplicate elements in array
// arr = is the array
// comp = is the component you want it to filter with
const filterArrayNoDuplicates = (arr, comp) => {
  const unique = arr
    .map(e => e[comp])

    // store the keys of the unique objects
    .map((e, i, final) => final.indexOf(e) === i && i)

    // eliminate the dead keys & store unique objects
    .filter(e => arr[e])
    .map(e => arr[e]);

  return unique;
};

module.exports = completedTransactionSelectAll;
