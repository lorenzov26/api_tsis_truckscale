const reportsDb = require("../../data-access/db-layer/reports/app"); //db
const suppliersDb = require("../../data-access/db-layer/suppliers/app")
const itemsDb = require("../../data-access/db-layer/items/app")
const driversDb = require("../../data-access/db-layer/drivers/app"); //db
const transactionDb = require("../../data-access/db-layer/transactions/app"); //db
const truckInfosDb = require("../../data-access/db-layer/truck-infos/app")

const { decrypt } = require("../../../crypting/app"); //decrypt

const moment = require("moment")

const { transactions } = require("../../data-access/sl-layer/transactions/app");
const { transactionTypes } = require("../../data-access/sl-layer/transaction-type/app")
const { weightTypes } = require("../../data-access/sl-layer/weight-types/app")
const { drivers } = require("../../data-access/sl-layer/drivers/app");
const { truckInfos } = require("../../data-access/sl-layer/truck-infos/app");
const { users } = require("../../data-access/sl-layer/users/app");

const { validateAccessRights } = require("../../validator/app"); //validator

//####################
const completedTransactionSelectAll = require("./select-all-completed-transactions");
const editReport = require("./edit-report")
//####################
const completedTransactionSelectAlls = completedTransactionSelectAll({
  reportsDb,
  decrypt,
  transactions,
  transactionTypes,
  weightTypes,
  drivers,
  truckInfos,
  users,
  validateAccessRights
});

const u_editReport = editReport({ truckInfosDb, transactionDb, driversDb, itemsDb, suppliersDb, validateAccessRights, moment, reportsDb })

const services = Object.freeze({ completedTransactionSelectAlls, u_editReport });

module.exports = services;
module.exports = { completedTransactionSelectAlls, u_editReport };
