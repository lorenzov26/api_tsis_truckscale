const seedDefault = ({ seedersDb, encrypt }) => {
  return async function post() {
    //default admin account
    const adminAccount = {
      id: 1,
      employee_id: encrypt("154151000"),
      email: encrypt("admin@truckscale.sap"),
      firstname: encrypt("Adu"),
      lastname: encrypt("Ministrator"),
      password: encrypt("password"),
      role_id: 1,
      created_at: new Date().toISOString()
    };

    const controllerAccount = {
      id: 2,
      employee_id: encrypt("154151001"),
      email: encrypt("controller@truckscale.sap"),
      firstname: encrypt("Con"),
      lastname: encrypt("Troller"),
      password: encrypt("password"),
      role_id: 2,
      created_at: new Date().toISOString()
    };

    //default admin role
    const adminRole = {
      id: 1,
      name: "admin",
      status: "active",
      created_at: new Date().toISOString(),
      created_by: 1
    };

    const controllerRole = {
      id: 2,
      name: "controller",
      status: "active",
      created_at: new Date().toISOString(),
      created_by: 1
    };

    const documentController = {
      id: 3,
      name: "document controller",
      status: "active",
      created_at: new Date().toISOString(),
      created_by: 1
    };

    //default modules
    const modules = [
      {
        id: 1,
        description: "Admin",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 2,
        description: "Transaction",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 3,
        description: "Truck and Driver",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 4,
        description: "Report",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      }
    ];

    //default actions
    const actions = [
      {
        id: 1,
        module_id: 1,
        description: "View Modules",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 2,
        module_id: 1,
        description: "Add Module",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 3,
        module_id: 1,
        description: "Edit Module",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 4,
        module_id: 1,
        description: "View Actions",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 5,
        module_id: 1,
        description: "Add Action",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 6,
        module_id: 1,
        description: "Edit Action",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 7,
        module_id: 1,
        description: "View Access Rights",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 8,
        module_id: 1,
        description: "Edit Access Rights",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 9,
        module_id: 1,
        description: "View Users",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 10,
        module_id: 1,
        description: "Add User",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 11,
        module_id: 1,
        description: "Edit User",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 12,
        module_id: 1,
        description: "View Truckscales",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 13,
        module_id: 1,
        description: "Add Truckscale",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 14,
        module_id: 1,
        description: "Edit Truckscale",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 15,
        module_id: 2,
        description: "View Transactions",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 16,
        module_id: 2,
        description: "Edit Transaction",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 17,
        module_id: 3,
        description: "View Truck Types",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 18,
        module_id: 3,
        description: "Add Truck Type",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 19,
        module_id: 3,
        description: "Edit Truck Type",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 20,
        module_id: 3,
        description: "View Trucks",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 21,
        module_id: 3,
        description: "Add Truck",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 22,
        module_id: 3,
        description: "Edit Truck",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 23,
        module_id: 3,
        description: "View Drivers",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 24,
        module_id: 3,
        description: "Add Driver",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 25,
        module_id: 3,
        description: "Edit Driver",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 26,
        module_id: 4,
        description: "View Reports",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 27,
        module_id: 1,
        description: "View Roles",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 28,
        module_id: 1,
        description: "Add Role",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      },
      {
        id: 29,
        module_id: 1,
        description: "Edit Role",
        status: "active",
        created_at: new Date().toISOString(),
        created_by: 1
      }
    ];

    const access_rights = [
      //admin
      {
        id: 1,
        actions_id: 1,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      {
        id: 2,
        actions_id: 2,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      {
        id: 3,
        actions_id: 3,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      {
        id: 4,
        actions_id: 4,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      {
        id: 5,
        actions_id: 5,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      {
        id: 6,
        actions_id: 6,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      {
        id: 7,
        actions_id: 7,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      {
        id: 8,
        actions_id: 8,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      {
        id: 9,
        actions_id: 9,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      {
        id: 10,
        actions_id: 10,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      {
        id: 11,
        actions_id: 11,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      {
        id: 12,
        actions_id: 12,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      {
        id: 13,
        actions_id: 13,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      {
        id: 14,
        actions_id: 14,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      {
        id: 15,
        actions_id: 27,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      {
        id: 16,
        actions_id: 28,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      {
        id: 17,
        actions_id: 29,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      {
        id: 18,
        actions_id: 15,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      {
        id: 19,
        actions_id: 16,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      {
        id: 20,
        actions_id: 17,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      {
        id: 21,
        actions_id: 18,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      {
        id: 22,
        actions_id: 19,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      {
        id: 23,
        actions_id: 20,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      {
        id: 24,
        actions_id: 21,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      {
        id: 25,
        actions_id: 22,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      {
        id: 26,
        actions_id: 23,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      {
        id: 27,
        actions_id: 24,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      {
        id: 28,
        actions_id: 25,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      {
        id: 29,
        actions_id: 26,
        roles_id: 1,
        created_at: new Date().toISOString()
      },
      //controller
      {
        id: 30,
        actions_id: 15,
        roles_id: 2,
        created_at: new Date().toISOString()
      },
      {
        id: 31,
        actions_id: 16,
        roles_id: 2,
        created_at: new Date().toISOString()
      },
      {
        id: 32,
        actions_id: 17,
        roles_id: 2,
        created_at: new Date().toISOString()
      },
      {
        id: 33,
        actions_id: 18,
        roles_id: 2,
        created_at: new Date().toISOString()
      },
      {
        id: 34,
        actions_id: 19,
        roles_id: 2,
        created_at: new Date().toISOString()
      },
      {
        id: 35,
        actions_id: 20,
        roles_id: 2,
        created_at: new Date().toISOString()
      },
      {
        id: 36,
        actions_id: 21,
        roles_id: 2,
        created_at: new Date().toISOString()
      },
      {
        id: 37,
        actions_id: 22,
        roles_id: 2,
        created_at: new Date().toISOString()
      },
      {
        id: 38,
        actions_id: 23,
        roles_id: 2,
        created_at: new Date().toISOString()
      },
      {
        id: 39,
        actions_id: 24,
        roles_id: 2,
        created_at: new Date().toISOString()
      },
      {
        id: 40,
        actions_id: 25,
        roles_id: 2,
        created_at: new Date().toISOString()
      },
      {
        id: 41,
        actions_id: 26,
        roles_id: 2,
        created_at: new Date().toISOString()
      },
      // document controller
      {
        id: 42,
        actions_id: 15,
        roles_id: 3,
        created_at: new Date().toISOString()
      },
      {
        id: 43,
        actions_id: 26,
        roles_id: 3,
        created_at: new Date().toISOString()
      }
    ];

    // default weight type
    const weightType = [
      {
        name: "Tare",
        created_at: new Date().toISOString()
      },
      {
        name: "Gross",
        created_at: new Date().toISOString()
      }
    ];

    // seed transaction type
    const transactionType = [
      {
        name: "Delivery",
        created_at: new Date().toISOString()
      },
      {
        name: "Withdrawal",
        created_at: new Date().toISOString()
      },
      {
        name: "Others",
        created_at: new Date().toISOString()
      }
    ];

    await seedersDb.cascadeTables();

    await seedersDb.insertRole(adminRole);

    await seedersDb.insertRole(controllerRole);

    await seedersDb.insertRole(documentController);

    await seedersDb.insertUser(adminAccount);

    await seedersDb.insertUser(controllerAccount);

    for (let i = 0; i < modules.length; i++) {
      await seedersDb.insertModule(modules[i]);
    }

    for (let i = 0; i < actions.length; i++) {
      await seedersDb.insertAction(actions[i]);
    }

    for (let i = 0; i < access_rights.length; i++) {
      await seedersDb.insertAccessRight(access_rights[i]);
    }

    // seed weight type
    for (let i = 0; i < weightType.length; i++) {
      const e = weightType[i];
      await seedersDb.insertWeightType(e);
    }

    // seed transaction type
    for (let i = 0; i < transactionType.length; i++) {
      const e = transactionType[i];
      await seedersDb.insertTransactionType(e);
    }

    // update created by
    // 1st param is user id, 2nd param is role id
    await seedersDb.updateRole(1, 1);

    await seedersDb.updateRole(1, 2);

    await seedersDb.resetSequence();

    return true;
  };
};

module.exports = seedDefault;
