const seedersDb = require("../../data-access/db-layer/seeders/app"); //db
const { encrypt } = require("../../../crypting/app"); //encryption

const seedDefault = require("./seed-default");

const uc_seedDefault = seedDefault({ seedersDb, encrypt });

const services = Object.freeze({
  uc_seedDefault
});

module.exports = services;

module.exports = {
  uc_seedDefault
};
