const notif = ({ io, requestDb, moment }) => {

    return function notification() {

        io.on("connect", (socket) => {

            socket.on("get notifs", async (data) => {

                try {

                    const d = new Date();
                    const date = moment(d).format("YYYY-MM-DD");

                    const notifications = await requestDb.getRequestNotifsByReceiverId({
                        info: {
                            id: data.user,
                            date: date
                        }
                    })

                    socket.emit("receive notifs", {
                        notifs: notifications.rows
                    })


                } catch (e) {
                    console.log(e)
                }

            });

            socket.on("notif read", async (data) => {

                try {

                    const updateIsRead = await requestDb.updateNotifIsRead({
                        info: {
                            is_read: true,
                            id: data.id
                        }
                    })


                } catch (e) {
                    console.log(e)
                }

            });

            socket.on("notif read all", async (data) => {

                try {

                    const notifs = data.notifs

                    for (let i = 0; i < notifs.length; i++) {
                        const updateIsRead = await requestDb.updateNotifIsRead({
                            info: {
                                is_read: true,
                                id: notifs[i]
                            }
                        })

                    }
                    
                } catch (e) {
                    console.log(e)
                }

            });




        });

    }

};

module.exports = notif;
