const { io } = require("../app")

const notif = require("./notifs")

const moment = require("moment")

const requestDb = require("../data-access/db-layer/requests/app")

const notifHandler = notif({io, requestDb, moment})

const services = Object.freeze({
    notifHandler
});

module.exports = services;
module.exports = {
    notifHandler
};