require("dotenv").config();
const nodemailer = require("nodemailer");
let transporter = nodemailer.createTransport({
  host: process.env.EMAIL_HOST,
  port: process.env.EMAIL_PORT,
  auth: {
    user: process.env.EMAIL_USER,
    pass: process.env.EMAIL_PASS,
  },
});
async function sendEmail(mailOptions) {
  const result = await new Promise((resolve) => {
    transporter.sendMail(mailOptions, (err, data) => {
      let msg;
      if (err) {
        msg = {
          status: 400,
          data: `Not sent ${err}`,
        };
        resolve(msg);
      }
      msg = {
        status: 200,
        data: "Sent successfully",
      };
      resolve(msg);
    });
  });
  return result;
}
module.exports = { sendEmail };