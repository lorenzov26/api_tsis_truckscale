const axios = require("axios");
// #################
const processSocketData = require("./process-data");
// #################
const socketConnect = async ({ io }) => {
  await processSocketData({ io, axios });
};
// #################

const services = Object.freeze({
  socketConnect
});

module.exports = services;
module.exports = {
  socketConnect
};
