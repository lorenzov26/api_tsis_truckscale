const express = require("express");
const router = express.Router();
const makeExpressCallback = require("../../src/express-callback/app");

const truckTypesRoutes = require("./routes");

const { verifyTokens } = require("../../src/token/app")


const truckTypesEndPoints = truckTypesRoutes({ router, makeExpressCallback, verifyTokens });

const services = Object.freeze({
    truckTypesEndPoints
});

module.exports = services;

module.exports = {
    truckTypesEndPoints
};

module.exports = router;
