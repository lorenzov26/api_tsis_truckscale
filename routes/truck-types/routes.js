const {
    c_addTruckType,
    c_selectAllTruckTypes,
    c_selectOneTruckType,
    c_updateTruckType
  } = require("../../src/controller/truck-types/app");
  
  const truckTypesRoutes = ({ router, makeExpressCallback, verifyTokens }) => {

    router.post("/add", verifyTokens, makeExpressCallback(c_addTruckType))

    router.get("/select", verifyTokens, makeExpressCallback(c_selectAllTruckTypes))

    router.get("/select/:id", verifyTokens, makeExpressCallback(c_selectOneTruckType))

    router.put("/update/:id", verifyTokens, makeExpressCallback(c_updateTruckType))

    return router;
    
  };
  
  module.exports = truckTypesRoutes;
  