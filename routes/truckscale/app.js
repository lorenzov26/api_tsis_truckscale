const express = require("express");
const router = express.Router();
const makeExpressCallback = require("../../src/express-callback/app");

const truckScale = require("./routes");

const { verifyTokens } = require("../../src/token/app")
//#########
const truckScales = truckScale({ router, makeExpressCallback, verifyTokens });

const services = Object.freeze({
  truckScales
});

module.exports = services;

module.exports = {
  truckScales
};

module.exports = router;
