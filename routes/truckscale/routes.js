const {
  truckScaleAddNews,
  selectAllTss,
  selectOneTss,
  truckScaleUpdates
} = require("../../src/controller/truckscale/app");

const truckScale = ({ router, makeExpressCallback, verifyTokens }) => {
  //########################
  // POST REQUESTS
  //########################

  //   add new truck scale
  router.post("/add", verifyTokens, makeExpressCallback(truckScaleAddNews));

  //########################
  // END POST REQUESTS
  //########################

  //########################
  // PUT REQUESTS
  //######################## 

  // update ts
  router.put("/update/:id", verifyTokens, makeExpressCallback(truckScaleUpdates));

  //########################
  // END PUT REQUESTS
  //########################

  //########################
  // GET REQUESTS
  //########################

  // select all
  router.get("/select", verifyTokens, makeExpressCallback(selectAllTss));

  // select one
  router.get("/select/:id", verifyTokens, makeExpressCallback(selectOneTss));

  //########################
  // END GET REQUESTS
  //########################

  return router;
};

module.exports = truckScale;
