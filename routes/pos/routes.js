const {
    c_selectAllPos
  } = require("../../src/controller/pos/app");
  
  const posRoutes = ({ router, makeExpressCallback, verifyTokens }) => {
  
  
    router.get(
      "/select",
      verifyTokens,
      makeExpressCallback(c_selectAllPos)
    );
   
  
   
  
    return router;
  };
  
  module.exports = posRoutes;