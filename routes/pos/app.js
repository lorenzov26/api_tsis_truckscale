
  const express = require("express");
  const router = express.Router();
  const makeExpressCallback = require("../../src/express-callback/app");
  const { verifyTokens } = require("../../src/token/app");
  
  const posRoutes = require("./routes");
  
  const posEndpoints = posRoutes({ router, makeExpressCallback, verifyTokens });
  
  const services = Object.freeze({
    posEndpoints
  });
  
  module.exports = services;
  
  module.exports = {
    posEndpoints
  };
  
  module.exports = router;
  