const {
  modulesAddNews,
  moduleUpdates,
  selectAllModuless,
  selectOneModules,
  c_modulesSelectAllWithActions
} = require("../../src/controller/modules/app");

const modules = ({ router, makeExpressCallback, verifyTokens }) => {
  //########################
  // POST REQUESTS
  //########################

  //   add new module
  router.post("/add", verifyTokens, makeExpressCallback(modulesAddNews));

  //########################
  // END POST REQUESTS
  //########################

  //########################
  // PUT REQUESTS
  //########################

  // update modules
  router.put("/update/:id", verifyTokens, makeExpressCallback(moduleUpdates));

  //########################
  // END PUT REQUESTS
  //########################

  //########################
  // GET REQUESTS
  //########################

  // select all modules
  router.get("/select", verifyTokens, makeExpressCallback(selectAllModuless));

  // select all modules with actions
  router.get("/select-with-actions", verifyTokens, makeExpressCallback(c_modulesSelectAllWithActions));

  // select single module
  router.get(
    "/select/:id",
    verifyTokens,
    makeExpressCallback(selectOneModules)
  );

  //########################
  // END GET REQUESTS
  //########################

  return router;
};

module.exports = modules;
