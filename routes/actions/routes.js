const {
  actionsAddNews,
  actionsUpdates,
  selectAllActionss,
  selectOneActions
} = require("../../src/controller/actions/app");

const actions = ({ router, makeExpressCallback, verifyTokens  }) => {
  //########################
  // POST REQUESTS
  //########################

  //   add new action
  router.post("/add", verifyTokens, makeExpressCallback(actionsAddNews));
  //########################
  // END POST REQUESTS
  //########################

  //########################
  // PUT REQUESTS
  //########################

  // update actions
  router.put("/update/:id", verifyTokens, makeExpressCallback(actionsUpdates));

  //########################
  // END PUT REQUESTS
  //########################

  //########################
  // GET REQUESTS
  //########################

  // select all actions
  router.get("/select", verifyTokens, makeExpressCallback(selectAllActionss));

  // select one action
  router.get("/select/:id", verifyTokens, makeExpressCallback(selectOneActions));

  //########################
  // END GET REQUESTS
  //########################

  return router;
};

module.exports = actions;
