const express = require("express");
const router = express.Router();
const makeExpressCallback = require("../../src/express-callback/app");
const { verifyTokens } = require("../../src/token/app");

const transationType = require("./routes");

const transationTypes = transationType({
  router,
  makeExpressCallback,
  verifyTokens
});

const services = Object.freeze({
  transationTypes
});

module.exports = services;

module.exports = {
  transationTypes
};

module.exports = router;
