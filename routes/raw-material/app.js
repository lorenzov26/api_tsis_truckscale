const express = require("express");
const router = express.Router();
const makeExpressCallback = require("../../src/express-callback/app");
const { verifyTokens } = require("../../src/token/app");

const rawMaterial = require("./routes");

const rawMaterials = rawMaterial({ router, makeExpressCallback, verifyTokens });

const services = Object.freeze({
  rawMaterials
});

module.exports = services;

module.exports = {
  rawMaterials
};

module.exports = router;
