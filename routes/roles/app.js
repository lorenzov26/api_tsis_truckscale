const express = require("express");
const router = express.Router();
const makeExpressCallback = require("../../src/express-callback/app");

const role = require("./routes");

const { verifyTokens } = require("../../src/token/app")
//#########
const roles = role({ router, makeExpressCallback, verifyTokens });

const services = Object.freeze({
  roles
});

module.exports = services;

module.exports = {
  roles
};

module.exports = router;
