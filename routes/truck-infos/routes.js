const {
    c_addTruckInfo,
    c_selectAllTruckInfos,
    c_selectOneTruckInfo,
    c_updateTruckInfo
  } = require("../../src/controller/truck-infos/app");
  
  const truckInfosRoutes = ({ router, makeExpressCallback, verifyTokens }) => {

    router.post("/add", verifyTokens, makeExpressCallback(c_addTruckInfo))

    router.get("/select", verifyTokens, makeExpressCallback(c_selectAllTruckInfos))

    router.get("/select/:id", verifyTokens, makeExpressCallback(c_selectOneTruckInfo))

    router.put("/update/:id", verifyTokens, makeExpressCallback(c_updateTruckInfo))

    return router;
    
  };
  
  module.exports = truckInfosRoutes;
  