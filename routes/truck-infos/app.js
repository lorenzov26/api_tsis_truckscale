const express = require("express");
const router = express.Router();
const makeExpressCallback = require("../../src/express-callback/app");

const truckInfosRoutes = require("./routes");

const { verifyTokens } = require("../../src/token/app")


const truckInfosEndPoints = truckInfosRoutes({ router, makeExpressCallback, verifyTokens });

const services = Object.freeze({
    truckInfosEndPoints
});

module.exports = services;

module.exports = {
    truckInfosEndPoints
};

module.exports = router;
