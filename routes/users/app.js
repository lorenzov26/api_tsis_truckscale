const express = require("express");
const router = express.Router();
const makeExpressCallback = require("../../src/express-callback/app");

const user = require("./routes");

const { verifyTokens } = require("../../src/token/app")
//#########
const users = user({ router, makeExpressCallback, verifyTokens });

const services = Object.freeze({
  users
});

module.exports = services;

module.exports = {
  users
};

module.exports = router;
