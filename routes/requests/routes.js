const {
    c_addRequest,
    c_selectAllRequests,
    c_approveRequest,
    c_disapproveRequest,
    c_validateRequest,
    c_cancelRequest
  } = require("../../src/controller/requests/app");
  
  const request = ({ router, makeExpressCallback, verifyTokens }) => {
    
    router.post("/add", verifyTokens, makeExpressCallback(c_addRequest));

    router.post("/select", verifyTokens, makeExpressCallback(c_selectAllRequests))

    router.put("/approve/:id", verifyTokens, makeExpressCallback(c_approveRequest))

    router.put("/disapprove/:id", verifyTokens, makeExpressCallback(c_disapproveRequest))

    router.put("/validate/:id", verifyTokens, makeExpressCallback(c_validateRequest))

    router.put("/cancel/:id", verifyTokens, makeExpressCallback(c_cancelRequest))
  
    return router;
  };
  
  module.exports = request;
  