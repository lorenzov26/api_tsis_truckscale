const express = require("express");
const router = express.Router();
const makeExpressCallback = require("../../src/express-callback/app");

const request = require("./routes");

const { verifyTokens } = require("../../src/token/app")
//#########
const requests = request({ router, makeExpressCallback, verifyTokens });

const services = Object.freeze({
    requests
});

module.exports = services;

module.exports = {
    requests
};

module.exports = router;
