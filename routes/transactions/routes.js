const {
  transactionAddNews,
  selectAllTransactionss,
  c_addInbound,
  c_addOutbound,
  purchaseOrderFetchs,
  notifForTransmittals,
  c_selectAllWarehouses,
  c_scanTransaction,
  grpoFsqrs,
  transactionUpdates,
  c_editTransaction,
  c_printTransaction,
  c_selectPrintCount,
  c_getLocations,
  c_cancelTransaction
} = require("../../src/controller/transactions/app");

const transaction = ({
  router,
  makeExpressCallback,
  verifyTokens,
  fsqrValidates
}) => {
  // POST

  router.put("/cancel/:id", verifyTokens, makeExpressCallback(c_cancelTransaction));

  router.post("/locations", verifyTokens, makeExpressCallback(c_getLocations))

  router.put("/edit/:id", verifyTokens, makeExpressCallback(c_editTransaction));

  // add
  router.post("/add", verifyTokens, makeExpressCallback(transactionAddNews));

  router.post("/scan", verifyTokens, makeExpressCallback(c_scanTransaction));

  router.post("/print", verifyTokens, makeExpressCallback(c_printTransaction));

  router.post("/selectPrintCount", verifyTokens, makeExpressCallback(c_selectPrintCount))

  // GET

  router.get(
    "/selectAllWarehouses",
    verifyTokens,
    makeExpressCallback(c_selectAllWarehouses)
  );

  // select all and single transaction if has query code in URL
  router.post(
    "/select",
    verifyTokens,
    makeExpressCallback(selectAllTransactionss)
  );

  router.post("/inbound/add", verifyTokens, makeExpressCallback(c_addInbound));
  router.post(
    "/outbound/add",
    verifyTokens,
    makeExpressCallback(c_addOutbound)
  );
  router.post("/fetch-sap", makeExpressCallback(purchaseOrderFetchs));

  router.get(
    "/notif/for-transmittal",
    verifyTokens,
    makeExpressCallback(notifForTransmittals)
  );

  // validate for fsqr only
  router.get("/grpo/:code", fsqrValidates, makeExpressCallback(grpoFsqrs));

  // update status and attached PO; in FSQR app
  router.post(
    "/update/:code",
    fsqrValidates,
    makeExpressCallback(transactionUpdates)
  );

  return router;
};

module.exports = transaction;
