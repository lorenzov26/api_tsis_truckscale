const express = require("express");
const router = express.Router();
const makeExpressCallback = require("../../src/express-callback/app");

const transaction = require("./routes");

const { fsqrValidates } = require("../../src/middlewares/app");

const { verifyTokens } = require("../../src/token/app");
//#########
const transactions = transaction({
  router,
  makeExpressCallback,
  verifyTokens,
  fsqrValidates
});

const services = Object.freeze({
  transactions
});

module.exports = services;

module.exports = {
  transactions
};

module.exports = router;
