const {
  selectAllCompletedTransactions,
  c_editReport
} = require("../../src/controller/reports/app");

const reports = ({ router, makeExpressCallback, verifyTokens }) => {
  router.post(
    "/select",
    verifyTokens,
    makeExpressCallback(selectAllCompletedTransactions)
  );

  router.put("/edit/:id", verifyTokens,  makeExpressCallback(c_editReport))

  return router;
};

module.exports = reports;
