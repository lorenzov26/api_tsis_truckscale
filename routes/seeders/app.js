const express = require("express");
const router = express.Router();
const makeExpressCallback = require("../../src/express-callback/app");

const seed = require("./routes");

const seeds = seed({ router, makeExpressCallback });

const services = Object.freeze({
  seeds
});

module.exports = services;

module.exports = {
  seeds
};

module.exports = router;
