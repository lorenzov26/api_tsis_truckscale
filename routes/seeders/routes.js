const { c_seedDefault } = require("../../src/controller/seeders/app");

const seed = ({ router, makeExpressCallback }) => {
  router.post("/default", makeExpressCallback(c_seedDefault));

  return router;
};

module.exports = seed;
