const {
  driversAddNews,
  driversUpdates,
  selectAllDrivers,
  selectOneDrivers
} = require("../../src/controller/drivers/app");



const drivers = ({ router, makeExpressCallback, verifyTokens }) => {
  //########################
  // POST REQUESTS
  //########################
  router.post("/add",  verifyTokens ,makeExpressCallback(driversAddNews));
  //########################
  // END POST REQUESTS
  //########################

  //########################
  // PUT REQUESTS
  //########################
  router.put("/update/:id", verifyTokens,makeExpressCallback(driversUpdates));
  //########################
  // END PUT REQUESTS
  //########################

  //########################
  // GET REQUESTS
  //########################

  // select all drivers
  router.get("/select", verifyTokens , makeExpressCallback(selectAllDrivers));

  // select single driver
  router.get("/select/:id", verifyTokens, makeExpressCallback(selectOneDrivers));

  //########################
  // END GET REQUESTS
  //########################

  return router;
};

module.exports = drivers;
