const {
  accessRightsAddNews,
  selectOnRolesAccessRightss
} = require("../../src/controller/access_rights/app");

const accessRights = ({ router, makeExpressCallback, verifyTokens }) => {
  //########################
  // POST REQUESTS
  //########################

  //   add new access rights
  router.post("/add", verifyTokens, makeExpressCallback(accessRightsAddNews));

  //########################
  // END POST REQUESTS
  //########################

  //########################
  // PUT REQUESTS
  //########################

  //########################
  // END PUT REQUESTS
  //########################

  //########################
  // GET REQUESTS
  //########################

  // select access rights base on role id
  router.get("/select/:id", verifyTokens, makeExpressCallback(selectOnRolesAccessRightss));

  //########################
  // END GET REQUESTS
  //########################

  return router;
};

module.exports = accessRights;
