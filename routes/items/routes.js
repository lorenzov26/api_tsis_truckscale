const {
    c_selectAllItems,
    c_importItems,
    c_uomSelectAll,
    c_insertUom,
    c_updateUom
  } = require("../../src/controller/items/app");
  
  
  
  const items = ({ router, makeExpressCallback, verifyTokens }) => {
   
  
    
    router.get("/select", verifyTokens , makeExpressCallback(c_selectAllItems));

    router.post("/import",  makeExpressCallback(c_importItems))

    router.get("/uom/select", verifyTokens, makeExpressCallback(c_uomSelectAll))

    router.post("/uom/add", verifyTokens, makeExpressCallback(c_insertUom))

    router.put("/uom/update/:id", verifyTokens, makeExpressCallback(c_updateUom))
  
    return router;
  };
  
  module.exports = items;
  