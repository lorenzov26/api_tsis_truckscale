FROM node:10
WORKDIR /usr/src/app
COPY package*.json ./
ENV http_proxy "http://172.16.1.6:3128"
ENV https_proxy "http://172.16.1.6:3128"
#for serial port
RUN apt-get update -qq  && apt-get install -y build-essential
# under a proxy
RUN npm config set registry http://registry.npmjs.org/
RUN npm config set http-proxy http://172.16.1.6:3128
RUN npm config set https-proxy http://172.16.1.6:3128
RUN npm config set proxy http://172.16.1.6:3128
RUN npm install
COPY . .
EXPOSE 301
CMD ["npm", "start"]

