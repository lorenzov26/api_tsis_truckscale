// module.exports = {
//   collectCoverage: true,
//   coverageDirectory: "coverage",
//   reporters: ["default", "bamboo-jest-reporter"],
//   collectCoverageFrom: ["**/*.js"],
//   coverageThreshold: {
//     global: {
//       branches: 100,
//       functions: 100,
//       statements: 100,
//       lines: 100
//     }
//   },
//   setupFilesAfterEnv: ["./jest.setup.js"]
// };

module.exports = {
  setupFilesAfterEnv: ["./jest.setup.js"]
};
